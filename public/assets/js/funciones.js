function procesarRegistro(objeto,metodo,datos,callback){    
    $('#preload').modal('show')
    $.ajax({
        url: '../class/frontController.php',        
        data:{
            objeto: objeto,
            metodo: metodo,
            datos: datos
        },
        type: 'POST',
        dataType: 'json',
        success: function(resultado){
            if(resultado.ejecuto == false){
                if(resultado.codigoError == 1062){
                    swal('Ups!', 'Registro duplicado!!!', 'error')
                }else{
                    swal('Ups!', resultado.msgError, 'error')
                }
            }else{
                callback(resultado)
            }                    
        },
        error: function(xhr,status){
            console.log('Disculpe, existio un problema procesando '+objeto);
        },
        complete: function(xhr,status){
            $('#preload').modal('hide')
        }
    });
}

function llenarFormulario(formulario, objeto, metodo, datos, callback){
    procesarRegistro(objeto, metodo, datos,function(r){
        $.each(r.data[0],function(campo,valor){                
            if($('#'+formulario+' #'+campo).is(':checkbox')){
                if(valor == 1){
                    $('#'+formulario+' #'+campo).prop("checked",true)
                }
            }else{
                $('#'+formulario+' #'+campo).val(valor)    
            }
        })
        callback(r)        
    })    
}

function llenarSelect(objeto, metodo, datos, elemento, campo, defecto, callback){
    procesarRegistro(objeto,metodo,datos,function(r){
        if(defecto == 1){
            $("#"+elemento).empty();
            $("#"+elemento).append("<option value=''>Seleccione...</option>");    
        }        
        for (i = 0; i < r.data.length; i++){        
            $("#"+elemento).append("<option value="+r.data[i].id+">"+r.data[i][campo]+"</option>")
        }
        callback(r) 
    })
}

function parsearFormulario(form) {    
    var arrayForm = $(form).serializeArray();    
    var objectForm = {};
    arrayForm.forEach(function (obj, index) {
        objectForm[obj.name] = obj.value;
    });    
    return objectForm;
}

function currency(value, decimals, separators) {
    decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
    separators = separators || ['.', "'", ','];
    var number = (parseFloat(value) || 0).toFixed(decimals);
    if (number.length <= (3 + decimals))
        return number.replace('.', separators[separators.length - 1]);
    var parts = number.split(/[-.]/);
    value = parts[parts.length > 1 ? parts.length - 2 : 0];
    var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
        separators[separators.length - 1] + parts[parts.length - 1] : '');
    var start = value.length - 6;
    var idx = 0;
    while (start > -3) {
        result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
            + separators[idx] + result;
        idx = (++idx) % 2;
        start -= 3;
    }
    return (parts.length == 3 ? '-' : '') + result;
}

function alertaPorFecha(fecha){    
    let x = fecha.split(" ")
    let diff = moment(moment(x[0])).diff(moment().format('YYYY-MM-DD'), 'days')
    if(diff <= 2){
        return '<span class="label label-danger">'+fecha+'</span>'
    }else if(diff <= 5){
        return '<span class="label label-warning">'+fecha+'</span>'
    }else{
        return '<span class="label label-default">'+fecha+'</span>'
    }
}

function pintarTabla(elemento){
    $('#'+elemento).dataTable().fnDestroy();
    tabla = $('#'+elemento).DataTable({
        "lengthMenu": [ 50, 100, 200, 300 ],
        "language":{
            "decimal":        "",
            "emptyTable":     "Sin datos para mostrar",
            "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
            "infoEmpty":      "Mostrando 0 de 0 de 0 registros",
            "infoFiltered":   "(Filtrado de un total de _MAX_ registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar:",
            "zeroRecords":    "Ningún registro encontrado",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Sig",
                "previous":   "Ant"
            },
            "aria": {
                "sortAscending":  ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "bDestroy": true
        }
    })
}

function sumarElementosArray(array){
    let total = 0
    for(i in array){
        total += array[i]
    }
    return total
}


function construirItinerario(viaje, fi, ff){
    procesarRegistro('plantillas', 'select', {id: viaje}, function(r){    
        /*let inicio = moment(r.data[0].fecha_inicio, 'YYYY-MM-DD')
        let fin = moment(r.data[0].fecha_fin, 'YYYY-MM-DD')
        let dias = fin.diff(inicio, 'days')*/

        let inicio = moment(fi, 'YYYY-MM-DD')
        let fin = moment(ff, 'YYYY-MM-DD')
        let dias = fin.diff(inicio, 'days')

        $('#contenidoDetalle').html(
            '<div class="container">'+
                '<div class="row">'+
                    '<div class="col-md-12">'+
                        '<ul class="timeline">'+
                        '</ul>'+
                    '</div>'+
                '</div>'+
            '</div>'
        )
        
        for (let i = 0; i <= dias; i++) {
            $('.timeline').append
                (
                    '<li>'+
                        '<a href="#" class="float-left">'+inicio.format('DD-MMM-YYYY')+'</a>'+
                        '<table class="table table-bordered table-striped table-sm">'+
                            '<thead>'+
                                '<tr>'+
                                    '<th class="text-center">Hora</th>'+
                                    '<th class="text-center">Destino</th>'+
                                    '<th class="text-center">Servicio</th>'+
                                    '<th class="text-center">Opción</th>'+
                                    '<th class="text-center">Pasajeros</th>'+
                                    '<th class="text-center">Detalle</th>'+
                                '</tr>'+
                            '</thead>'+
                            '<tbody id="contenidoDia_'+inicio.format('YYYY-MM-DD')+'"></tbody>'+
                        '</table>'+
                    '</li>'
                )
            inicio.add(1, 'days')

        }
        procesarRegistro('plantillasDetalle', 'getItinerario', {plantilla: viaje, estado:'Activo'}, function(r){
            let fila
            let cu_iti
            let labels = {
                Gestionar: 'bg-gray',                
                Bloqueado: 'bg-yellow',
                Reservado: 'bg-green',
                Devuelto: 'bg-red'
            }        
            for(let i = 0; i < r.data.length; i++){
                cu_iti = r.data[i].di * r.data[i].cantidad
                inicio_iti = 1
                for(j = 0; j< cu_iti; j++){
                    $('#contenidoDia_'+inicio_iti).append(
                        '<tr class="'+labels[r.data[i].estado]+'">'+
                            '<td>'+r.data[i].hora_inicio+'</td>'+
                            '<td>'+r.data[i].destino+'</td>'+
                            '<td>'+r.data[i].servicio+'</td>'+
                            '<td>'+r.data[i].opcion+'</td>'+
                            '<td>'+r.data[i].pasajeros+'</td>'+
                            '<td>'+r.data[i].detalle+'</td>'+                            
                        '</tr>'
                    )
                    inicio_iti++
                }
            }            
        })
    })
}