<?php include 'header.php'; ?>
    <!-- about us section start -->
    <section class="about-us-wrapper pt-3 pb-3" style="background-color: #f2f4f6">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <form id="dataViaje">
                    <div class="row filtrosBusqueda">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="input-daterange input-group input d-flex justify-content-center" id="datepicker">
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <label><strong>Fecha inicio</strong></label>
                                    <input type="text" id="fecha_inicio" name="fecha_inicio"/>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <label><strong>Fecha fin</strong></label>
                                    <input type="text" id="fecha_fin" name="fecha_fin"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                            <label><strong># personas</strong></label>
                            <input type="number" id="pasajeros" name="pasajeros" placeholder="# Personas">
                        </div>
                        <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 d-flex align-items-center d-flex justify-content-center">
                            <a id="buscarFiltros" class="call-action-btn search-action-btn mr-2"><i class="fa fa-search"></i>Buscar</a>
                            <a id="limpiarFiltros" class="call-action-btn search-action-btn-filter"><i class="fa fa-filter"></i>Limpiar</a>
                        </div>
                    </div>
                    <div class="row d-flex justify-content-center">
                        <div class="custom-controls-stacked d-flex flex-row flex-wrap">
                            <label class="custom-control fill-checkbox">
                                <input type="checkbox" class="fill-control-input" value="Naturaleza">
                                <span class="fill-control-indicator"></span>
                                <span class="fill-control-description">Naturaleza</span>
                            </label>
                            <label class="custom-control fill-checkbox">
                                <input type="checkbox" class="fill-control-input" value="Música & Cultura">
                                <span class="fill-control-indicator"></span>
                                <span class="fill-control-description">Música & Cultura</span>
                            </label>
                            <label class="custom-control fill-checkbox">
                                <input type="checkbox" class="fill-control-input" value="Gastronomia">
                                <span class="fill-control-indicator"></span>
                                <span class="fill-control-description">Gastronomia</span>
                            </label>
                            <label class="custom-control fill-checkbox">
                                <input type="checkbox" class="fill-control-input" value="Aventura">
                                <span class="fill-control-indicator"></span>
                                <span class="fill-control-description">Aventura</span>
                            </label>
                            <label class="custom-control fill-checkbox">
                                <input type="checkbox" class="fill-control-input" value="Religioso">
                                <span class="fill-control-indicator"></span>
                                <span class="fill-control-description">Religioso</span>
                            </label>
                            <label class="custom-control fill-checkbox">
                                <input type="checkbox" class="fill-control-input" value="Otras">
                                <span class="fill-control-indicator"></span>
                                <span class="fill-control-description">Otras</span>
                            </label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- about us section end -->

    <section class="trending-section mt-5 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xl-12 text-center">
                    <div class="section-title">
                        <h2>Planes</h2>
                        <span>Elije el plan que mas se adapte a ti</span>
                    </div>
                </div>
            </div>
            <div id="seccionPlantillas" class="row">
            </div>
        </div>
    </section>
    <!-- trending Categories section end -->

    <!-- call to action section start -->
    <section class="call-to-action-wrap bg-cover" style="background-image: url('assets/img/call-to-action-bg.jpg')">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-xl-8 col-sm-12  col-md-7">
                    <div class="call-to-action">
                        <h2>Recibe Apoyo De Nuestro Equipo</h2>
                        <span>Coméntanos tu próximo destino</span>
                    </div>
                </div>
                <div class="col-xl-4 col-sm-12 col-md-5 text-lg-right">
                    <a href="https://wa.me/573148801607" target="_blank" class="call-action-btn"><i class="far fa-check-circle"></i>Contáctanos</a>
                </div>
            </div>
        </div>
    </section>
    <!-- call to action section end -->

<?php include 'footer.php'; ?>

    <script type="text/javascript">
        var user

        function initLogin(user){
            dataUser = user
            let dataViaje = JSON.parse(localStorage.getItem("viajeGlobal"))

            $('.input-daterange').datepicker({
                todayBtn: true,
                language: "es",
                format: "yyyy-mm-dd"
            });

            if (dataViaje != undefined && dataViaje[5] != undefined) {
                for (var i = 0; i < dataViaje.length; i++) {
                    $('#'+dataViaje[i].name).val(dataViaje[i].value)
                }

                $( ".fill-control-input" ).each(function( index ) {
                    if(dataViaje[5].value.includes($(this).val())){
                        $(this).prop('checked', true)
                    }
                });
                
                loadPlanes(dataViaje[3].value, dataViaje[2].value, dataViaje[5].value)
            }else{
                loadPlanes('', '', '')
            }

            $('#buscarFiltros').on('click', function(){
                console.log("data",dataViaje)
                setInfoGlobalViaje(function(personas,dias,filtros){
                    loadPlanes(dias, personas, filtros)
                })
            })

            $('#limpiarFiltros').on('click', function(){
                $('#dataViaje').trigger("reset");
            })
        }

        function loadPlanes(dias, pasajeros, filtros){
            procesarRegistro('plantillas', 'getPlantillas', {'estado':'activo', 'dias':dias, 'pasajeros':pasajeros, 'filtros': filtros}, function(r){
                if (r.ejecuto) {
                    if (r.data.length != 0) {
                        let fila = ''
                        for(let i = 0; i < r.data.length; i++){
                            let promocion = ''
                            if (r.data[i].promocion == 1) {
                                promocion = '<div class="promocionPlan">Promoción</div>'
                            }
                            fila += 
                                    '<div class="col-xl-4 col-md-6 col-lg-4 col-sm-12">'+
                                        '<div class="single-popular-item">'+
                                            '<div class="item-cover-image bg-cover containerImage">'+
                                                '<img src="../assets/img/plantillas/'+r.data[i].id+'.jpg" class="image">'+
                                                '<div class="middle">'+
                                                    '<p class="contenidoDescripcion">'+r.data[i].descripcion+'</p>'+
                                                '</div>'+
                                                promocion+
                                                '<div class="tituloPlan">'+
                                                    '<span class="item-price">'+r.data[i].nombre+'</span>'+
                                                '</div>'+
                                            '</div>'+
                                            '<div class="row item-content" style="margin: 0px; padding: 0px;">'+
                                                '<div class="col-xl-12 col-md-12 col-lg-12 col-sm-12 col-xs-12 text-center" style="background-color:#0000000a">'+
                                                    '<span id="text-incluye_'+r.data[i].id+'" style="display: none">'+r.data[i].incluye+'</span>'+
                                                    '<span id="text-n-incluye_'+r.data[i].id+'" style="display: none">'+r.data[i].no_incluye+'</span>'+
                                                '</div>'+
                                                '<div class="btn-incluye col-xl-6 col-md-6 col-lg-6 col-sm-6 col-xs-6 text-center" data-id-plantilla="'+r.data[i].id+'">Incluye</div>'+
                                                '<div class="btn-n-incluye col-xl-6 col-md-6 col-lg-6 col-sm-6 col-xs-6 text-center" data-id-plantilla="'+r.data[i].id+'">No incluye</div>'+
                                            '</div>'+
                                            '<div class="item-content">'+
                                                '<div id=score_"'+r.data[i].id+'" class="item-feedback text-center d-flex justify-content-center align-items-center">'+
                                                    '<div class="rateYo" data-rate="'+r.data[i].AVG+'"></div>'+ ' <span>(' + r.data[i].AVG + ')</span>' +
                                                '</div>'+
                                                '<div class="item-meta-data open text-center">'+
                                                    '<a  class="btn-loquiero" onclick="checkPlan('+r.data[i].id+',\''+r.data[i].nombre+'\')">Lo quiero!</a>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'
                        }
                        $('#seccionPlantillas').html(fila)
                    }else{
                        $('#seccionPlantillas').html('<h5 class="text-center">Ups! No tenemos servicios disponibles, intenta con otros destinos o tipos de servicios.</h5>')
                    }

                    $( ".rateYo").each(function( index ) {
                        $(this).rateYo({
                            starWidth: "20px",
                            ratedFill: "#139dc8",
                            readOnly: true,
                            rating: $(this).data('rate')
                        });
                    });

                    $('.btn-incluye').on('click', function(){
                        dataPlantillaId = $(this).attr('data-id-plantilla')
                        $('#text-n-incluye_'+dataPlantillaId).css('display','none')
                        $('#text-incluye_'+dataPlantillaId).css('display','inline-block')
                    })

                    $('.btn-n-incluye').on('click', function(){
                        dataPlantillaId = $(this).attr('data-id-plantilla')
                        $('#text-incluye_'+dataPlantillaId).css('display','none')
                        $('#text-n-incluye_'+dataPlantillaId).css('display','inline-block')
                    })


                }   
            })
        }

        function checkPlan(idPlantilla, nombrePlantilla){
            console.log(dataViaje)
            if (dataViaje[0].value != '' && dataViaje[3].value != null) {
                window.location.href = 'check.php'
                let preInfo = []
                preInfo.push({'name':'idPlantilla', 'value':idPlantilla})
                preInfo.push({'name':'nombrePlantilla', 'value':nombrePlantilla})
                localStorage.setItem("preInfo", JSON.stringify(preInfo));
            }else{
                swal('Ups!', 'Debes ingresar parametros de busqueda para continuar.', 'warning')
                $( ".filtrosBusqueda :input").each(function( index ) {
                    console.log(this)
                    $(this).css("border","solid red 1px")
                });
            }
        }

        function setInfoGlobalViaje(callback){
            let pasajeros = $('#pasajeros').val()
            let fecha_inicio = $('#fecha_inicio').val()
            let fecha_fin = $('#fecha_fin').val();

            let fecha_inicio_moment = moment(fecha_inicio, 'YYYY-MM-DD');
            let fecha_fin_moment = moment(fecha_fin, 'YYYY-MM-DD');

            let dias = (fecha_fin_moment.diff(fecha_inicio_moment, 'days')+1)

            let viajeGlobal = $("#dataViaje").serializeArray()

            let dataFiltros = []

            $( ".fill-control-input" ).each(function( index ) {
                if($(this).prop('checked')){
                    dataFiltros.push($(this).val())
                }
            });

            viajeGlobal.push({'name':'dias', 'value':dias})
            viajeGlobal.push({'name':'fk_clientes', 'value':dataUser.usuario.id})
            viajeGlobal.push({'name':'filtros', 'value':dataFiltros})

            localStorage.setItem("viajeGlobal", JSON.stringify(viajeGlobal));

            let dataViaje = JSON.parse(localStorage.getItem("viajeGlobal"))
            callback(dataViaje[2].value, dataViaje[3].value, dataViaje[5].value)
        }
    </script>