<?php include 'header.php'; ?>
    <!-- about us section start -->
    <section class="about-us-wrapper pt-2 pb-5">
        <div class="container-fluid" style="background-color: #f9f9f9">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 pt-3">
                    <div class="about-us-content-left">
                        <h4>Mis viajes</h4>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col"># Reserva</th>
                                    <th scope="col">Viaje</th>
                                    <th scope="col">Estado</th>
                                    <th scope="col">Detalle</th>
                                </tr>
                            </thead>
                            <tbody id="tblViajes">
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 bg-white" id="contenidoDetalle">
                </div>
            </div>
        </div>
    </section>
    <!-- about us section end -->

<?php include 'footer.php';  ?>

<script type="text/javascript">
    function initLogin(user){
    
        procesarRegistro('viajes', 'getViajesXusuarioWeb', {'campo':'fk_clientes', 'valor':user.usuario.id}, function(r){
            let item = ''

            if (r.ejecuto) {
                for (var i = 0; i < r.data.length; i++) {
                    item += '<tr>'+
                                '<th scope="row">'+r.data[i].id+'</th>'+
                                '<td><strong>Inicio:</strong> '+r.data[i].fecha_inicio+ ' - <strong>Fin:</strong> ' + r.data[i].fecha_fin +'</td>'+
                                '<td>'+r.data[i].estado+'</td>'+
                                '<td><a class="call-action-btn btn-sm" onclick="verDetalle('+r.data[i].id+')"></i><i class="fa fa-star" aria-hidden="true"></i> Calificar</a></td>'
                            '</tr>'
                }
                
                $('#tblViajes').append(item)
            }
        })
    } 

    function verDetalle(idViaje){
        procesarRegistro('viajesDetalle', 'getDetalle', {'fk_viajes':idViaje}, function(r){
            let item = ''
            item = '<div class="review_box">'+
                        '<h4>¿Que tan buena fue tu experiencia?</h4>'+
                        '<ul id="starts">'+
                        '</ul>'+
                        '<button class="btn-submit" onclick="guardarCalificacion()">Guardar</button>'+
                    '</div>'

            $('#contenidoDetalle').html(item)

            if (r.ejecuto) {
                for (var i = 0; i < r.data.length; i++) {
                    $('#starts').append(
                        '<li class="d-flex align-items-center">'+
                            '<div class="rateYo" data-viaje-opcion="'+r.data[i].fk_opciones+'" data-viaje-detalle="'+r.data[i].id+'" data-rate="'+r.data[i].calificacion+'"></div>'+
                            r.data[i].servicio+
                        '</li>'+
                        '<input id="obs_'+r.data[i].id+'" type="text" placeholder="Observación" name="pasajeros" value="'+r.data[i].observacion+'">'
                    )
                }

                $( ".rateYo").each(function( index ) {
                    $(this).rateYo({
                        starWidth: "20px",
                        ratedFill: "#ef4761",
                        fullStar: true,
                        rating: $(this).data('rate')
                    });
                });
            }
        })
    }

    function guardarCalificacion(){
        $(".loader-wrap").css( "display", "block" );
        
        data = []
        $( ".rateYo" ).each(function( index ) {
            let objeto = {}
            objeto.calificacion = $( this ).rateYo("rating");
            objeto.fk_opciones = $( this ).data('viaje-opcion')
            objeto.id = $( this ).data('viaje-detalle')
            objeto.observacion = $( "#obs_"+objeto.id ).val()
            data.push(objeto);
        });

        procesarRegistro('viajesDetalle', 'updateCalificacion', data, function(r){
            if (r.ejecuto) {
                $(".loader-wrap").css( "display", "none" );
                swal('Perfecto!', 'Gracias por ayudarnos a mejorar', 'success')
            }
        })
    }

</script>