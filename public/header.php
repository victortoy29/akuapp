<!DOCTYPE html>
<html lang="es">
<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- ======== Page title ============ -->
    <title>AKUA - Agencia de Viajes y Turismo</title>

    <!-- ========== Favicon Icon ========== -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- ===========  All Stylesheet ================= -->
    <!--  fontawesome css plugins -->
    <link rel="stylesheet" href="assets/css/fontawesome.min.css">
    <!-- Font Awesome OLD -->
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <!--  slick css plugins -->
    <link rel="stylesheet" href="assets/css/slick.css">
    <!--  rangeSlider css plugins -->
    <link rel="stylesheet" href="assets/css/ion.rangeSlider.min.css">
    <!--  slick theme css plugins -->
    <link rel="stylesheet" href="assets/css/slick-theme.css">
    <!--  magnific-popup css plugins -->
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <!--  owl carosuel css plugins -->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <!--  owl theme css plugins -->
    <link rel="stylesheet" href="assets/css/owl.theme.css">
    <!--  Bootstrap css plugins -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- template main style css file -->
    <link rel="stylesheet" href="style.css">
    <!-- template responsive css stylesheet -->
    <link rel="stylesheet" href="assets/css/responsive.css">

    <!-- timeline -->
    <link rel="stylesheet" href="assets/css/timeline.css">
    <!-- datapicker -->
    <link rel="stylesheet" href="assets/plugins/datapicker/css/bootstrap-datepicker3.min.css">
    <!-- customControls -->
    <link rel="stylesheet" href="assets/css/customControls.css">
    <!-- hoverImage -->
    <link rel="stylesheet" href="assets/css/hoverImage.css">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">

    <style>
        .main-menu nav ul li a{
            font-size: 14px;
        }

        .header-section{
            border-bottom: 4px solid #139dc8;
        }

        .search-action-btn{
            padding-left: 10px !important;
            padding-right: 10px !important;
            color: #FFF !important;
        }

        .search-action-btn-filter{
            padding-left: 10px !important;
            padding-right: 10px !important;
            color: #FFF !important;
            background-color: #a90a32 !important;
        }

        .actividaddes-action-btn{
            background-color: #17a2b8 !important;
        }

        .actividaddes-action-btn:hover{
            background-color: #148EA1 !important;
        }

        .btn-loquiero:hover{
            color: #FFF !important;
        }

        .labelInfo p{
            margin-bottom: 0px;
        }

        .btn-incluye{
            border-bottom-left-radius:30px;
            background-color: #00000017;
            border-right: 1px solid white;
        }

        .btn-n-incluye{
            border-bottom-right-radius:30px;
            background-color: #00000017
        }
        
        .btn-incluye:hover{
            background-color: #0000000a !important;
            cursor: pointer;
        }

        .btn-n-incluye:hover{
            background-color: #0000000a !important;
            cursor: pointer;
        }

        .btn-regresar{
            border-radius: 50px !important;
            padding-left: 10px !important;
            padding-right: 10px !important;
        }

        .btn-reservar{
            background-color: #27AE60 !important;
            color: #fff !important;
            cursor: pointer !important;
        }

        .btn-reservar:hover{
            background-color: #1D8348 !important;
        }

        .btn-sm{
            background-color: #27AE60 !important;
            color: #fff !important;
            cursor: pointer !importan
        }

    </style>
</head>

<body class="theme_body">

    <!-- preloader element started -->
    <div class="loader-wrap">
        <div class="pin"></div>
        <div class="pulse"></div>
    </div>
    <!-- preloader element end -->

    <!-- header section start -->
    <header class="header-section">
        <!-- top bar -->
        
        <!-- main menu -->
        <div class="main-header" style="padding: 5px 0px">
            <div class="main-menu-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-sm-12">
                            <div class="main-menu">
                                <nav class="navbar navbar-expand-lg">
                                    <a class="navbar-brand" href="index.php">
                                        <img src="assets/img/logo.png" class="logo-display" alt="shipo" style="width: 60%;">
                                    </a> 
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav_mneu">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                    <div class="collapse navbar-collapse justify-content-end" id="nav_mneu">
                                        <ul class="navbar-nav">
                                            <li><a href="inicio.php"><i class="fas fa-home"></i> Inicio</a></li>
                                            <li><a id="btnReserva" href="booking.php" style="display: none"><i class="fas fa-paper-plane"></i> Reservas</a></li>
                                            <li><a id="btnUser" style="display: none"><i class="fas fa-user"></i> <span class="nameUser"></span></a></li>
                                            <li><a href="logout.php" id="btnSalir" style="display: none"><i class="fas fa-sign-out-alt"></i> Salir</a></li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header section end -->

    <!-- Modal logueo-->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" style="background-color:#80282800; border: #80282800">
                <div class="modal-body">
                    <div class="search-tab-wrap mt-5">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#inicioSesion">Ingresar</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#registro">Soy nuevo</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active container" id="inicioSesion">
                                <div class="search-form-box">
                                    <h3>Hola,</h3>
                                    <p>Ingresa tus datos para ingresar.</p>
                                    <form id="formInicioSesion" class="search-form">
                                        <input type="email" placeholder="Email" name="correo" required="true">
                                        <input type="password" placeholder="Contraseña" name="password" required="true">
                                        <button type="input">Listo</button>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane container" id="registro">
                                <div class="search-form-box">
                                    <h3>Bienvenido!</h3>
                                    <p>Queremos conocerte.</p>
                                    <form id="formRegistro" class="search-form">
                                        <input name="nombre" type="text" placeholder="¿Como te llamas?" required="true">
                                        <input name="correo" type="email" placeholder="¿Tu email?" required="true">
                                        <input name="telefono" type="number" placeholder="¿Tu telefono?" required="true">
                                        <input name="password" type="password" placeholder="Contraseña" required="true">
                                        <button type="input">A viajar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>