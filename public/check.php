<?php include 'header.php'; ?>
    <div class="row" style="margin: 20px;">
        <div class="col text-left">
            <a href="inicio.php" class="call-action-btn btn-regresar"><i class="fa fa-chevron-circle-left"></i>Regresar</a>
        </div>
        <div class="col text-center">
            <span><b>Descripción modo de uso:</b> descubre el itinerario del plan y aumenta la experiencia con nuevas actividades en horas libres.</span>
        </div>
        <div id="divBtnReservar" class="col text-right">
            <div id="seccionTotal" style="font-size: 12px">
            </div>
            <a class="call-action-btn btn-reservar btnReservaSave"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></i>Reservar ahora</a>
        </div>
    </div>

    <!-- explors map and sidebar section start -->
    <section class="listico-explors-map-search section-padding pt-0 pb-0">
        <div class="container-fluid m-0 p-0">
            <div class="row">
                <div class="col-8" id="contenidoDetalle"></div>
                <div class="col-3">
                    <div class="top-place-widgets">
                        <!--div class="sidebar-advance-search-wrapper mt-5">
                            <div class="listico-advance-search">
                                <div class="row">
                                    <div class="col labelInfo">
                                        <strong><p>Tu plan</p></strong>
                                        <strong><p>Fecha inicio</p></strong>
                                        <strong><p>Fecha fin</p></strong>
                                        <strong><p>Nro. personas</p></strong>
                                    </div>
                                    <div class="col labelInfo">
                                        <p id="listaPlanes"></p>
                                        <p id="labelInicio"></p>
                                        <p id="labelFin"></p>
                                        <p id="labelPersonas"></p>
                                    </div>
                                </div>
                            </div>
                            <a id="btnReservaSave" class="btn-default btn-loquiero mt-3">Reserva Ahora</a>
                        </div-->
                        <!--div class="seccionServicios"></div-->
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" id="seccionServicios"></div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <i class="fa fa-arrow-circle-left fa-2x mt-5" aria-hidden="true" style="color: #139dc8 !important"></i>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <i class="fa fa-arrow-circle-right fa-2x mt-5" aria-hidden="true" style="color: #139dc8 !important"></i>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- explors map and sidebar section end -->

    <div class="row" style="margin: 20px;">
        <div class="col text-left">
            <span><b>Plan:</b> <span id="listaPlanes"></span></span>
            <span><b>Fecha inicio:</b> <span id="labelInicio"></span></span>
            <span><b>Fecha fin:</b> <span id="labelFin"></span></span>
            <span><b>Nro personas:</b> <span id="labelPersonas"></span></span>
        </div>
        <div class="col text-right">
            <a class="call-action-btn btn-reservar btnReservaSave"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></i>Reservar ahora</a>
        </div>
    </div>

    <!-- Modal Seleccionar Opcion -->
    <div class="modal fade" id="modalSeleccionOpcion" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Agregar Servicio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formSeleccionOpcion" class="search-form">
                        <label>Hora del servicio</label>
                        <div class="form-row">
                            <div class="col">
                                <select id="horaInicioOpcion" name="hora_inicio" required="required"></select>
                            </div>
                        </div>
                        <div class="form-row seccionHotel" style="display: none;">
                            <label>Cantidad de noches</label>
                            <input id="cantidadNoches" type="number" placeholder="# Noches" value="1" name="cantidad" required="true">
                        </div>
                        <input type="number" id="noPasajeros" placeholder="# pasajeros" name="pasajeros" required="true">
                        <button type="input" form="formSeleccionOpcion">Agregar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php include 'footer.php'; ?>

    <script type="text/javascript">
        var res = []
        var opcion
        var flag = 0

        var preInfo
        var dataInitViaje
        var nuevoServicio = []

        function initLogin(user){
            preInfo = JSON.parse(localStorage.getItem("preInfo"))
            dataInitViaje = JSON.parse(localStorage.getItem("viajeGlobal"))

            console.log(preInfo)
            console.log(dataInitViaje)

            $('#listaPlanes').text(preInfo[1].value)
            $('#labelInicio').text(dataInitViaje[0].value)
            $('#labelFin').text(dataInitViaje[1].value)
            $('#labelPersonas').text(dataInitViaje[2].value)
            
            procesarRegistro('plantillasDetalle', 'getItinerario', {plantilla: preInfo[0].value}, function(r){
                res = r.data
                console.log(res)
                construirItinerario2(preInfo[0].value, dataInitViaje[0].value, dataInitViaje[1].value, dataInitViaje[3].value, res)
            })

            procesarRegistro('plantillas', 'getPlantillas', {id: preInfo[0].value}, function(r){
                $('#seccionTotal').html(
                    '<strong><span class="text-left">TOTAL </span></strong>'+
                    '<span><strong>Grupo: </strong> $ '+r.data[0]['total']+'</span> '+
                    '<span><strong>Persona: </strong> $ '+r.data[0]['total']/r.data[0]['pasajeros']+'</span>'
                )
            })
        }

        function construirItinerario2(idPlantilla, fi, ff, dias, r){

            let inicio = moment(fi, 'YYYY-MM-DD')
            let fin = moment(ff, 'YYYY-MM-DD')

            $('#contenidoDetalle').html(
                '<div class="container">'+
                    '<div class="row">'+
                        '<div class="col-md-12">'+
                            '<ul class="timeline">'+
                            '</ul>'+
                        '</div>'+
                    '</div>'+
                '</div>'
            )

            $('.timeline').html('')
        
            for (let i = 0; i < dias; i++) {
                $('.timeline').append
                    (
                        '<li>'+
                            '<a href="#" class="float-left">'+inicio.format('DD-MMM-YYYY')+'</a>'+
                            '<table class="table table-bordered table-sm">'+
                                '<thead>'+
                                    '<tr>'+
                                        '<th class="text-center">Hora</th>'+
                                        '<th class="text-center">Destino</th>'+
                                        '<th class="text-center">Servicio</th>'+
                                        '<th class="text-center">Opción</th>'+
                                        '<th class="text-center">Pasajeros</th>'+
                                        '<th class="text-center">Duración</th>'+
                                        '<th class="text-center">Llegada</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody id="contenidoDia_'+(i+1)+'"></tbody>'+
                            '</table>'+
                        '</li>'
                    )
                inicio.add(1, 'days')
            }

            let cu_iti
            let hora_inicio_disponibilidad
            let tiempo_libre
            let tiempo_usado
            let sitio = ''
            let idSitio = ''
            let dia = 0
            let centinela = 1
            //La variable sincronizadorDias sirve para ir a la par con los dias consecutivos, si por ejemplo el viaje es de 4 dias y no tiene
            //Actividades el dia 2 y 3 entonces esta variable hace que se detenga y cree disponibilidad ya que en los registros de la base de datos
            //no viene para validar
            let sincronizadorDias = 1 

            for(let i = 0; i < r.length; i++){
                cu_iti = r[i].di * r[i].cantidad
                inicio_iti = r[i].dia
                //Evaluo tipo de sevicio
                if(r[i].ts == 2 || r[i].ts == 4){
                    if(dia != r[i].dia){//Esto es para evaluar el cambo de dia y contar desde las 0 horas
                        if(sincronizadorDias != r[i].dia){
                            for (let j = sincronizadorDias; j < r[i].dia ; j++) {
                                sitio = r[i].destino
                                idSitio = r[i].idDestino
                                $('#contenidoDia_'+j).append('<tr>'+
                                    '<td colspan=7>Tienes <span class="badge badge-primary">24 horas</span> libres en <span class="badge badge-primary">'+sitio+'</span><a id="buscarFiltros" onclick="recomendarActividad(24,'+idSitio+','+j+',\'00:00\',\'23:00\')" class="call-action-btn search-action-btn actividaddes-action-btn float-right">Ver actividades <i class="fa fa-arrow-circle-o-right"></i></a></td>'+
                                '</tr>')
                                sincronizadorDias++
                            }
                        }
                        if(centinela != 1){
                            //Determina espacio al finalizar el día 
                            tiempo_kuy = moment('24:00','HH:mm').diff(moment(tiempo_usado,'HH:mm'),'hours')
                            if(tiempo_kuy > 0){
                                $('#contenidoDia_'+dia).append('<tr>'+
                                    '<td colspan=7>Tienes <span class="badge badge-primary">'+tiempo_kuy+' horas</span> libres en <span class="badge badge-primary">'+sitio+'</span><a id="buscarFiltros" onclick="recomendarActividad('+tiempo_kuy+','+idSitio+','+dia+','+'\''+hora_inicio_disponibilidad+'\''+',\'24:00\')" class="call-action-btn search-action-btn actividaddes-action-btn  float-right">Ver actividades <i class="fa fa-arrow-circle-o-right"></i></a></td>'+
                                '</tr>')
                            }
                        }else{
                            sitio = r[i].destino
                            idSitio = r[i].idDestino
                        }
                        hora_inicio_disponibilidad = '00:00'
                        dia = r[i].dia
                        sincronizadorDias++
                    }                        
                    tiempo_libre = moment(r[i].hora_inicio,'HH:mm').diff(moment(hora_inicio_disponibilidad,'HH:mm'),'hours')
                    tiempo_usado = moment(r[i].hora_inicio,'HH:mm').add(r[i].duracion,'hours').format('HH:mm')
                    if(tiempo_libre  > 0){
                        $('#contenidoDia_'+inicio_iti).append('<tr>'+
                                    '<td colspan=7>Tienes <span class="badge badge-primary">'+tiempo_libre+' horas</span> libres en <span class="badge badge-primary">'+sitio+'</span><a id="buscarFiltros" onclick="recomendarActividad('+tiempo_libre+','+idSitio+','+dia+','+'\''+hora_inicio_disponibilidad+'\''+','+'\''+r[i].hora_inicio+'\''+')" class="call-action-btn search-action-btn actividaddes-action-btn float-right">Ver actividades <i class="fa fa-arrow-circle-o-right"></i></a></td>'+
                                '</tr>')
                    }
                    if(r[i].ts == 2){
                        sitio = r[i].destino
                        idSitio = r[i].idDestino
                    }else{
                        sitio = r[i].llegada
                        idSitio = r[i].idLlegada
                    }
                    hora_inicio_disponibilidad = tiempo_usado
                    centinela++
                }
                          
                for(j = 1; j <= cu_iti; j++){
                    $('#contenidoDia_'+inicio_iti).append('<tr>'+
                            '<td>'+r[i].hora_inicio+'</td>'+
                            '<td>'+r[i].destino+'</td>'+
                            '<td>'+r[i].servicio+'</td>'+
                            '<td>'+r[i].opcion+'</td>'+
                            '<td>'+r[i].pasajeros+'</td>'+
                            '<td>'+r[i].duracion+'</td>'+
                            '<td>'+r[i].llegada+'</td>'+
                        '</tr>')                        
                    inicio_iti++
                }
            }
        }

        function recomendarActividad(tiempo, idDestino, diaActividad, horaInicioDis, horaInicio){
            $('#seccionServicios').html('')
            procesarRegistro('opciones', 'getOpcionesXservicio', {'estado':'activo', 'fk_destinos':idDestino, 'duracion':tiempo}, function(r){
                if (r.ejecuto) {
                    if (r.data.length != 0) {
                        let fila = ''
                        let flagActive
                        for(let i = 0; i < r.data.length; i++){
                            if (i == 0) {
                                flagActive = 'active'
                            }else{
                                flagActive = ''
                            }

                            fila += '<div class="carousel-item '+flagActive+'">'+
                                '<div class="single-popular-item">'+
                                    '<div class="item-cover-image bg-cover" style="background-image: url('+r.data[i][0].imagen+')">'+
                                       '<div class="tags">'+
                                            '<ul>'+
                                                '<li><a href="#">'+r.data[i][0].nombreTipoServicio+'</a></li>'+
                                            '</ul>'+
                                        '</div>'+
                                        '<div class="item-details">'+
                                            '<div class="brand">'+
                                                '<span class="item-price"><i class="fas fa-map-marker-alt"></i> '+ r.data[i][0].nombreDestino+'</span>'+
                                            '</div>'+
                                            '<div class="item-category">'+
                                                '<div class="cat-icon">'+
                                                    '<i class="text-light fa '+r.data[i][0].icono+'"></i>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="item-content">'+
                                        '<h4><a href="listing-single-details.html">'+r.data[i][0].nombre+'</a></h4>'+
                                        '<div class="row">'+
                                            '<div class="col">'+
                                                '<span class="badge badge-info">Incluye</span><p>'+r.data[i][0].incluye+'</p>'+
                                            '</div>'+
                                            '<div class="col">'+
                                                '<span class="badge badge-secondary">No incluye</span><p>'+r.data[i][0].no_incluye+'</p>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="item-store-location">'+
                                            '<div class="menu-item-price">'+
                                                '<ul id="opciones_'+r.data[i][0].idServicio+'">'+
                                                '</ul>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'
                        }

                        $('#seccionServicios').html(fila)
                        $('.carousel').carousel('pause')
                    
                        for (var j = 0; j < r.data.length; j++) {
                            let item = ''
                            for (let i = 0; i < r.data[j].length; i++) {
                                item += '<li class="elejirOpcion" data-duracion="'+r.data[j][i].duracion+'" data-opcion="'+r.data[j][i].id+'" data-idts="'+r.data[j][i].idts+'">'+r.data[j][i].opcion+
                                    '<span>'+
                                        '$'+r.data[j][i].precio+ ' ' +
                                        '<i class="fa fa-shopping-cart" aria-hidden="true"></i>'+
                                    '</span>'+
                                '</li>'
                            }
                            $('#opciones_'+r.data[j][0].idServicio).html(item)
                        }
                        
                        $('.elejirOpcion').on('click', function(){
                            let idOpcion = $(this).attr('data-opcion')
                            let idts = $(this).attr('data-idts')
                            let duracion = $(this).attr('data-duracion')
                            dia = diaActividad

                            jQuery.each( r.data, function( i, val ) {
                                for (let i = 0; i < val.length; i++) {
                                    if (val[i].id == idOpcion) {
                                        nuevoServicio =  val[i]
                                    }
                                }
                            });
                            seleccionarOpcion(idOpcion,idts, horaInicioDis, horaInicio, duracion)
                        })

                    }else{
                        $('#seccionServicios').html('No hemos encontrado actividades disponibles')
                    }
                }         
            })
        }

        function seleccionarOpcion(idOpcion, idts, horaInicioDis, horaInicio, duracion){
            opcion = idOpcion

            let horaFinOpcion = moment(horaInicio,'HH:mm').subtract(duracion, 'hours').format('HH:mm')

            let i = 0
            let hora

            $('#horaInicioOpcion').html('')
            $('#noPasajeros').val('')
            
            do {
                hora = moment(horaInicioDis,'HH:mm').add(i, 'hours').format('HH:mm');
                $('#horaInicioOpcion').append('<option value="'+hora+'">'+hora+'</option>')
                i++
            }
            while (hora < horaFinOpcion);

            $('#modalSeleccionOpcion').modal('show')
            $('#modalSeleccionOpcion').on('shown.bs.modal', function (e) {
                
                if (idts == 3) {
                    $('.seccionHotel').css('display', 'block')
                }
            })
        }

        $('#formSeleccionOpcion').on('submit',function(e){
            e.preventDefault();

            let objFormulario = parsearFormulario($('#formSeleccionOpcion'));
            
            objFormulario.dia = dia
            objFormulario.fk_opciones = nuevoServicio.id
            objFormulario.ts = nuevoServicio.idts
            objFormulario.duracion = nuevoServicio.duracion
            objFormulario.destino = nuevoServicio.nombreDestino
            objFormulario.di = nuevoServicio.di
            objFormulario.servicio = nuevoServicio.nombre
            objFormulario.opcion = nuevoServicio.opcion
            objFormulario.llegada = nuevoServicio.llegada
            objFormulario.idDestino = nuevoServicio.idDestino
            objFormulario.idLlegada = nuevoServicio.idLlegada

            objFormulario.precio = nuevoServicio.precio
            objFormulario.costo = nuevoServicio.costo

            objFormulario.cantidad = parseInt(objFormulario.cantidad)
            objFormulario.pasajeros = parseInt(objFormulario.pasajeros)
            
            res.push(objFormulario)

            res.sort(function(a, b) {
                return  a.dia - b.dia || parseInt(a.hora_inicio) - parseInt(b.hora_inicio);
            });

            $('.timeline').html('')
            $('#modalSeleccionOpcion').modal('hide')
            construirItinerario2(preInfo[0].value, dataInitViaje[0].value, dataInitViaje[1].value, dataInitViaje[3].value, res)
        });

        $('.btnReservaSave').on('click', function(){
            swal("¿Desea finalizar su reserva ahora?")
            .then((value) => {
                if (value) {
                    procesarRegistro('viajesDetalle', 'cargarPlantillaWeb', {dataViaje:dataInitViaje, data:res}, function(r){
                        if (r.ejecuto) {
                            swal('Perfecto!', 'Tu reserva se realizo correctamente', 'success')
                            window.location.href = "booking.php"
                        }else{
                            swal('Ups!', 'Se ha presentado un error: ' + r.msgError, 'error')
                        }
                    })
                }
            });
        })
        
    </script>
