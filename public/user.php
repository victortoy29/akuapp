<?php include 'header.php'; ?>
    <!-- about us section start -->
    <section class="about-us-wrapper pt-5 pb-5" style="background-color: #f9f9f9">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="about-us-content-left">
                        <h3>Hola, <span id="nombreUsuario"></span> <br>Esperamos que tu experiencia con Akua sea inolvidable!</h3>
                        <p>En esta sección podrás administrar tu información personal y la de tus viajes.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="col-12">
                        <div class="comment_box">
                            <h5>Mi perfil</h5>
                            <form id="formPerfil">
                                <input type="text" id="nombre" name="nombre">
                                <input type="mail" id="correo" name="correo">
                                <input type="number" id="telefono" name="telefono">
                            </form>
                        </div>
                    </div>
                    <div class="col-12">
                        <button class="btn-submit" type="submit" form="formPerfil">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- about us section end -->

<?php include 'footer.php';  ?>

<script type="text/javascript">

    function initLogin(user){
        console.log(user)
        $('#nombreUsuario').append(user.usuario.nombre)

        llenarFormulario('formPerfil','clientes', 'select', {'id':user.usuario.id}, function(r){
            console.log(r)
        })

        $('#formPerfil').on('submit',function(e){
            e.preventDefault();
            let objFormulario = parsearFormulario($('#formPerfil'));
            objFormulario.id = user.usuario.id
    
            $.ajax({
                url: '../class/frontController.php',
                data:{
                    objeto: 'clientes',
                    metodo: 'update',
                    datos: objFormulario
                },
                type: 'POST',
                dataType: 'json',
                success: function(r){
                    if (r.ejecuto) {
                        swal('Perfecto!', 'Tu información se actualizo', 'success')
                    }else{
                        swal('Ups!', 'Se ha presentado un error: ' + r.msgError, 'error')
                    }
                },
                error: function(xhr,status){
                    alert('Disculpe, existio un problema procesando')
                },
                complete: function(xhr,status){
                    $('#preload').modal('hide')
                }
            });            
        });
    }
    
</script>