<!DOCTYPE html>
<html lang="es">
<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- ======== Page title ============ -->
    <title>AKUA - Agencia de Viajes y Turismo</title>

    <!-- ========== Favicon Icon ========== -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- ===========  All Stylesheet ================= -->
    <!--  fontawesome css plugins -->
    <link rel="stylesheet" href="assets/css/fontawesome.min.css">
    <!-- Font Awesome OLD -->
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <!--  slick css plugins -->
    <link rel="stylesheet" href="assets/css/slick.css">
    <!--  rangeSlider css plugins -->
    <link rel="stylesheet" href="assets/css/ion.rangeSlider.min.css">
    <!--  slick theme css plugins -->
    <link rel="stylesheet" href="assets/css/slick-theme.css">
    <!--  magnific-popup css plugins -->
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <!--  owl carosuel css plugins -->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <!--  owl theme css plugins -->
    <link rel="stylesheet" href="assets/css/owl.theme.css">
    <!--  Bootstrap css plugins -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- template main style css file -->
    <link rel="stylesheet" href="style.css">
    <!-- template responsive css stylesheet -->
    <link rel="stylesheet" href="assets/css/responsive.css">

    <!-- timeline -->
    <link rel="stylesheet" href="assets/css/timeline.css">

    <style>
        body {
            padding-top: 200px;
            /*margin: 0;
            width: 100%;
            height: 100vh;
            background: linear-gradient(-45deg, #ee7752, #e73c7e, #23a6d5, #23d5ab);
            background-size: 400% 400%;
            animation: gradientBG 15s ease infinite;*/
        }

        /*@keyframes gradientBG {
        0% {
        background-position: 0% 50%;
        }
        50% {
        background-position: 100% 50%;
        }
        100% {
        background-position: 0% 50%;
        }
        }*/

    </style>

</head>

<body class="theme_body">

    <!-- preloader element started -->
    <div class="loader-wrap">
        <div class="pin"></div>
        <div class="pulse"></div>
    </div>
    <!-- preloader element end -->

    <div class="search-tab-wrap">
	    <!-- Nav tabs -->
	    <ul class="nav nav-tabs">
	        <li class="nav-item">
	            <a class="nav-link active" data-toggle="tab" href="#inicioSesion">Ingresar</a>
	        </li>
	        <li class="nav-item">
	            <a class="nav-link" data-toggle="tab" href="#registro">Soy nuevo</a>
	        </li>
	    </ul>
	    <!-- Tab panes -->
	    <div class="tab-content">
	        <div class="tab-pane active container" id="inicioSesion">
	            <div class="search-form-box">
	                <h3>Hola,</h3>
	                <p>Ingresa tus datos para ingresar.</p>
	                <form id="formInicioSesion" class="search-form">
	                    <input type="email" placeholder="Email" name="correo" required="true">
	                    <input type="password" placeholder="Contraseña" name="password" required="true">
	                    <button type="input">Listo</button>
	                </form>
	            </div>
	        </div>
	        <div class="tab-pane container" id="registro">
	            <div class="search-form-box">
	                <h3>Bienvenido!</h3>
	                <p>Queremos conocerte.</p>
	                <form id="formRegistro" class="search-form">
	                    <input name="nombre" type="text" placeholder="¿Como te llamas?" required="true">
	                    <input name="correo" type="email" placeholder="¿Tu email?" required="true">
	                    <input name="telefono" type="number" placeholder="¿Tu telefono?" required="true">
	                    <input name="password" type="password" placeholder="Contraseña" required="true">
	                    <button type="input">A viajar</button>
	                </form>
	            </div>
	        </div>
	    </div>
	</div>

    <!--  ALl JS Plugins -->
    <script src="assets/js/jquery-1.12.4.min.js"></script>
    <script src="assets/js/jquery.easing.1.3.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/scrollUp.min.js"></script>
    <script src="assets/js/magnific-popup.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/slick.min.js"></script>
    <script src="assets/js/ion.rangeSlider.min.js"></script>
    <script src="assets/js/rater.min.js"></script>
    <script src="assets/js/main.js"></script>
    
    <!--Sweet Alert-->
    <script src="../bower_components/sweetalert/docs/assets/sweetalert/sweetalert.min.js"></script>

    <!-- moment -->
    <script src="../bower_components/moment/min/moment.min.js"></script>
    <script src="../bower_components/moment/locale/es.js"></script>

    <script src="assets/js/funciones.js"></script>

    <script type="text/javascript">
    $(function(){
        $('#formInicioSesion').on('submit',function(e){
            e.preventDefault();
            let objFormulario = parsearFormulario($('#formInicioSesion'));
            $.ajax({
                url: '../class/frontController.php',
                data:{
                    objeto: 'clientes',
                    metodo: 'loginService',
                    datos: objFormulario
                },
                type: 'POST',
                dataType: 'json',
                success: function(r){
                    if(r.ejecuto){
                        window.location.href = 'inicio.php';
                    }else{                        
                        swal('Ups!', 'Se ha presentado un error: ' + r.msgError, 'error')
                    }
                },
                error: function(xhr,status){
                    alert('Disculpe, existio un problema procesando')
                },
                complete: function(xhr,status){
                    $('#preload').modal('hide')
                }
            });            
        });

        $('#formRegistro').on('submit',function(e){
            e.preventDefault();
            let objFormulario = parsearFormulario($('#formRegistro'));
            $.ajax({
                url: '../class/frontController.php',
                data:{
                    objeto: 'clientes',
                    metodo: 'insertClienteWeb',
                    datos: objFormulario
                },
                type: 'POST',
                dataType: 'json',
                success: function(r){
                    if (r.ejecuto) {
                        swal('Perfecto!', 'El usuario se creo correctamente', 'success')
                    }else{
                        swal('Ups!', 'Se ha presentado un error: ' + r.msgError, 'error')
                    }
                },
                error: function(xhr,status){
                    alert('Disculpe, existio un problema procesando')
                },
                complete: function(xhr,status){
                    $('#preload').modal('hide')
                }
            });            
        });
    })
    </script>
</body>
</html>