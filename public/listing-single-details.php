<?php include 'header.php'; ?>
    <div id="header"></div>

    <div class="single-details-navbar">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-4 col-6 ">
                    <nav class="details-nav navbar navbar-expand-lg">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#detailsnav">
                            <i class="fas fa-bars"></i>
                        </button>
                       <div class="collapse navbar-collapse justify-content-start" id="detailsnav">
                          <ul class="navbar-nav">
                            <a href="#des-box">Descripción</a>
                            <a href="#photo-box">Galeria</a>
                            <a href="#map-box">Ubicación</a>
                            <a href="#menu-price">Opciones</a>
                          </ul>
                       </div>
                    </nav>
                </div>
                <div class="col-lg-5 col-md-8 col-6">
                    <div class="social-link-share">
                        <ul>
                            <li><a target="_blank" href="https://www.facebook.com/akuaviajesyturismo/"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a target="_blank" href="https://www.instagram.com/akuaviajesyturismo/"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="single-details-box-section section-padding page-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-sm-12">
                    <div class="single-content-box-area">
                        <div class="description_box box" id="des-box">
                            <h3>Descripción</h3>
                            <p id="descripcion"></p>
                        </div>
                        <div class="menu_price box" id="menu-price">
                            <h3>Elije Una Opción</h3>
                            <div class="menu-item-price">
                                <ul id="opciones">
                                </ul>
                            </div>
                        </div>
                        <div class="photos_box box" id="photo-box">
                            <h3>Galeria</h3>
                            <div class="galeria" style="width: 100%"></div>
                        </div>
                        <div class="map_box box" id="map-box">
                            <h3>Ubicación</h3>
                            <div class="map-wrap">
                                <iframe src="https://maps.google.com/maps?q=cali%20colombia&t=&z=11&ie=UTF8&iwloc=&output=embed"  style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12">
                    <div class="single-sidebar-widgets">
                        <div class="single-widgets widgets box">
                            <h3>Incluye</h3>
                            <h6>Si</h6>
                            <ul id="incluyeSi">
                            </ul>
                            <br>
                            <h6>No</h6>
                            <ul id="incluyeNo">
                            </ul>
                        </div><!-- single widget end -->
                        <div class="single-widgets widgets box">
                            <h3>Contacto</h3>
                            <li><i class="far fa-map"></i>Cra. 30 No. 5A-79 San Fernando, Cali - CO</li>
                            <li><i class="far fa-envelope"></i>planes@akua.com.co</li>
                            <li><i class="fas fa-phone"></i>+57 (314) 8801607</li>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal Seleccionar Opcion -->
    <div class="modal fade" id="modalSeleccionOpcion" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Agregar Servicio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formSeleccionOpcion" class="search-form">
                        <label>Viaje</label>
                        <select id="viajes" name="fk_viajes" onchange="loadFechas(this)" required="true"></select>
                        <label>Fecha y hora del servicio</label>
                        <div class="form-row">
                            <div class="col">
                                <select class="listado_dias" name="fecha_inicio"></select>
                            </div>
                            <div class="col">
                                <input type="time" name="hora_inicio" required="true">
                            </div>
                        </div>
                        <label>Cantidad de noches</label>
                        <input id="cantidadNoches" type="number" placeholder="# Noches" value="1" name="cantidad" required="true" style="display: none;">
                        <input type="number" placeholder="# pasajeros" name="pasajeros" required="true">
                        <button type="input" form="formSeleccionOpcion">Agregar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Crear Viaje -->
    <div class="modal fade" id="modalCrearViaje" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Mi viaje</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formCrearViaje" class="search-form">
                        <input type="text" name="nombre" placeholder="Nombra tu viaje" required="true">
                        <label for="">Fecha del viaje</label>
                        <input type="date" name="fecha_inicio" required="true">
                        <input type="date" name="fecha_fin" required="true">
                        <input type="number" placeholder="# pasajeros" name="pasajeros" required="true">
                        <button type="input" form="formCrearViaje">Crear</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


<?php include 'footer.php'; ?>

<script type="text/javascript">
    var idService = <?=$_REQUEST['service']?>;
    var userLogin
    var opcion
    var tipoServicio

    function initLogin(user){
        userLogin = user.usuario
    }

    function loadFechas(select){
        let fechainicio = $(select).find(':selected').data('fechainicio')
        let fechafin = $(select).find(':selected').data('fechafin')

        let inicio = moment(fechainicio, 'YYYY-MM-DD')
        let fin = moment(fechafin, 'YYYY-MM-DD')
        let dias = fin.diff(inicio, 'days')

        //iteración para llenar combo de días
        for (let i = 0; i <= dias; i++) {
            $('.listado_dias').append("<option value="+inicio.format('YYYY-MM-DD')+">"+inicio.format('DD-MMM-YYYY')+"</option>")
            inicio.add(1, 'days')
        }
    }


    function seleccionarOpcion(idOpcion){
        if(userLogin != undefined){
            opcion = idOpcion
            procesarRegistro('viajes', 'getViajesXusuarioWeb', {'campo':'fk_clientes', 'valor':userLogin.id}, function(r){
                if (r.ejecuto) {
                    if (r.data.length != 0) {
                        $('#modalSeleccionOpcion').modal('show')
                        $('#modalSeleccionOpcion').on('shown.bs.modal', function (e) {
                            
                            if (tipoServicio == 3) {
                                $('#cantidadNoches').css('display', 'block')
                            }

                            let elemento = 'viajes'
                            $("#"+elemento).empty();
                            $("#"+elemento).append("<option value=''>Seleccione...</option>");
                            
                            for (i = 0; i < r.data.length; i++){
                                $("#"+elemento).append("<option value="+r.data[i].id+" data-fechainicio="+r.data[i].fecha_inicio+" data-fechafin="+r.data[i].fecha_fin+">"+r.data[i].nombre+"</option>")
                            }
                        })
                    }else{
                        $('#modalCrearViaje').modal('show')
                    }
                }
            })
        }else{
            swal('Ups!', 'Antes de continuar, recuerda iniciar sesión', 'warning')
        }
    }

    procesarRegistro('servicios', 'getServicios', {'id':idService}, function(r){
        let fila = ''

        if (r.ejecuto) {
            tipoServicio = r.data[0].idts
            fila += '<section class="banner-section bg-cover bg-overlay" style="background-image: url(https://picsum.photos/1900/700)">'+
                        '<div class="container">'+
                            '<div class="row">'+
                                '<div class="col-lg-6 col-sm-12">'+
                                    '<div class="banner-left-content d-flex align-items-center">'+
                                        '<div class="banner-icon">'+
                                            '<i class="text-light fa '+r.data[0].icono+'"></i>'+
                                        '</div>'+
                                        '<div class="banner-content">'+
                                            '<h3>'+r.data[0].servicio+'</h3>'+
                                            '<span><i class="fas fa-map"></i>'+r.data[0].destino+'</span>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-5 offset-lg-1 col-sm-12 text-lg-right">'+
                                    '<div class="banner-left-content">'+
                                        '<div class="all-reviews">'+
                                            '<span><i class="fas fa-star"></i></span>'+
                                            '<span><i class="fas fa-star"></i></span>'+
                                            '<span><i class="fas fa-star"></i></span>'+
                                            '<span><i class="fas fa-star"></i></span>'+
                                            '<span><i class="fas fa-star"></i></span>'+
                                            'Excelente, (3 Reseñas)'+
                                        '</div>'+
                                        '<div class="feedback-option">'+
                                            '<a href="#"><span><i class="far fa-heart"></i></span>Me gusta</a>'+
                                            '<a href="#"><span><i class="fas fa-share"></i></span>Compartir</a>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</section>'
            $('#header').append(fila)
            $('#descripcion').append(r.data[0].descripcion)
            $('#incluyeSi').append(r.data[0].incluye)
            $('#incluyeNo').append(r.data[0].no_incluye)
        }       
    })

    procesarRegistro('opciones', 'getOpciones', {'fk_servicios':idService}, function(r){
        let item = ''

        if (r.ejecuto) {
            for (var i = 0; i < r.data.length; i++) {
                item += '<li class="elejirOpcion" data-opcion="'+r.data[i].id+'">'+r.data[i].opcion+
                    '<span>'+
                        '$'+r.data[i].precio+ ' ' +
                        '<i class="fa fa-shopping-cart" aria-hidden="true"></i>'+
                    '</span>'+
                '</li>'
            }
            
            $('#opciones').append(item)

            $('.elejirOpcion').on('click', function(){
                let idOpcion = $(this).attr('data-opcion')
                seleccionarOpcion(idOpcion)
            })
        }       
    })

    procesarRegistro('galeria_servicios', 'getImagenes', {'fk_servicios':idService}, function(r){
        let item = ''

        if (r.ejecuto) {
            for (var i = 0; i < r.data.length; i++) {
                item += '<div class="img-responsive">'+
                            '<img src="'+r.data[i].url+'" style="margin:auto;" width="100%">'+
                        '</div>'
            }
            
            $('.galeria').append(item).slick({
                dots: true,
                infinite: true,
                speed: 500,
                fade: true,
                cssEase: 'linear'
            });
        }
    })

    $('#formCrearViaje').on('submit',function(e){
        e.preventDefault();
        let objFormulario = parsearFormulario($('#formCrearViaje')); 
        objFormulario.fk_clientes = userLogin.id
        $.ajax({
            url: '../class/frontController.php',
            data:{
                objeto: 'viajes',
                metodo: 'insertViajeWeb',
                datos: objFormulario
            },
            type: 'POST',
            dataType: 'json',
            success: function(r){
                if (r.ejecuto) {
                    $('#modalCrearViaje').modal('hide')
                    $('#modalCrearViaje').on('hidden.bs.modal', function (e) {
                        swal('Perfecto!', 'Ahora puedes agregar los servicios que necesites para tu viaje.', 'success')
                    })
                }else{
                    swal('Ups!', 'Se ha presentado un error: ' + r.msgError, 'error')
                }
            },
            error: function(xhr,status){
                alert('Disculpe, existio un problema procesando')
            },
            complete: function(xhr,status){
                $('#preload').modal('hide')
            }
        });            
    });

    $('#formSeleccionOpcion').on('submit',function(e){
        e.preventDefault();
        let objFormulario = parsearFormulario($('#formSeleccionOpcion'));
        objFormulario.fk_opciones = opcion
        $.ajax({
            url: '../class/frontController.php',
            data:{
                objeto: 'viajesDetalle',
                metodo: 'insert',
                datos: objFormulario
            },
            type: 'POST',
            dataType: 'json',
            success: function(r){
                if (r.ejecuto) {
                    $('#modalSeleccionOpcion').modal('hide')
                    $('#modalSeleccionOpcion').on('hidden.bs.modal', function (e) {
                        swal('Perfecto!', 'Perfecto.', 'success')
                    })
                }else{
                    swal('Ups!', 'Se ha presentado un error: ' + r.msgError, 'error')
                }
            },
            error: function(xhr,status){
                alert('Disculpe, existio un problema procesando')
            },
            complete: function(xhr,status){
                $('#preload').modal('hide')
            }
        });            
    });

</script>