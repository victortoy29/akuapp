<!-- footer section wrapper start -->
    <footer class="footer-section">
        <div class="footer-widgets-section">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-md-6 col-lg-3 col-sm-12">
                        <div class="single-footer-widget">
                            <div class="widget-title">
                                <h4>Akua</h4>
                            </div>
                            <p>Somos una agencia de viajes que brinda experiencias turísticas y de aventura en el Pacífico colombiano.</p>
                        </div>
                    </div>

                    <div class="col-xl-3 col-md-6 col-lg-3 col-sm-12">
                        <div class="single-footer-widget">
                            <div class="widget-title">
                                <h4>Dirrección</h4>
                            </div>
                            <p><strong>SEDE PRINCIPAL</strong>
                                Cra. 30 No. 5A-79 San Fernando<br>
                                <strong>Cali - Colombia</strong>
                                +57 (314) 8801607
                            planes@akua.com.co</p>
                        </div>
                    </div>

                    <div class="col-xl-3 col-md-6 col-lg-3 col-sm-12">
                        <div class="single-footer-widget">
                            <div class="widget-title">
                                <h4>Suscríbete</h4>
                            </div>
                            <div class="subscribe-box">
                                <form action="#">
                                    <input type="text" placeholder="Correo electrónico">
                                    <button type="submit">enviar</button>
                                </form>
                            </div>
                            <div class="social-icons">
                                <span>Síguenos</span>
                                <ul>
                                    <li><a href="https://www.facebook.com/akuaviajesyturismo/"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="https://www.instagram.com/akuaviajesyturismo/"><i class="fab fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="footer-bottom-section">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-sm-12">
                        <span>&copy;2019 AKUA - Agencia de Viajes y Turismo – Todos los derechos reservados</span>
                    </div>
                    <div class="col-xl-6 col-sm-12">
                        <div class="footer-menu">
                            <ul>
                                <li><a href="#">Inicio</a></li>
                                <li><a href="#">Akua</a></li>
                                <li><a href="#">Explora</a></li>
                                <li><a href="#">Contacto</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer section wrapper end -->
    
    <!--  ALl JS Plugins -->
    <script src="assets/js/jquery-1.12.4.min.js"></script>
    <script src="assets/js/jquery.easing.1.3.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/scrollUp.min.js"></script>
    <script src="assets/js/magnific-popup.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/slick.min.js"></script>
    <script src="assets/js/ion.rangeSlider.min.js"></script>
    <script src="assets/js/rater.min.js"></script>
    <script src="assets/js/main.js"></script>
    
    <!--Sweet Alert-->
    <script src="../bower_components/sweetalert/docs/assets/sweetalert/sweetalert.min.js"></script>

    <!-- moment -->
    <script src="../bower_components/moment/min/moment.min.js"></script>
    <script src="../bower_components/moment/locale/es.js"></script>
    
    <!-- datapicker -->
    <script src="assets/plugins/datapicker/js/bootstrap-datepicker.min.js"></script>
    <script src="assets/plugins/datapicker/locales/bootstrap-datepicker.es.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>

    <script src="assets/js/funciones.js"></script>

    <script type="text/javascript">
        $(function(){
            procesarRegistro('helpersWeb', 'sesion', {'':''}, function(r){
                var pagina = document.location.href.match(/[^\/]+$/)[0]
                if (r.ejecuto == true) {
                    if(!jQuery.isEmptyObject(r.data)){
                        //Cuando esta logueado
                        initLogin(r.data)
                        $('.nameUser').text(r.data.usuario.nombre)
                        $('#btnUser').css('display', 'inline')
                        $('#btnUser').attr('href', 'user.php?id='+r.data.usuario.id)
                        $('#btnSalir').css('display', 'inline')
                        $('#btnReserva').css('display', 'inline')
                        $('#btnLogueo').css('display', 'none')
                    }else{
                        $(".loader-wrap").css( "display", "block" );             
                        if (pagina != 'index.php') {
                            window.location.href = "index.php"
                        }
                    }
                }
            })

            $('#btnSalir').on('click',function(){
                procesarRegistro('helpersWeb', 'sesion', {destroy:1}, function(r){
                    if (r.ejecuto == true) {
                        window.location.href = "index.php"   
                    }
                })
            })
        })
    </script>
</body>
</html>