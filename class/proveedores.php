<?php
require_once 'database.php';
require_once 'model.php';

class proveedores extends model{
	protected $tabla = 'proveedores';

	public function getProveedoresPorFiltro($datos){
		$filtro = '1';
		if($datos['destino']==''){
			$filtro .= ' AND 1';
		}else{
			$filtro .= " AND fk_destinos = $datos[destino]";
		}

		if($datos['tipo_servicio']==''){
			$filtro .= ' AND 1';
		}else{
			$filtro .= " AND fk_tipos_servicio = $datos[tipo_servicio]";
		}		
		$sql = "SELECT
					proveedores.id,
					proveedores.nombre
				FROM 
					(proveedores INNER JOIN proveedores_oferta ON proveedores.id = proveedores_oferta.fk_proveedores)
					INNER JOIN proveedores_destinos ON proveedores.id = proveedores_destinos.fk_proveedores
				WHERE
					$filtro
				ORDER BY 
					proveedores.nombre";
		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}

	public function getSeviciosByProveedor($datos){
		$sql = "SELECT
					servicios.nombre,
					opciones.opcion,
					viajes.estado AS vestado,
					viajes.id AS idv,
					viajes.codigo_reserva AS cr,
					viajes_detalle.*
				FROM 
					(viajes_detalle 
					INNER JOIN viajes ON fk_viajes = viajes.id)
					INNER JOIN (opciones 
					INNER JOIN servicios ON fk_servicios = servicios.id) ON fk_opciones = opciones.id
				WHERE
					$datos[campo] = $datos[valor]
					AND viajes.estado IN ('Cotización', 'Reserva')
					AND viajes_detalle.estado IN ('Gestionar', 'Bloqueado', 'Reservado')
					AND viajes_detalle.fecha_inicio >= DATE(NOW())
				ORDER BY
					viajes_detalle.fecha_inicio,
					hora_inicio";
		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}

	public function getLike($datos){
		$sql = "SELECT 
					*
				FROM 
					proveedores
				WHERE (nombre LIKE '%$datos[valor]%'					
					OR numero LIKE '%$datos[valor]%')
					AND id != 1";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}
}