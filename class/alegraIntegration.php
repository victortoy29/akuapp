<?php

class alegraIntegration
{
    private $signature;
    private $urlBase;
    private $headers;

    public function __construct()
    {
        $this->signature = "marketing@akua.com.co:71123e49036d2e8a6d2f";
        $this->urlBase = "https://app.alegra.com/api/v1/";
        $this->headers = [
            "Content-type: application/json",
            "Accept: application/json"
        ];
    }
    
    public function createContact(array $data)
    {    
        try {
            if (!isset($data['name'])) {
                throw new invalidargumentexception("El nombre es requerido");
            }
            $this->urlBase .= "contacts";
        
            $response = $this->execCurl($data, true);
            $response = json_decode($response);
   
            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return $response;
        } catch (\invalidargumentexception $exc) {
            return $exc->getMessage();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }


    public function createService(array $data)
    {
        try {
            if (!isset($data['name'])) {
                throw new invalidargumentexception("El nombre es requerido");
            }
            if (!isset($data['price'])) {
                throw new invalidargumentexception("El precio es requerido");
            }

            $this->urlBase .= "items";
        
            $response = $this->execCurl($data, true);
            $response = json_decode($response);
   
            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return $response;
        } catch (\invalidargumentexception $exc) {
            return ["status" => false, "data" => $exc->getMessage()];
        } catch (\Throwable $th) {
            return ["status" => false, "data" => $th->getMessage()];
        }
    }

    public function createBill(array $data)
    {
        try {
            if (!isset($data['date'])) {
                throw new invalidargumentexception("La fecha es requerida");
            }
            if (!isset($data['dueDate'])) {
                throw new invalidargumentexception("La fecha de vencimiento es requerida");
            }

            if (!isset($data['client'])) {
                throw new invalidargumentexception("El cliente es requerido");
            }

            if (!isset($data['items']) && count($data['items']) > 0) {
                throw new invalidargumentexception("Debes tener al menos un item");
            }
            $this->urlBase .= "invoices";
        
            $response = $this->execCurl($data, true);
            $response = json_decode($response);
   
            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return ["status" => true, "data" => $response];
        } catch (\invalidargumentexception $exc) {
            return ["status" => false, "data" => $exc->getMessage()];
        } catch (\Throwable $th) {
            return ["status" => false, "data" => $th->getMessage()];
        }
    }

    public function createPay(array $data)
    {
        try {
            if (!isset($data['date'])) {
                throw new invalidargumentexception("La fecha es requerida");
            }
            if (!isset($data['bankAccount'])) {
                throw new invalidargumentexception("El banco es requerido");
            }

            if (!isset($data['items']) && count($data['items']) > 0) {
                throw new invalidargumentexception("Debes tener al menos un item");
            }
            
            $this->urlBase .= "payments";
        
            $response = $this->execCurl($data, true);
            $response = json_decode($response);
   
            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return ["status" => true, "data" => $response];
        } catch (\invalidargumentexception $exc) {
            return ["status" => false, "data" => $exc->getMessage()];
        } catch (\Throwable $th) {
            return ["status" => false, "data" => $th->getMessage()];
        }
    }

    public function createBillProvider(array $data)
    {
        try {
            if (!isset($data['date'])) {
                throw new invalidargumentexception("La fecha es requerida");
            }
            if (!isset($data['dueDate'])) {
                throw new invalidargumentexception("La fecha de vencimiento es requerida");
            }

            if (!isset($data['provider'])) {
                throw new invalidargumentexception("El proveedor es requerido");
            }

            if (!isset($data['purchases']) && count($data['purchases']) > 0) {
                throw new invalidargumentexception("Debes tener al menos un item");
            }
            $this->urlBase .= "bills";
        
            $response = $this->execCurl($data, true);
            $response = json_decode($response);
   
            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return ["status" => true, "data" => $response];
        } catch (\invalidargumentexception $exc) {
            return ["status" => false, "data" => $exc->getMessage()];
        } catch (\Throwable $th) {
            return ["status" => false, "data" => $th->getMessage()];
        }
    }

    public function deleteContact($id)
    {
        try {
            if (!isset($id)) {
                throw new invalidargumentexception("El identificador del cliente es requerido");
            }

            $this->urlBase .= "contacts/".$id;
        
            $response = $this->execCurl([], false, false, false, true);
            $response = json_decode($response);

            return ["status" => true, "data" => $response];
        } catch (\invalidargumentexception $exc) {
            return ["status" => false, "data" => $exc->getMessage()];
        } catch (\Throwable $th) {
            return ["status" => false, "data" => $th->getMessage()];
        }
    }

    public function editContact($id, $data)
    {
        try {
            if (!isset($id)) {
                throw new invalidargumentexception("El identificador del cliente es requerido");
            }

            $this->urlBase .= "contacts/".$id;
        
            $response = $this->execCurl($data, false, false, true);
            $response = json_decode($response);

            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return ["status" => true, "data" => $response];
        } catch (\invalidargumentexception $exc) {
            return ["status" => false, "data" => $exc->getMessage()];
        } catch (\Throwable $th) {
            return ["status" => false, "data" => $th->getMessage()];
        }
    }

    public function editService($id, $data)
    {
        try {
            if (!isset($id)) {
                throw new invalidargumentexception("El identificador del cliente es requerido");
            }

            $this->urlBase .= "items/".$id;
        
            $response = $this->execCurl($data, false, false, true);
            $response = json_decode($response);

            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return ["status" => true, "data" => $response];
        } catch (\invalidargumentexception $exc) {
            return ["status" => false, "data" => $exc->getMessage()];
        } catch (\Throwable $th) {
            return ["status" => false, "data" => $th->getMessage()];
        }
    }

    public function editBill($id, $data)
    {
        try {
            if (!isset($id) || $id == "") {
                throw new invalidargumentexception("El identificador de la factura es requerido");
            }

            $this->urlBase .= "invoices/".$id;
        
            $response = $this->execCurl($data, false, false, true);
            $response = json_decode($response);

            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return ["status" => true, "data" => $response];
        } catch (\invalidargumentexception $exc) {
            return ["status" => false, "data" => $exc->getMessage()];
        } catch (\Throwable $th) {
            return ["status" => false, "data" => $th->getMessage()];
        }
    }
    
    public function editPay($id, $data)
    {
        try {
            if (!isset($id) || $id == "") {
                throw new invalidargumentexception("El identificador de la factura es requerido");
            }

            $this->urlBase .= "payments/".$id;
        
            $response = $this->execCurl($data, false, false, true);
            $response = json_decode($response);

            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return ["status" => true, "data" => $response];
        } catch (\invalidargumentexception $exc) {
            return ["status" => false, "data" => $exc->getMessage()];
        } catch (\Throwable $th) {
            return ["status" => false, "data" => $th->getMessage()];
        }
    }

    public function cancelPay($id)
    {
        try {
            if (!isset($id) || $id == "") {
                throw new invalidargumentexception("El identificador de la factura es requerido");
            }

            $this->urlBase .= "payments/".$id."/void";
        
            $response = $this->execCurl([], true);
            $response = json_decode($response);

            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return ["status" => true, "data" => $response];
        } catch (\invalidargumentexception $exc) {
            return ["status" => false, "data" => $exc->getMessage()];
        } catch (\Throwable $th) {
            return ["status" => false, "data" => $th->getMessage()];
        }
    }

    public function cancelBill($id)
    {
        try {
            if (!isset($id) || $id == "") {
                throw new invalidargumentexception("El identificador de la factura es requerido");
            }

            $this->urlBase .= "invoices/".$id."/void";
        
            $response = $this->execCurl([], true);
            $response = json_decode($response);

            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return ["status" => true, "data" => $response];
        } catch (\invalidargumentexception $exc) {
            return ["status" => false, "data" => $exc->getMessage()];
        } catch (\Throwable $th) {
            return ["status" => false, "data" => $th->getMessage()];
        }
    }

    public function findContact($id)
    {
        try {
            if (!isset($id) || $id == "") {
                throw new invalidargumentexception("El identificador del cliente es requerido");
            }
            $this->urlBase .= "contacts/".$id;
        
            $response = $this->execCurl();
            $response = json_decode($response);

            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }
            
            return ["status" => true, "data" => $response];
        } catch (\invalidargumentexception $exc) {
            return ["status" => false, "data" => $exc->getMessage()];
        } catch (\Throwable $th) {
            return ["status" => false, "data" => $th->getMessage()];
        }
    }  

    public function execCurl($data = [], $flagPost = false, $flagGet = false, $flagPut = false, $flagDelete = false)
    {
        $ch = \curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->urlBase);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($flagPost == true) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }
        if ($flagPut == true) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }
        if ($flagDelete == true) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_USERPWD, $this->signature);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}
