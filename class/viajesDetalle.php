<?php
require_once 'database.php';
require_once 'model.php';
require_once 'plantillasDetalle.php';
require_once 'opciones.php';
require_once 'viajes.php';

class viajesDetalle extends model{
	protected $tabla = 'viajes_detalle';

	public function getDetalle($datos){
		$filtro = '';
		if(isset($datos['estado']) and $datos['estado'] == 'Activo'){
			$filtro = "AND viajes_detalle.estado IN ('Gestionar', 'Bloqueado', 'Reservado', 'Devuelto') AND fk_viajes = $datos[viaje]";
		}else{
			foreach ($datos as $key => $value) {
				$filtro .= " AND viajes_detalle.$key = '$value'";
			}
		}
		$sql = "SELECT 
					viajes_detalle.id,
					destinos.nombre AS destino,
					um,
					icono,
					servicios.id AS ids,
					servicios.fk_tipos_servicio AS ts,
					servicios.nombre AS servicio,
					servicios.descripcion AS servicio_descripcion,
					servicios.recomendaciones AS servicio_recomendaciones,
					servicios.incluye AS servicio_incluye,
					servicios.no_incluye AS servicio_noincluye,
					opciones.opcion,
					proveedores.id AS idp,
					proveedores.nombre AS proveedor,
					viajes_detalle.fecha_inicio,
					viajes_detalle.hora_inicio,
					viajes_detalle.detalle,					
					viajes_detalle.costo,
					viajes_detalle.precio,
					viajes_detalle.descuento,
					viajes_detalle.pasajeros,
					viajes_detalle.cantidad,					
					viajes_detalle.estado,
					viajes_detalle.observaciones,
					viajes_detalle.fk_viajes as idv,
					viajes_detalle.calificacion,
					viajes_detalle.observacion,
					viajes_detalle.fk_opciones
				FROM
					viajes_detalle 
					INNER JOIN (opciones 
					INNER JOIN (((servicios 
					INNER JOIN tipos_servicio ON fk_tipos_servicio = tipos_servicio.id) 
					INNER JOIN proveedores ON fk_proveedores = proveedores.id) 
					INNER JOIN destinos ON fk_destinos = destinos.id) ON fk_servicios = servicios.id) ON fk_opciones = opciones.id
				WHERE 1 $filtro					
				ORDER BY
					viajes_detalle.fecha_inicio,
					viajes_detalle.hora_inicio";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function getItinerario($datos){
		$sql = "SELECT 
					viajes_detalle.id,
					viajes_detalle.fecha_inicio,
					viajes_detalle.hora_inicio,
					destinos.id AS idDestino,
					destinos.nombre AS destino,
					destinos.descripcion AS desDestino,
					servicios.fk_tipos_servicio AS ts,
					servicios.id AS idServicio,
					servicios.nombre AS servicio,
					servicios.descripcion,
					servicios.incluye,
					servicios.no_incluye,
					servicios.recomendaciones,
					proveedores.nombre AS proveedor,
					opciones.opcion,
					opciones.dias_en_itinerario AS di,					
					viajes_detalle.detalle,
					viajes_detalle.pasajeros,
					viajes_detalle.cantidad,
					viajes_detalle.estado
				FROM
					viajes_detalle 
					INNER JOIN (opciones 
					INNER JOIN (((servicios 
					INNER JOIN tipos_servicio ON fk_tipos_servicio = tipos_servicio.id) 
					INNER JOIN proveedores ON fk_proveedores = proveedores.id) 
					INNER JOIN destinos ON fk_destinos = destinos.id) ON fk_servicios = servicios.id) ON fk_opciones = opciones.id
				WHERE 
					viajes_detalle.estado IN ('Gestionar', 'Bloqueado', 'Reservado') 
					AND fk_viajes = $datos[viaje]
				ORDER BY					
					viajes_detalle.fecha_inicio,
					viajes_detalle.hora_inicio";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function getIndicador($datos){
		$sql = "SELECT
					fk_viajes,
					estado,
					COUNT(1) AS cantidad
				FROM 
					viajes_detalle
				WHERE 
					estado IN ('Gestionar','Bloqueado', 'Reservado','Devuelto')
				GROUP BY 
					fk_viajes,estado
				ORDER BY 
					fk_viajes";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function cargarPlantilla($datos){
		$respuesta = [];		
		$inicio = '';
		$adicion = '';
		$plantilla = new plantillasDetalle();
		$registros = $plantilla->select(['fk_plantillas'=>$datos['plantilla']]);
		foreach ($registros['data'] as $value) {
			$adicion = ' + '.($value['dia']-1).' days';
			$inicio = date('Y-m-d', strtotime($datos['inicio_plantilla'].$adicion));
			$info = [
				'fk_viajes' 	=> $datos['viaje'],
				'fecha_inicio'	=> $inicio,
				'hora_inicio'	=> $value['hora_inicio'],
				'fk_opciones'	=> $value['fk_opciones'],
				'costo'			=> $value['costo'],
				'precio'		=> $value['precio'],
				'pasajeros'		=> $value['pasajeros'],
				'cantidad'		=> $value['cantidad']
			];
			$respuesta = parent::insert($info);
		}
		return $respuesta;
	}

	public function cargarPlantillaWeb($datos){

		$respuesta = [];		
		$inicio = '';
		$adicion = '';
	
		$registros = $datos['data'];
		$viaje = $datos['dataViaje'];

		$dbViaje = new viajes();
		$resDbViaje = $dbViaje->insertViajeWeb($viaje);

		if ($resDbViaje['ejecuto']) {
			foreach ($registros as $value) {
				$adicion = ' + '.($value['dia']-1).' days';
				$inicio = date('Y-m-d', strtotime($viaje[0]['value'].$adicion));
				$info = [
					'fk_viajes' 	=> $resDbViaje['insertId'],
					'fecha_inicio'	=> $inicio,
					'hora_inicio'	=> $value['hora_inicio'],
					'fk_opciones'	=> $value['fk_opciones'],
					'costo'			=> $value['costo'],
					'precio'		=> $value['precio'],
					'pasajeros'		=> $value['pasajeros'],
					'cantidad'		=> $value['cantidad']
				];
				$respuesta = parent::insert($info);
			}
			return $respuesta;
		}
	}

	public function getValorTotal($datos){
		$sql = "SELECT
					fk_viajes,
					SUM((precio-(precio*(descuento/100)))*pasajeros*cantidad) AS valor_total
				FROM
					viajes_detalle
				WHERE
					fk_viajes = $datos[id]
					AND viajes_detalle.estado != 'Cancelado'
				GROUP BY 
					fk_viajes";
		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}

	public function updateCalificacion($datos)
	{
    	for ($i=0; $i < count($datos); $i++) {
    		$sql = "UPDATE viajes_detalle SET ";
	    	foreach ($datos[$i] as $clave=>$valor)
	    	{       	
	    		if ($clave != 'id') {
	    			$sql .= "$clave = '$valor',";
	    		}
	    	}
	    	
			$sql .= "modificado_por =".$_SESSION['usuario']['id'].",fecha_modificacion=NOW()";
    		$sql .= " WHERE id = ".$datos[$i]['id'];

			$db = new database();
	    	$resUpdateCalificacion = $db->ejecutarConsulta($sql);

	    	if ($resUpdateCalificacion['ejecuto'] == 1) {
	    		// Consulta y almacena el id del servicio, al cual se le actualizara el campo
		    	// correspondiente al promedio de calificación.

		    	$sqlIdServicio = "SELECT fk_servicios FROM `opciones` WHERE `id` = '".$datos[$i]['fk_opciones']."'";

		    	$db = new database();
		    	$idServicio = $db->ejecutarConsulta($sqlIdServicio);
		    	// Se obtiene el promedio (AVG) de la calificación para un servicio determinado,
		    	// para ello se hace una pre-busqueda de todas las opciones que compartan el
		    	// mismo servicio, teniendo como referencia la opción que se esta calificando.
		    	// ($datos[$i]['opcion'])
			    
			    $superQuery = "	SELECT ROUND(AVG(calificacion),1) AS 'avg' FROM `viajes_detalle`
			    				WHERE `fk_opciones` IN (
			    					SELECT id FROM `opciones` WHERE `fk_servicios` = ('".$idServicio['data'][0]['fk_servicios']."')";

			    $db = new database();
		    	$resSuperQuery = $db->ejecutarConsulta($superQuery);

		    	if ($resSuperQuery['ejecuto'] == 1) {
		    		$nuevoScore = $resSuperQuery['data'][0]['avg'];
		    		$sqlUpdateScore = "UPDATE `servicios` SET `promedio_calificaciones` = '".$nuevoScore."' WHERE `servicios`.`id` = '".$idServicio['data'][0]['fk_servicios']."'";

		    		$db = new database();
		    		$idServicio = $db->ejecutarConsulta($sqlUpdateScore);
		    	}
	    	}
	    }

	    return json_decode('{"ejecuto":true}');
	}
}

// 