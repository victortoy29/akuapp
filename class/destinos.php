<?php
require_once 'database.php';
require_once 'model.php';

class destinos extends model{
	protected $tabla = 'destinos';

	public function getDestinos($datos){
		$sql = "SELECT
					destinos.id,
					departamentos.nombre AS departamento,
					zonas.nombre AS zona,
					destinos.nombre AS destino,
					destinos.descripcion
				FROM 
					destinos 
						INNER JOIN (zonas 
						INNER JOIN departamentos ON fk_departamentos = departamentos.id) ON fk_zonas = zonas.id
				WHERE 1 ";
		foreach ($datos as $key => $value) {
			$sql .= "AND destinos.$key = '$value' ";
		}
		$sql .=	"ORDER BY destinos.nombre";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}
}