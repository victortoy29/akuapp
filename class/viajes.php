<?php
require_once 'database.php';
require_once 'model.php';

class viajes extends model{
	protected $tabla = 'viajes';

	public function getViajes($datos){
		$sql = "SELECT
					viajes.id,
					viajes.codigo_reserva AS cr,
					clientes.fk_tipos_cliente,
					clientes.nombre AS cliente,
					clientes.tipo_doc,
					clientes.numero,
					viajes.nombre_viaje,
					viajes.pasajeros,
					viajes.fecha_inicio,
					viajes.fecha_fin,
					viajes.opcionales,
					viajes.estado,
					viajes.fecha_creacion,
					usuarios.id AS idVendedor,
					usuarios.nombre AS vendedor,
					usuarios.cargo,
					usuarios.telefono,
					usuarios.correo,
					usuarios.skype,
					DATEDIFF(fecha_fin,now()) AS diasRestantes
				FROM (viajes 
					INNER JOIN usuarios ON viajes.creado_por = usuarios.id) 
					INNER JOIN clientes ON fk_clientes = clientes.id
				WHERE viajes.$datos[campo] = '$datos[valor]'
					AND viajes.id != 1
				ORDER BY viajes.fecha_inicio";				
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function getViajesXusuarioWeb($datos){
		$sql = "SELECT
					viajes.id,
					viajes.nombre,
					viajes.estado,
					viajes.fecha_inicio,
					viajes.fecha_fin
				FROM viajes
					INNER JOIN clientes ON fk_clientes = clientes.id
				WHERE viajes.$datos[campo] = '$datos[valor]'
					AND viajes.id != 1";
				
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function crearReserva($datos){
		//Primero se crea el codigo de reserva, este se hace autoincremental
		$sql = "UPDATE 
					viajes AS dest,
					(SELECT MAX(codigo_reserva)+1 AS codigo FROM viajes) AS src
				SET 
					dest.codigo_reserva = src.codigo,
					dest.estado = 'Reserva'
				WHERE dest.id = $datos[id]";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function insertViajeWeb($datos)
	{	

		$sql = "INSERT INTO viajes SET ";
    	foreach ($datos as $clave=>$valor)
    	{
    		if ($valor['name'] != 'dias') {
    			$sql .= $valor['name'] . " = '" . $valor['value'] . "',";
    		}
    	}

		$sql .= "web=1,creado_por =1,fecha_creacion=NOW(),fecha_modificacion=NOW()";

		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function guardarOpcionales($datos){
		$info = [
			'id' => $datos['id'],
			'opcionales' =>	json_encode($datos['opcionales'], JSON_UNESCAPED_UNICODE)
		];
		return parent::update($info);
	}
}