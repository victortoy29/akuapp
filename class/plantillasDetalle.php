<?php
require_once 'database.php';
require_once 'model.php';

class plantillasDetalle extends model{
	protected $tabla = 'plantillas_detalle';

	public function getDetalle($datos){
		$sql = "SELECT 
					plantillas_detalle.id,
					destinos.nombre AS destino,
					um,					
					servicios.nombre AS servicio,
					opciones.opcion,
					plantillas_detalle.dia,
					plantillas_detalle.hora_inicio,
					plantillas_detalle.costo,
					plantillas_detalle.precio,
					plantillas_detalle.pasajeros,
					plantillas_detalle.cantidad
				FROM
					plantillas_detalle 
					INNER JOIN (opciones 
					INNER JOIN ((servicios INNER JOIN tipos_servicio ON fk_tipos_servicio = tipos_servicio.id)
					INNER JOIN destinos ON fk_destinos = destinos.id) ON fk_servicios = servicios.id) ON fk_opciones = opciones.id
				WHERE 1";
		foreach ($datos as $key => $value) {
			$sql .= " AND plantillas_detalle.$key = '$value'";
		}					
		$sql .= " ORDER BY
					plantillas_detalle.dia,
					plantillas_detalle.hora_inicio";
		//echo $sql;
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function delete($datos){
		$sql = "DELETE FROM plantillas_detalle WHERE id = $datos[id]";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function getValorTotal($datos){
		$sql = "SELECT
					fk_plantillas,
					SUM(precio*pasajeros*cantidad) AS valor_total
				FROM
					plantillas_detalle
				WHERE
					fk_plantillas = $datos[id]
				GROUP BY 
					fk_plantillas";
		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}

	public function getItinerario($datos){		
		$sql = "SELECT 
					plantillas_detalle.id,
					plantillas_detalle.dia,
					plantillas_detalle.hora_inicio,
					dd.id AS idDestino,
					dd.nombre AS destino,
					dd.descripcion AS desDestino,
					servicios.fk_tipos_servicio AS ts,
					servicios.id AS idServicio,
					servicios.nombre AS servicio,
					opciones.opcion,
					dll.nombre AS llegada,
					dll.id AS idLlegada,
					opciones.duracion,
					opciones.dias_en_itinerario AS di,
					plantillas_detalle.pasajeros,
					plantillas_detalle.cantidad,
					plantillas_detalle.fk_plantillas,
					plantillas_detalle.costo,
					plantillas_detalle.precio,
					plantillas_detalle.fk_opciones
				FROM
					plantillas_detalle 
					INNER JOIN ((opciones INNER JOIN destinos AS dll ON fk_llegada = dll.id) 
					INNER JOIN ((servicios INNER JOIN tipos_servicio ON fk_tipos_servicio = tipos_servicio.id)
					INNER JOIN destinos AS dd ON fk_destinos = dd.id) ON fk_servicios = servicios.id) ON fk_opciones = opciones.id
				WHERE
					fk_plantillas = $datos[plantilla]
				ORDER BY
					plantillas_detalle.dia,
					plantillas_detalle.hora_inicio";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}
}