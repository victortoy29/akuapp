<?php
require_once 'database.php';
require_once 'model.php';

class ciudades extends model{
	protected $tabla = 'ciudades';

	public function getCiudades($datos){
		$sql = "SELECT
					ciudades.id,
					paises.nombre AS pais,
					ciudades.nombre
				FROM ciudades INNER JOIN paises ON fk_paises = paises.id
				WHERE 1 ";
				foreach ($datos as $key => $value) {
					$sql .= "AND ciudades.$key = '$value' ";
				}
		$sql .=	"ORDER BY nombre";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}
}