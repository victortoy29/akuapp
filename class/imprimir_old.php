<?php
require_once 'viajes.php';
require_once 'viajesDetalle.php';
require_once '../plugins/fpdf/fpdf.php';

$objViaje = new viajes();
$viaje = $objViaje->getViajes(['campo'=>'id', 'valor'=> $_GET['idV']]);
$opcionales = json_decode(str_replace("\r\n", "\\n", $viaje['data'][0]['opcionales']));
//print_r($opcionales);

$objDetalle = new viajesDetalle();
$detalle = $objDetalle->getDetalle(['viaje'=> $_GET['idV'], 'estado'=>'Activo']);
//print_r($detalle);
//exit();
$total = $objDetalle->getValorTotal(['id'=> $_GET['idV']]);

setlocale(LC_ALL,"es_ES");
$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','',16);
$pdf->Image('../assets/img/logo.png',20,30,50);
$pdf->SetY(70);
$pdf->Cell(190,10,'Santiago de Cali, '.strftime("%d de %B del %Y"),0,1);
$pdf->Ln(30);
$pdf->SetFont('Arial','B',16);
$pdf->SetTextColor(0,156,199);
$pdf->Cell(190,10,utf8_decode('Cliente'),0,1);
$pdf->SetFont('Arial','',14);
$pdf->SetTextColor(0,0,0);
$pdf->SetX(30);
$pdf->Cell(190,10,utf8_decode($viaje['data'][0]['cliente']),0,1);
$pdf->Ln();

$pdf->SetFont('Arial','B',16);
$pdf->SetTextColor(0,156,199);
$pdf->Cell(190,10,utf8_decode('Información general'),0,1);
$pdf->SetFont('Arial','',16);
$pdf->SetTextColor(0,0,0);
$pdf->SetX(30);
$pdf->Cell(40,10,utf8_decode('Código:'));
$pdf->Cell(30,10,'C-'.$viaje['data'][0]['id'],0,1);
$pdf->SetX(30);
$pdf->Cell(40,10,'Fecha inicio:');
$pdf->Cell(30,10,$viaje['data'][0]['fecha_inicio'],0,1);
$pdf->SetX(30);
$pdf->Cell(40,10,'Fecha fin:');
$pdf->Cell(30,10,$viaje['data'][0]['fecha_fin'],0,1);
$pdf->SetX(30);
$pdf->Cell(40,10,'Pasajeros:');
$pdf->Cell(30,10,$viaje['data'][0]['pasajeros'],0,1);
$pdf->Image('../assets/img/linea.png',0,252,207);


$pdf->SetDrawColor(34,143,252);
$pagina = 0;
foreach ($detalle['data'] as $value) {
	if($pagina % 2 == 0){
		$pdf->AddPage();
		$pdf->SetY(20);
	}
	$y = $pdf->GetY();
	if(file_exists('../assets/img/servicios/'.$value['ids'].'.jpg')){
		$pdf->Cell(90,50,$pdf->Image('../assets/img/servicios/'.$value['ids'].'.jpg',$pdf->GetX(),$pdf->GetY(),90,50),0,1);
	}else{
		$pdf->Cell(90,50,'No existe imagen',1,1,'C');
	}
	$pdf->SetXY(100, $y);
	//Titulo
	$pdf->SetFont('Arial','B',14);
	$pdf->MultiCell(95,10,utf8_decode($value['servicio']));
	
	for ($i=0; $i < count($opcionales); $i++) {
	 	if($value['id'] == $opcionales[$i]->id and isset($opcionales[$i]->opciones)){
	 		foreach ($opcionales[$i]->opciones as $opcion) {
	 			if($opcion == 'descripcion'){
					$pdf->SetX(100);
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(95,5,utf8_decode('Descripción'),0,1);
					$pdf->SetX(100);
					$pdf->SetFont('Arial','',10);
					$pdf->MultiCell(95,5,utf8_decode($value['servicio_descripcion']));
	 			}
	 			if($opcion == 'recomendaciones'){
	 				$pdf->SetX(100);
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(95,5,'Recomendaciones',0,1);
					$pdf->SetX(100);
					$pdf->SetFont('Arial','',10);
					$pdf->MultiCell(95,5,utf8_decode($value['servicio_recomendaciones']));
	 			}
	 			if($opcion == 'incluye'){
					$pdf->SetX(100);
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(95,5,'Incluye',0,1);
					$pdf->SetX(100);
					$pdf->SetFont('Arial','',10);
					$pdf->MultiCell(95,5,utf8_decode($value['servicio_incluye']));
	 			}
	 			if($opcion == 'no_incluye'){
					$pdf->SetX(100);
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(95,5,'No incluye',0,1);
					$pdf->SetX(100);
					$pdf->SetFont('Arial','',10);
					$pdf->MultiCell(95,5,utf8_decode($value['servicio_noincluye']));
	 			}
	 		}
	 	}
	}
	$pdf->Line(10,145,200,145);
	$pdf->SetY(150);
	$pagina++;
}

$pdf->AddPage();
$pdf->SetFont('Arial','B',15);
$pdf->SetTextColor(0,156,199);
$pdf->Cell(190,15,utf8_decode('Información de precio'),0,1);
$pdf->SetFont('Arial','B',12);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(150,10,'PRECIO PAX',0,0,'R');
$pdf->Cell(40,10,'$'.number_format(($total['data'][0]['valor_total']/$viaje['data'][0]['pasajeros']),0,',','.'),0,1,'R');
$pdf->Cell(150,10,'PRECIO TOTAL',0,0,'R');
$pdf->Cell(40,10,'$'.number_format($total['data'][0]['valor_total'],0,',','.'),0,1,'R');
$pdf->Ln();

//Nota aclaratoria
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(255,0,0);
$pdf->Cell(190,5,'NOTA',0,1,'C');
$pdf->MultiCell(190,5,utf8_decode('TARIFAS SUJETAS A CAMBIO Y A DISPONIBILIDAD SIN PREVIO AVISO, PARA PAGOS CON TARJETA DE CRÉDITO SE APLICARÁ SUPLEMENTO'),0,'C');
$pdf->Ln();

//Sección de información de la cuenta
$pdf->SetFont('Arial','B',12);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(190,7,utf8_decode('INFORMACIÓN GENERAL'),0,1);
$pdf->SetFont('Arial','',10);
$pdf->Cell(190,7,utf8_decode('Realizar consignación en la siguiente cuenta:'),0,1);
$pdf->Cell(30,7,'Banco:');
$pdf->Cell(50,7,'Davivienda',0,1);
$pdf->Cell(30,7,'Tipo:');
$pdf->Cell(50,7,'Cuenta de ahorros',0,1);
$pdf->Cell(30,7,utf8_decode('Número:'));
$pdf->Cell(50,7,'0127 7002 1108',0,1);
$pdf->Cell(30,7,'Beneficiario:');
$pdf->Cell(50,7,utf8_decode('Diana María Guevara B'),0,1);
$pdf->Cell(30,7,'CC:');
$pdf->Cell(50,7,'31.576.329',0,1);
$pdf->Ln(2);
$pdf->MultiCell(190,5,utf8_decode('Una vez realizada la consignación, enviar comprobante de pago al correo electrónico asisgerencia@akua.com.co para realizar la reserva.'));
$pdf->Ln(2);
$pdf->MultiCell(190,5,utf8_decode('Nota: Tenga en cuenta que los horarios están sujetos a cambios por estados de mareas y clima.'));
$pdf->Ln(10);

$pdf->Image('../assets/img/usuarios/'.$viaje['data'][0]['idVendedor'].'.jpg',15,$pdf->GetY(),60);
$pdf->SetFont('Arial','B',14);
$pdf->SetTextColor(0,156,199);
$pdf->SetX(80);
$pdf->Cell(120,10,utf8_decode($viaje['data'][0]['vendedor']),0,1);
$pdf->SetFont('Arial','',12);
$pdf->SetTextColor(0,0,0);
$pdf->SetX(80);
$pdf->Cell(130,10,'Akua agencia de viajes',0,1);
$pdf->SetX(80);
$pdf->Cell(130,10,'Cra 30 No. 5A - 79, San fernando, Cali',0,1);
$pdf->SetX(80);
$pdf->Cell(40,10,utf8_decode('Teléfono:'));
$pdf->Cell(90,10,$viaje['data'][0]['telefono'],0,1);
$pdf->SetX(80);
$pdf->Cell(40,10,utf8_decode('Correo electrónico:'));
$pdf->Cell(90,10,$viaje['data'][0]['correo'],0,1);
$pdf->SetX(80);
$pdf->Cell(40,10,'Sitio web:');
$pdf->Cell(90,10,'http://akua.com.co',0,1);
$pdf->Image('../assets/img/linea.png',0,252,207);

$pdf->Output();
?>