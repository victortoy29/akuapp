
<?php
require_once 'database.php';
require_once 'model.php';
require_once 'viajesDetalle.php';
require_once 'plantillasDetalle.php';

class plantillas extends model{
	protected $tabla = 'plantillas';

	public function crear($datos){
		//selecciono la información del viaje
		$resultado = parent::insert([
				'nombre' => $datos['nombre'],
				'pasajeros' => $datos['pasajeros'],
				'dias'=> $datos['dias']
			]
		);
		if($resultado['ejecuto']){
			//selecciono el detalle del viaje
			$respuesta = [];
			$dia = 0;
			$inicio = new DateTime($datos['fecha_inicio']);
			$viajesDetalle = new viajesDetalle();
			$r_viajesDetalle = $viajesDetalle->select(['fk_viajes' => $datos['viaje']]);
			$plantillasDetalle = new plantillasDetalle();
			foreach ($r_viajesDetalle['data'] as $value) {
				//Calcular días				
				$actual = new DateTime($value['fecha_inicio']);
				$dia = $actual->diff($inicio)->format("%a");				
				$info = [
					'fk_plantillas' => $resultado['insertId'],
					'dia'			=> $dia+1,
					'hora_inicio'	=> $value['hora_inicio'],
					'fk_opciones'	=> $value['fk_opciones'],
					'costo'			=> $value['costo'],
					'precio'		=> $value['precio'],
					'pasajeros'		=> $value['pasajeros'],
					'cantidad'		=> $value['cantidad']
				];
				$respuesta = $plantillasDetalle->insert($info);
			}
			return $respuesta;
		}
	}

	public function getPlantillas($datos){
		/*$sql = "SELECT 
					plantillas.id,
					plantillas.nombre,
					plantillas.descripcion,
					plantillas.pasajeros,
					plantillas.dias,
					plantillas.promocion,
					plantillas.incluye,
					plantillas.no_incluye
				FROM
					plantillas
				WHERE 1";*/

		$sql = "SELECT plantillas_detalle.fk_plantillas, ROUND(AVG(servicios.promedio_calificaciones),1) as AVG,plantillas.id,plantillas.nombre,plantillas.descripcion,plantillas.pasajeros,plantillas.dias,plantillas.promocion,plantillas.incluye,plantillas.no_incluye FROM `plantillas_detalle` INNER JOIN opciones ON opciones.id = plantillas_detalle.fk_opciones INNER JOIN servicios ON servicios.id = opciones.fk_servicios INNER JOIN plantillas ON plantillas.id = plantillas_detalle.fk_plantillas WHERE 1";

		foreach ($datos as $key => $value) {
			if ($key != 'dias' && $value != '') {
				$sql .= " AND plantillas.$key = '$value'";
			}
		}

		if ($datos['dias'] != '') {
			$sql .= " AND plantillas.dias = " . $datos['dias'];
		}

		$sql .= " GROUP BY plantillas_detalle.fk_plantillas";

		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	/*public function getScore($datos){
		$sql = "SELECT plantillas_detalle.fk_plantillas, ROUND(AVG(servicios.promedio_calificaciones),1) as AVG FROM `plantillas_detalle` INNER JOIN opciones ON opciones.id = plantillas_detalle.fk_opciones INNER JOIN servicios ON servicios.id = opciones.fk_servicios WHERE 1";

		foreach ($datos as $key => $value) {
			if ($key != 'dias' && $value != '') {
				$sql .= " AND plantillas.$key = '$value'";
			}
		}

		if ($datos['dias'] != '') {
			$sql .= " AND plantillas.dias = " . $datos['dias'];
		}

		$sql .= " GROUP BY plantillas_detalle.fk_plantillas";

		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}*/

	public function subirImagen($datos){
		$filename = $_FILES['file']['name'];		
		$location = "../assets/img/plantillas/".$filename;
		$imageFileType = pathinfo($location,PATHINFO_EXTENSION);
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "JPG") {
 			return [
				'ejecuto' => false,
				'msgError' => 'Tipo de archivo no permitido'
			];
		}		
		if(move_uploaded_file($_FILES['file']['tmp_name'],'../assets/img/plantillas/'.$datos.'.jpg')){
			return [
				'ejecuto' => true,
				'msg' => 'Subio correctamente'
			];
		}
	}
}