<?php
require_once 'database.php';
require_once 'model.php';

class departamentos extends model{
	protected $tabla = 'departamentos';

	public function getDepartamentos($datos){
		$sql = "SELECT *
				FROM departamentos
				WHERE estado = 'Activo'
				ORDER BY nombre";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}
}