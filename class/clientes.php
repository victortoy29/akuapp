<?php
require_once 'database.php';
require_once 'model.php';

class clientes extends model{
	protected $tabla = 'clientes';

	public function getClientes($datos){
		$sql = "SELECT 
					clientes.*,
					tipos_cliente.nombre AS tipo
				FROM 
					clientes INNER JOIN tipos_cliente ON fk_tipos_cliente = tipos_cliente.id
				WHERE
					1 "; 
				foreach ($datos as $key => $value) {
					$sql .= "AND clientes.$key = '$value' ";
				}
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function getLike($datos){
		$sql = "SELECT 
					clientes.*,
					tipos_cliente.nombre AS tipo
				FROM 
					clientes INNER JOIN tipos_cliente ON fk_tipos_cliente = tipos_cliente.id
				WHERE (clientes.nombre LIKE '%$datos[valor]%'					
					OR clientes.numero LIKE '%$datos[valor]%')
					AND clientes.id != 1";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function insertClienteWeb($datos){		
		$datos['password'] = md5($datos['password']);
		return parent::insert($datos);
	}

	public function update($datos){
		$_SESSION['usuario']['nombre'] = $datos['nombre'];
		return parent::update($datos);
	}

	public function loginService($datos){
		$datos['password'] = md5($datos['password']);
		$registro = $this->select($datos);

		if (count($registro['data']) == 0) {
			return [
				'ejecuto' => false,
				'msgError' => 'Credenciales erróneas'
			];
		}elseif($registro['data'][0]['estado'] == 'Cancelado'){
			return [
				'ejecuto' => false,
				'msgError' => 'Su usuario ha sido cancelado'
			];
		}else{
			$usuario = [
				'id' => $registro['data'][0]['id'],
				'nombre' => $registro['data'][0]['nombre'],
				'login' => $registro['data'][0]['correo']
			];
			$_SESSION['usuario'] = $usuario;

			//registrar fecha de utimo acceso
			$acceso = [
				'id' => $registro['data'][0]['id'],
				'ultimo_acceso' => date("Y-m-d H:i:s"),
				'nombre' => $usuario['nombre']
			];
			$this->update($acceso);

			return [
				'ejecuto' => true,
				'data' => $usuario,
				'msgError' => 'Ha iniciado sesión correctamente'
			];
		}
	}
}