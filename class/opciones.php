<?php
require_once 'database.php';
require_once 'model.php';

class opciones extends model{
	protected $tabla = 'opciones';

	public function getOpciones($datos){
		$sql = "SELECT 
					* 
				FROM 
					opciones
				WHERE 1";
				foreach ($datos as $key => $value) {
					$sql .= " AND opciones.$key = '$value' ";
				}
				$sql .= "AND opciones.id != 1
				ORDER BY opciones.opcion";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function opcionesDestinos($datos){
		$sql = "SELECT 
					opciones.*,
					destinos.nombre AS llegada
				FROM 
					opciones INNER JOIN destinos ON fk_llegada = destinos.id
				WHERE
					1";
				foreach ($datos as $key => $value) {
					$sql .= " AND opciones.$key = '$value' ";
				}					
		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}

	public function getOpcionesXservicio($datos){
		$sql = "SELECT 
					servicios.id AS idServicio,
					servicios.nombre,
					servicios.imagen,
					servicios.incluye,
					servicios.no_incluye,
					opciones.id,
					opciones.opcion,
					opciones.precio,
					opciones.costo,
					opciones.duracion,
					opciones.dias_en_itinerario AS di,
					tipos_servicio.icono,
					tipos_servicio.nombre AS nombreTipoServicio,
					tipos_servicio.id AS idts,
					destinos.id AS idDestino,
					destinos.nombre as nombreDestino,
					dll.nombre AS llegada,
					dll.id AS idLlegada
				FROM
					(opciones
					INNER JOIN servicios ON servicios.id = opciones.fk_servicios) 
					INNER JOIN destinos ON fk_destinos = destinos.id
					INNER JOIN tipos_servicio ON servicios.fk_tipos_servicio = tipos_servicio.id
					INNER JOIN destinos AS dll ON fk_llegada = dll.id
				WHERE 1";
				foreach ($datos as $key => $value) {
					if ($value != '' && $key != 'duracion') {
						$sql .= " AND servicios.$key = '$value' ";
					}
				}
				$sql .= "AND servicios.id != 1
						AND servicios.fk_tipos_servicio IN(2)
						AND opciones.duracion <= ". $datos['duracion'] ."
						";
		$db = new database();
		$resDb = $db->ejecutarConsulta($sql);

		$newResDb = [];
		$pos = 0;
		$po_ = 0;
		$tmp = 0;

		for ($i=0; $i < count($resDb['data']); $i++) {
			if ($tmp == 0) {
				$tmp = $resDb['data'][$i]['idServicio'];
			}
			
			if ($tmp == $resDb['data'][$i]['idServicio']) {
				$newResDb[$pos][$po_] = $resDb['data'][$i];
				$tmp = $resDb['data'][$i]['idServicio'];
				$po_++;
			}else{
				$pos++;
				$po_ = 0;
				$newResDb[$pos][$po_] = $resDb['data'][$i];
				$tmp = $resDb['data'][$i]['idServicio'];
				$po_++;
			}
		}

		return array('ejecuto' => true, 'data' => $newResDb);
	}
}