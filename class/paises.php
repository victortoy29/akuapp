<?php
require_once 'database.php';
require_once 'model.php';

class paises extends model{
	protected $tabla = 'paises';

	public function getPaises($datos){
		$sql = "SELECT * 
				FROM paises
				WHERE estado = 'Activo'
				ORDER BY nombre";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}
}