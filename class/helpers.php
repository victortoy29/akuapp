<?php
class helpers{
	public function sesion(){				
		$permisos = [
			'Administrador' => [
				'menuConfiguracion',				
				'menuClientes',				
				'menuCotizaciones',
				'menuReservas',
				'menuCxC',
				'menuCxP'
			],
			'Comercial' => [
				'menuClientes',				
				'menuCotizaciones',
				'menuReservas',
			],
			'Operaciones' => [
				'menuCotizaciones',
				'menuReservas',
			],
			'Contabilidad' => [
				'menuCxC',
				'menuCxP'
			],
			'Proveedor' => [				
				'x'
			]
		];

		if(isset($_SESSION['usuario']['perfil'])){
			return [
				'ejecuto' => true,
				'data' => $_SESSION,
				'permisos' => $permisos[$_SESSION['usuario']['perfil']]
			];	
		}else{
			return [
				'ejecuto' => true,
				'data' => '',
				'permisos' => []
			];
		}		
	}
}