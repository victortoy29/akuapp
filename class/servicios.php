<?php
require_once 'database.php';
require_once 'model.php';

class servicios extends model{
	protected $tabla = 'servicios';
	
	public function getServicios($datos){
		$sql = "SELECT 
					servicios.id,
					departamentos.nombre AS departamento,
					zonas.nombre AS zona,
					destinos.nombre AS destino,
					tipos_servicio.icono,
					tipos_servicio.id AS idts,
					tipos_servicio.nombre AS ts,					
					servicios.nombre AS servicio,
					servicios.descripcion,
					servicios.incluye,
					servicios.no_incluye,
					servicios.recomendaciones,
					servicios.estado,
					servicios.categoria,
					servicios.imagen
				FROM
					(servicios
					INNER JOIN tipos_servicio ON servicios.fk_tipos_servicio = tipos_servicio.id) 
					INNER JOIN (destinos INNER JOIN (zonas INNER JOIN departamentos ON fk_departamentos = departamentos.id) ON fk_zonas = zonas.id) ON fk_destinos = destinos.id
				WHERE 1";
				foreach ($datos as $key => $value) {
					if ($value != '') {
						$sql .= " AND servicios.$key = '$value' ";
					}
				}
				$sql .= "AND servicios.id != 1
				ORDER BY servicios.nombre";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function getServiciosPorFiltro($datos){
		$filtro = '1';
		if($datos['destino']==''){
			$filtro .= ' AND 1';
		}else{
			$filtro .= " AND fk_destinos = $datos[destino]";
		}
		if($datos['tipo_servicio']==''){
			$filtro .= ' AND 1';
		}else{
			$filtro .= " AND fk_tipos_servicio = $datos[tipo_servicio]";
		}
		$sql = "SELECT 
					servicios.id,
					servicios.nombre
				FROM 
					servicios
				WHERE 
					$filtro
					AND servicios.id != 1
				ORDER BY servicios.nombre";
		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}

	public function getUnidad($datos){
		$sql = "SELECT 					
					um
				FROM
					servicios INNER JOIN tipos_servicio ON fk_tipos_servicio = tipos_servicio.id
				WHERE
					servicios.id = $datos[id]";
		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}

	public function subirImagen($datos){
		$filename = $_FILES['file']['name'];		
		$location = "../assets/img/servicios/".$filename;		
		$imageFileType = pathinfo($location,PATHINFO_EXTENSION);
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "JPG") {
 			return [
				'ejecuto' => false,
				'msgError' => 'Tipo de archivo no permitido'
			];
		}		
		if(move_uploaded_file($_FILES['file']['tmp_name'],'../assets/img/servicios/'.$datos.'.jpg')){
			return [
				'ejecuto' => true,
				'msg' => 'Subio correctamente'
			];
		}
	}
}