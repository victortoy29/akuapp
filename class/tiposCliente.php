<?php
require_once 'database.php';
require_once 'model.php';

class tiposCliente extends model{
	protected $tabla = 'tipos_cliente';

	public function getTipos($datos){
		$sql = "SELECT * 
				FROM tipos_cliente
				WHERE estado = 'Activo'
				ORDER BY nombre";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}
}