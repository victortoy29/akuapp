<?php
require_once 'database.php';

class cronJob
{
    private $alegraIntegration;

    private $signature;
    private $urlBase;
    private $urlPetition = "";
    private $headers;

    public function __construct()
    {
        $this->signature = "marketing@akua.com.co:71123e49036d2e8a6d2f";
        $this->urlBase = "https://app.alegra.com/api/v1/";
        $this->headers = [
            "Content-type: application/json",
            "Accept: application/json"
        ];
    }

    public function init()
    {
        $db = new database();

        /**
         * Guardar clientes
         */
        $sql = "SELECT * FROM `clientes`  WHERE alegra_sincro = 0";
        $data = $db->ejecutarConsulta($sql);
        foreach ($data['data'] as $key) {
            $db = new database();
            if (!isset($key['nombre']) || empty($key['nombre']) || $key['nombre'] == "") {
                continue;
            }
            $servicio = ["name" => $key['nombre'], "type" => "client"];
            $response = $this->createContact($servicio);
            $sql = "UPDATE clientes SET alegra_sincro = 1, alegra_id = ". $response->id .", alegra_response = '". json_encode($response) . "' WHERE id = " . $key['id'];
            $db->ejecutarConsulta($sql);
        }

        /**
         * Guardar proveedores
         */
        $sql = "SELECT * FROM `proveedores`  WHERE alegra_sincro = 0";
        $data = $db->ejecutarConsulta($sql);

        foreach ($data['data'] as $key) {
            $db = new database();
            if (!isset($key['nombre']) || empty($key['nombre']) || $key['nombre'] == "") {
                continue;
            }
            $servicio = ["name" => $key['nombre'], "type" => "provider"];
            $response = $this->alegraIntegration->createContact($servicio);
            $sql = "UPDATE proveedores SET alegra_sincro = 1, alegra_id = ". $response->id .", alegra_response = '". json_encode($response) . "' WHERE id = " . $key['id'];
            $db->ejecutarConsulta($sql);
        }

        /**
         * Guardar items
         */
        $sql = "SELECT * FROM `opciones`  WHERE alegra_sincro = 0";
        $data = $db->ejecutarConsulta($sql);
        foreach ($data['data'] as $key) {
            $db = new database();
            $dataAllgra = ["name" => $key['opcion'], "price" => $key['costo'] ];
            $response = $this->createService($dataAllgra);
            $sql = "UPDATE opciones SET alegra_sincro = 1, alegra_id = ". $response->id .", alegra_response = '". json_encode($response) . "' WHERE id = " . $key['id'];
            $db->ejecutarConsulta($sql);
        }

        /**
         * Guardar facturas de ventas
         */
        $sql = "SELECT cxc.id, cxc.fecha_creacion AS fecha_creacion, DATE_ADD(cxc.fecha_creacion, INTERVAL 30 DAY) AS fecha_vencimiento, clientes.alegra_id AS cliente_alegra_id, opciones.alegra_id as opciones_alegra_id, opciones.alegra_id, opciones.opcion, viajes_detalle.precio, viajes_detalle.cantidad FROM ((viajes INNER JOIN clientes ON fk_clientes = clientes.id) INNER JOIN cxc ON viajes.id=cxc.fk_viajes) INNER JOIN (viajes_detalle INNER JOIN opciones ON fk_opciones=opciones.id) ON viajes.id=viajes_detalle.fk_viajes WHERE cxc.alegra_sincro = 0 ORDER BY cxc.id";
        $data = $db->ejecutarConsulta($sql);
        $arr = array();
        $arrItems = array();
        $index = 0;
        for ($i=0; $i < count($data['data']); $i++) {
            $key = $data['data'][$i];
            $obj = new stdClass();

            if ($i == 0) {
                $obj->date = $key['fecha_creacion'];
                $obj->dueDate = $key['fecha_vencimiento'];
                $obj->client = $key['cliente_alegra_id'];
                $arrItems[] = ["id" => $key['opciones_alegra_id'], "price" => $key['precio']];
                array_push($arr, $obj);
            } else {
                if ($key['id'] == $data['data'][$i - 1]['id']) {
                    $arrItems[] = ["id" => $key['opciones_alegra_id'], "price" => $key['precio']];
                } else {
                    $arr[$index]->items = $arrItems;
                    $arrItems = array();

                    $obj->date = $key['fecha_creacion'];
                    $obj->dueDate = $key['fecha_vencimiento'];
                    $obj->client = $key['cliente_alegra_id'];
                    $arrItems[] = ["id" => $key['opciones_alegra_id'], "price" => $key['precio']];
                    array_push($arr, $obj);
                    $index++;
                }
                if (count($data['data']) == ($i + 1)) {
                    $arr[$index]->items = $arrItems;
                }
            }
        };
        foreach ($arr as $key) {
            $db = new database();
            $dataAllgra = ["date" => $key->date, "dueDate" => $key->dueDate, 'client' => $key->client, 'items' => $key->items ];
            $response = $this->createBill($dataAllgra);
            $sql = "UPDATE cxc SET alegra_sincro = 1, alegra_id = ". $response->id .", alegra_response = '". json_encode($response) . "' WHERE id = " . $key->id;
            $db->ejecutarConsulta($sql);
        }

        /**
         * Guardar pagos a facturas de venta
         */
        $sql = "SELECT cxc_pagos.id, cxc_pagos.valor, cxc.alegra_id AS cxc_alegra_id, cxc_pagos.fecha_creacion FROM `cxc_pagos`INNER JOIN cxc ON cxc.id = cxc_pagos.fk_cxc INNER JOIN viajes ON cxc.fk_viajes = viajes.id WHERE cxc_pagos.alegra_sincro = 0";
        $data = $db->ejecutarConsulta($sql);
        foreach ($data['data'] as $key) {
            $db = new database();
            $invoices[] =  ["id" => $key['cxc_alegra_id'], "amount" => $key['valor']];
            $servicio = ["date" => $key['fecha_creacion'], "bankAccount" => 1, "invoices" => $invoices];
            $response = $this->createPay($servicio);
            $sql = "UPDATE cxc_pagos SET alegra_sincro = 1, alegra_id = ". $response->id .", alegra_response = '". json_encode($response) . "' WHERE id = " . $key['id'];
            $db->ejecutarConsulta($sql);
        }

        /**
         * Guardar facturas de compra
         */
        $sql = "SELECT cxp.id, cxp.fecha_creacion AS fecha_creacion, DATE_ADD(cxp.fecha_creacion, INTERVAL 30 DAY) AS fecha_vencimiento, proveedores.alegra_id AS proveedores_alegra_id, opciones.alegra_id as opciones_alegra_id, opciones.alegra_id, opciones.opcion, viajes_detalle.precio, viajes_detalle.cantidad FROM ((viajes INNER JOIN clientes ON fk_clientes = clientes.id) INNER JOIN cxp ON viajes.id=cxp.fk_viajes) INNER JOIN proveedores ON cxp.fk_proveedores = proveedores.id INNER JOIN (viajes_detalle INNER JOIN opciones ON fk_opciones=opciones.id) ON viajes.id=viajes_detalle.fk_viajes WHERE cxp.alegra_sincro = 0 ORDER BY cxp.id";
        $data = $db->ejecutarConsulta($sql);
        foreach ($data['data'] as $key) {
            $db = new database();
            $items[] =  ["id" => $key['opciones_alegra_id'], "price" => $key['precio']];
            $invoices = ["items" => $items];
            $servicio = ["date" => $key['fecha_creacion'], "dueDate" => $key['fecha_vencimiento'], "provider" => $key['proveedores_alegra_id'], "purchases" => $invoices];
            $response = $this->createBillProvider($servicio);
            $sql = "UPDATE cxc_pagos SET alegra_sincro = 1, alegra_id = ". $response->id .", alegra_response = '". json_encode($response) . "' WHERE id = " . $key['id'];
            $db->ejecutarConsulta($sql);
        }

        /**
         * Guardar pagos a facturas de compra
         */
        $sql = "SELECT cxp_pagos.id, cxp_pagos.valor, cxp.alegra_id AS cxp_alegra_id, cxp_pagos.fecha_creacion FROM `cxp_pagos`INNER JOIN cxp ON cxp.id = cxp_pagos.fk_cxp INNER JOIN viajes ON cxp.fk_viajes = viajes.id WHERE cxp_pagos.alegra_sincro = 0";
        $data = $db->ejecutarConsulta($sql);
        foreach ($data['data'] as $key) {
            $db = new database();
            $invoices[] =  ["id" => $key['cxp_alegra_id'], "amount" => $key['valor']];
            $servicio = ["date" => $key['fecha_creacion'], "bankAccount" => 1, "bills" => $invoices];
            $response = $this->createPayProvider($servicio);
            $sql = "UPDATE cxp_pagos SET alegra_sincro = 1, alegra_id = ". $response->id .", alegra_response = '". json_encode($response) . "' WHERE id = " . $key['id'];
            $db->ejecutarConsulta($sql);
        }
        
        return ["status" => "OK"];
    }

    public function syncItems()
    {
        $items = $this->obtenerItems();
        foreach ($items as $key) {
           $price = $key['price'][0]['price'];
           $db = new database();
           $sql = "INSERT INTO `opciones` (`id`, `name`, `price`)  VALUES (".$key['costo'] .", '".$key['name'] ."', '". $price ."') ON DUPLICATE KEY UPDATE name='".$key['name'] ."', price='". $price ."'";
           $db->ejecutarConsulta($sql); 
        }
    }

    public function obtenerItems()
    {
        $this->urlPetition = $this->urlBase . "items";
        $response = $this->execCurl([], true);
        $response = json_decode($response);

        return $response;
    }

    public function createContact(array $data)
    {
        try {
            if (!isset($data['name'])) {
                throw new invalidargumentexception("El nombre es requerido");
            }
            $this->urlPetition = $this->urlBase . "contacts";
        
            $response = $this->execCurl($data, true);
            $response = json_decode($response);
   
            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return $response;
        } catch (\invalidargumentexception $exc) {
            return $exc->getMessage();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }


    public function createService(array $data)
    {
        try {
            if (!isset($data['name'])) {
                throw new invalidargumentexception("El nombre es requerido");
            }
            if (!isset($data['price'])) {
                throw new invalidargumentexception("El precio es requerido");
            }

            $this->urlBase .= "items";
        
            $response = $this->execCurl($data, true);
            $response = json_decode($response);
   
            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return $response;
        } catch (\invalidargumentexception $exc) {
            return ["status" => false, "data" => $exc->getMessage()];
        } catch (\Throwable $th) {
            return ["status" => false, "data" => $th->getMessage()];
        }
    }

    public function createBill(array $data)
    {
        try {
            if (!isset($data['date'])) {
                throw new invalidargumentexception("La fecha es requerida");
            }
            if (!isset($data['dueDate'])) {
                throw new invalidargumentexception("La fecha de vencimiento es requerida");
            }

            if (!isset($data['client'])) {
                throw new invalidargumentexception("El cliente es requerido");
            }

            if (!isset($data['items']) && count($data['items']) > 0) {
                throw new invalidargumentexception("Debes tener al menos un item");
            }
            $this->urlBase .= "invoices";
        
            $response = $this->execCurl($data, true);
            $response = json_decode($response);
   
            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return $response;
        } catch (\invalidargumentexception $exc) {
            return $exc->getMessage();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function createPay(array $data)
    {
        try {
            if (!isset($data['date'])) {
                throw new invalidargumentexception("La fecha es requerida");
            }
            if (!isset($data['bankAccount'])) {
                throw new invalidargumentexception("El banco es requerido");
            }

            if (!isset($data['invoices']) && count($data['invoices']) > 0) {
                throw new invalidargumentexception("Debes tener al menos un item");
            }

            $this->urlBase .= "payments";
        
            $response = $this->execCurl($data, true);
            $response = json_decode($response);
   
            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return $response;
        } catch (\invalidargumentexception $exc) {
            return $exc->getMessage();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }


    public function createPayProvider(array $data)
    {
        try {
            if (!isset($data['date'])) {
                throw new invalidargumentexception("La fecha es requerida");
            }
            if (!isset($data['bankAccount'])) {
                throw new invalidargumentexception("El banco es requerido");
            }

            if (!isset($data['bills']) && count($data['bills']) > 0) {
                throw new invalidargumentexception("Debes tener al menos un item");
            }

            $this->urlBase .= "payments";
        
            $response = $this->execCurl($data, true);
            $response = json_decode($response);
   
            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return $response;
        } catch (\invalidargumentexception $exc) {
            return $exc->getMessage();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }


    public function createBillProvider(array $data)
    {
        try {
            if (!isset($data['date'])) {
                throw new invalidargumentexception("La fecha es requerida");
            }
            if (!isset($data['dueDate'])) {
                throw new invalidargumentexception("La fecha de vencimiento es requerida");
            }

            if (!isset($data['provider'])) {
                throw new invalidargumentexception("El proveedor es requerido");
            }

            if (!isset($data['purchases']) && count($data['purchases']) > 0) {
                throw new invalidargumentexception("Debes tener al menos un item");
            }
            $this->urlBase .= "bills";
        
            $response = $this->execCurl($data, true);
            $response = json_decode($response);
   
            if (isset($response->code)) {
                throw new invalidargumentexception($response->message);
            }

            return $response;
        } catch (\invalidargumentexception $exc) {
            return $exc->getMessage();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function execCurl($data = [], $flagPost = false, $flagGet = false, $flagPut = false, $flagDelete = false)
    {
        $ch = \curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->urlBase);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($flagPost == true) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }
        if ($flagPut == true) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }
        if ($flagDelete == true) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_USERPWD, $this->signature);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}
