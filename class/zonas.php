<?php
require_once 'database.php';
require_once 'model.php';

class zonas extends model{
	protected $tabla = 'zonas';

	public function getZonas($datos){
		$sql = "SELECT
					zonas.id,
					departamentos.nombre AS departamento,
					zonas.nombre AS zona
				FROM 
					zonas INNER JOIN  departamentos ON fk_departamentos = departamentos.id
				WHERE 1 ";
		foreach ($datos as $key => $value) {
			$sql .= "AND zonas.$key = '$value' ";
		}
		$sql .=	"ORDER BY 
					zonas.nombre";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}
}