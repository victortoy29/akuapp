<?php
require_once 'database.php';
require_once 'model.php';

class galeria_servicios extends model{
	protected $tabla = 'galeria_servicios';
	
	public function getImagenes($datos){
		$sql = "SELECT galeria_servicios.url, galeria_servicios.fk_servicios
			 	FROM galeria_servicios
			 	INNER JOIN servicios ON servicios.id = galeria_servicios.fk_servicios
			 	WHERE 1";
				foreach ($datos as $key => $value) {
					$sql .= " AND galeria_servicios.$key = '$value' ";
				}
				$sql .= "ORDER BY galeria_servicios.id";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}
}