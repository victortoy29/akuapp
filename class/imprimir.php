<?php
require_once 'viajes.php';
require_once 'viajesDetalle.php';
require_once 'tiposCliente.php';
require_once '../plugins/fpdf/fpdf.php';

$objViaje = new viajes();
$viaje = $objViaje->getViajes(['campo'=>'id', 'valor'=> $_GET['idV']]);

$objDetalle = new viajesDetalle();
$detalle = $objDetalle->getDetalle(['viaje'=> $_GET['idV'], 'estado'=>'Activo']);

$total = $objDetalle->getValorTotal(['id'=> $_GET['idV']]);

class PDF extends FPDF
{
	function Header()
	{	    
	    $this->Image('../assets/img/logo.png',20,10,40);
	}
}

setlocale(LC_ALL,"es_ES");
$pdf = new PDF();
$pdf->AddPage();
$pdf->SetFont('Arial','',13);
$pdf->SetMargins(20,45);
$pdf->SetY(45);
$pdf->Cell(170,10,'Santiago de Cali, '.strftime("%d de %B del %Y"),0,1);
$pdf->Ln(10);

$pdf->Cell(170,6,utf8_decode('Señor/a:'),0,1);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(170,6,utf8_decode($viaje['data'][0]['cliente']),0,1);
$pdf->SetFont('Arial','',13);
$pdf->Cell(170,6,'La ciudad',0,1);
$pdf->Ln(10);

$pdf->Cell(170,6,'Cordial saludo,',0,1);
$pdf->Ln(10);

//fechas
$fi = explode( "-", $viaje['data'][0]['fecha_inicio']);
$ff = explode( "-", $viaje['data'][0]['fecha_fin']);

$pdf->MultiCell(170,6,utf8_decode('Por medio de la presente, enviamos cotización del viaje "'.$viaje['data'][0]['nombre_viaje'].'", para '.$viaje['data'][0]['pasajeros']).' pasajeros, del '.strftime("%d de %B", mktime(0,0,0,$fi[1],$fi[2],$fi[0])).' al '.strftime("%d de %B del %Y", mktime(0,0,0,$ff[1],$ff[2],$ff[0])).'.');
$pdf->Ln(10);

//Parrafo por tipo de cliente
$objTipo = new tiposCliente();
$tipo = $objTipo->select(['id'=>$viaje['data'][0]['fk_tipos_cliente']]);
$pdf->MultiCell(170,6,utf8_decode($tipo['data'][0]['parrafo']));
$pdf->Ln(10);

$pdf->SetFont('Arial','B',14);
$pdf->MultiCell(170,6,utf8_decode($viaje['data'][0]['nombre_viaje']));
$pdf->Ln(10);

//Esta variable es para construir un buffer con los id de servicios y evitar los repetidos
$bufferServicios = [];
$bufferTransporte = [];
$serviciosDesplegar = [];
$fotosDesplegar = [
	'transporte'=>[],
	'hospedaje'=>[],
	'actividades'=>[]
];
$temp;

foreach ($detalle['data'] as $value) {
	//Esta porción de codigo sirve para filtrar los incluye y no incluye, especialmente por los hoteles que se repiten
	if(!in_array($value['ids'], $bufferServicios)){
		$bufferServicios[] = $value['ids'];
		$serviciosDesplegar[] = [
			'ts' => $value['ts'],
			'ids' => $value['ids'],
			'servicio' => $value['servicio'],
			'cantidad' => $value['cantidad'],
			'incluye' => $value['servicio_incluye'],
			'noIncluye' => $value['servicio_noincluye'],
			'recomendaciones' => $value['servicio_recomendaciones']
		];
		if($value['ts'] == 3 or $value['ts'] == 4){
			$fotosDesplegar['hospedaje'][] = [
				'id' => $value['ids'],
				'nombre' => $value['servicio'],
				'descripcion' => $value['servicio_descripcion'],
				'incluye' => $value['servicio_incluye'],
				'noincluye' => $value['servicio_noincluye']
			];
		}
	}

	//Esta porción de codigo sirve para filtra las fotos que se van a mostrar
	if($value['ts'] == 2){
		$fotosDesplegar['actividades'][] = [
			'id' => $value['ids'],
			'nombre' => $value['servicio'],
			'descripcion' => $value['servicio_descripcion'],
			'incluye' => $value['servicio_incluye'],
			'noincluye' => $value['servicio_noincluye']
		];
	}

	if($value['ts'] == 5 or $value['ts'] == 6){
		$temp = $value['ts'].'-'.$value['idp'];
		if(!in_array($temp, $bufferTransporte)){
			$bufferTransporte[] = $temp;
			$fotosDesplegar['transporte'][] = [
				'id' => $value['ids'],
				'nombre' => $value['servicio']
			];
		}
	}
}


$pdf->SetFont('Arial','B',12);
$pdf->Cell(170,6,'EL PLAN INCLUYE',0,1);
$pdf->SetFont('Arial','',12);
$pdf->Ln(10);
//Listado de servicios
foreach ($serviciosDesplegar as $value) {
	if($pdf->GetY() >= 270){
		$pdf->AddPage();
	}
	$pdf->SetX(30);
	$pdf->Cell(5,6,$pdf->Image('../assets/img/check.png',$pdf->GetX(),$pdf->GetY()));
	if($value['ts'] == 3){
		$pdf->Cell(155,6,$value['cantidad'].' noches en '.utf8_decode($value['servicio']),0,1);	
	}else{
		$pdf->Cell(155,6,utf8_decode($value['servicio']),0,1);
	}
}
$pdf->Ln(10);

$pdf->SetFont('Arial','B',12);
$pdf->Cell(170,6,'EL PLAN NO INCLUYE',0,1);
$pdf->SetFont('Arial','',12);
$pdf->Ln(10);
//Listado de no incluye
foreach ($serviciosDesplegar as $value) {
	if($pdf->GetY() >= 270){
		$pdf->AddPage();
	}
	if($value['noIncluye'] != ''){
		$pdf->SetX(30);
		$pdf->Cell(5,6,$pdf->Image('../assets/img/check.png',$pdf->GetX(),$pdf->GetY()));
		$pdf->Cell(155,6,utf8_decode($value['noIncluye']),0,1);	
	}
}
$pdf->SetX(30);
$pdf->Cell(5,6,$pdf->Image('../assets/img/check.png',$pdf->GetX(),$pdf->GetY()));
$pdf->Cell(155,6,'Gastos no especificados en el plan',0,1);
$pdf->Ln(10);

$pdf->SetX(50);
$pdf->SetFont('Arial','B',13);
$pdf->SetTextColor(0,156,199);
$pdf->Cell(60,7,'VALOR POR PERSONA ');
$pdf->SetTextColor(0,0,0);
$pdf->Cell(50,7,'$'.number_format(($total['data'][0]['valor_total']/$viaje['data'][0]['pasajeros']),0,',','.'),0,1,'R');
$pdf->SetX(50);
$pdf->SetTextColor(0,156,199);
$pdf->Cell(60,7,'VALOR TOTAL');
$pdf->SetTextColor(0,0,0);
$pdf->Cell(50,7,'$'.number_format($total['data'][0]['valor_total'],0,',','.'),0,1,'R');
$pdf->Ln(10);

$pdf->SetTextColor(255,0,0);
$pdf->Cell(170,7,'NOTA:',0,1,'C');
$pdf->Ln(10);
$pdf->SetFont('Arial','',12);
$pdf->SetX(30);
$pdf->MultiCell(150,6,utf8_decode('TARIFAS SUJETAS A CAMBIO Y A DISPONIBILIDAD SIN PREVIO AVISO, PARA PAGOS CON TARJETA DE CRÉDITO SE APLICARÁ SUPLEMENTO.'));
$pdf->Ln(10);

$pdf->SetTextColor(0,0,0);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(170,7,'TRANSPORTE',0,1,'C');
$pdf->Ln(10);
$pdf->SetFont('Arial','',10);
foreach ($fotosDesplegar['transporte'] as $value) {
	if(file_exists('../assets/img/servicios/'.$value['id'].'.jpg')){
		$pdf->Cell(170,50,$pdf->Image('../assets/img/servicios/'.$value['id'].'.jpg',$pdf->GetX()+40,$pdf->GetY(),90,50),0,1);
	}else{
		$pdf->Cell(170,50,'No existe imagen',0,1,'C');
	}
	$pdf->Cell(170,7,utf8_decode($value['nombre']),0,1,'C');
}
$pdf->Ln(10);

$pdf->SetFont('Arial','B',12);
$pdf->Cell(170,7,'HOSPEDAJE',0,1,'C');
$pdf->Ln(10);
$pdf->SetFont('Arial','',10);
foreach ($fotosDesplegar['hospedaje'] as $value) {
	$y = $pdf->GetY();
	if(file_exists('../assets/img/servicios/'.$value['id'].'.jpg')){
		$pdf->Cell(90,50,$pdf->Image('../assets/img/servicios/'.$value['id'].'.jpg',$pdf->GetX(),$pdf->GetY(),90,50));
	}else{
		$pdf->Cell(90,50,'No existe imagen',0,0,'C');
	}
	$pdf->SetXY(110, $y);
	$pdf->SetFont('Arial','B',12);
	$pdf->Cell(80,7,utf8_decode($value['nombre']),0,1);
	$pdf->SetX(110);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(80,5,utf8_decode($value['descripcion']),0,1);
	$pdf->SetX(110);
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(80,5,'Incluye',0,1);
	$pdf->SetX(110);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(80,5,utf8_decode($value['incluye']),0,1);
	$pdf->SetX(110);
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(80,5,'No incluye',0,1);
	$pdf->SetX(110);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(80,5,utf8_decode($value['noincluye']),0,1);
	$pdf->Ln(5);
}
$pdf->SetY($y+60);

$pdf->SetFont('Arial','B',12);
$pdf->Cell(170,7,'ACTIVIDADES',0,1,'C');
$pdf->Ln(10);
$pdf->SetFont('Arial','',10);
foreach ($fotosDesplegar['actividades'] as $value) {
	$y = $pdf->GetY();
	if(file_exists('../assets/img/servicios/'.$value['id'].'.jpg')){
		$pdf->Cell(90,50,$pdf->Image('../assets/img/servicios/'.$value['id'].'.jpg',$pdf->GetX(),$pdf->GetY(),90,50));
	}else{
		$pdf->Cell(90,50,'No existe imagen',0,0,'C');
	}
	$pdf->SetXY(110, $y);
	$pdf->SetFont('Arial','B',12);
	$pdf->Cell(80,7,utf8_decode($value['nombre']),0,1);
	$pdf->SetX(110);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(80,5,utf8_decode($value['descripcion']),0,1);
	$pdf->SetX(110);
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(80,5,'Incluye',0,1);
	$pdf->SetX(110);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(80,5,utf8_decode($value['incluye']),0,1);
	$pdf->SetX(110);
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(80,5,'No incluye',0,1);
	$pdf->SetX(110);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(80,5,utf8_decode($value['noincluye']),0,1);
	$pdf->Ln(5);	
}
$pdf->SetY($y+60);

$pdf->SetFont('Arial','B',12);
$pdf->Cell(170,6,'RECOMENDACIONES',0,1);
$pdf->SetFont('Arial','',12);
$pdf->Ln(10);
//Listado de no incluye
foreach ($serviciosDesplegar as $value) {
	if($pdf->GetY() >= 270){
		$pdf->AddPage();
	}
	if($value['recomendaciones'] != ''){
		$pdf->SetX(30);
		$pdf->Cell(5,6,$pdf->Image('../assets/img/check.png',$pdf->GetX(),$pdf->GetY()));
		$pdf->Cell(155,6,utf8_decode($value['recomendaciones']),0,1);	
	}
}
$pdf->Ln(10);

$pdf->SetFont('Arial','B',12);
$pdf->Cell(170,6,utf8_decode('INFORMACIÓN GENERAL'),0,1);
$pdf->SetFont('Arial','',12);
$pdf->Cell(170,6,utf8_decode('Realizar consignación en la siguiente cuenta'),0,1);
$pdf->Cell(170,6,'Banco: Bancolombia',0,1);
$pdf->Cell(170,6,'Tipo de cuenta: Ahorros',0,1);
$pdf->Cell(170,6,'No. de cuenta: 303-900084-55',0,1);
$pdf->Cell(170,6,utf8_decode('Titular: Diana María Guevara Barco'),0,1);
$pdf->Cell(170,6,utf8_decode('Cédula: 31576329'),0,1);
$pdf->Ln(10);

$pdf->MultiCell(170,6,utf8_decode('Una vez realizada la consignación, enviar comprobante de pago al siguiente correo electrónico: asisgerencia@akua.com.co para legalizar la reserva'),0,1);
$pdf->Ln(10);

$pdf->Cell(170,6,'Cordialmente,',0,1);
$pdf->Ln(10);

$pdf->SetFont('Arial','B',12);
$pdf->Cell(170,6,utf8_decode($viaje['data'][0]['vendedor']),0,1);
$pdf->SetFont('Arial','',10);
$pdf->Cell(170,6,utf8_decode($viaje['data'][0]['cargo']),0,1);
$pdf->Ln(40);

$pdf->SetFont('Arial','B',12);
$pdf->Cell(170,6,'CONTACTOS',0,1);
$pdf->SetFont('Arial','',12);
$pdf->Cell(80,6,utf8_decode('Teléfonos:'));
$pdf->Cell(80,6,utf8_decode('Dirección:'),0,1);
$pdf->SetFont('Arial','',10);
$pdf->SetX(30);
$pdf->Cell(80,6,'314 8801607');
$pdf->Cell(80,6,utf8_decode('Cra 30 No. 5A - 79'),0,1);
$pdf->SetX(30);
$pdf->Cell(80,6,'301 5417282');
$pdf->Cell(80,6,utf8_decode('San fernando, Cali'),0,1);
$pdf->SetFont('Arial','',12);
$pdf->Cell(80,6,utf8_decode('Correo electrónico:'));
$pdf->Cell(80,6,utf8_decode('Sitio web:'),0,1);
$pdf->SetFont('Arial','',10);
$pdf->SetX(30);
$pdf->Cell(80,6,'gerencia@akua.com.co');
$pdf->Cell(60,6,'akua.com.co',0,1);

$pdf->Output();
?>