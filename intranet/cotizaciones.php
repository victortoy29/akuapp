<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <span class="label bg-amarillo">Cotizaciones</span>
        </h1>
        <ol class="breadcrumb">
            <li><a href="intranet/index.php"><i class="fa fa-dashboard"></i> Menu</a></li>
            <li class="active"> Cotizaciones</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listado</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tablaContenido" class="table table-bordered">
                                <thead>
                                    <tr>                                        
                                        <th class="text-center">Código</th>
                                        <th class="text-center">Cliente</th>
                                        <th class="text-center">Pasajeros</th>
                                        <th class="text-center">Fecha inicio</th>
                                        <th class="text-center">Fecha fin</th>
                                        <th class="text-center">Gestión de Servicios</th>
                                        <th class="text-center">Vendedor</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody id="contenido"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalCotizaciones">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <span id="modalCotizacionesTitulo"></span>
                    <small> Modificar cotización</small>
                </h4>
            </div>
            <div class="modal-body">
                <form id="formularioCotizaciones">
                    <div class="form-group">
                        <label for="nombre_viaje" class="control-label">Título viaje</label>
                        <input id="nombre_viaje" name="nombre_viaje" class="form-control" type="text" required>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="pasajeros" class="control-label">Cantidad de pasajeros</label>
                                <input id="pasajeros" name="pasajeros" class="form-control" type="number" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fecha_inicio" class="control-label">Fecha inicio</label>
                                <input id="fecha_inicio" name="fecha_inicio" class="form-control calendario" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fecha_fin" class="control-label">Fecha fin</label>
                                <input id="fecha_fin" name="fecha_fin" class="form-control calendario" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="observaciones" class="control-label">Observaciones</label>
                        <textarea id="observaciones" name="observaciones" class="form-control" cols="10" rows="3"></textarea>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button class="btn btn-default btn-submit" type="submit" form="formularioCotizaciones">
                    <i class="fa fa-save"></i> Actualizar
                </button>                
            </div>
        </div>            
    </div>        
</div>

<div class="modal fade" id="modalAnulacion">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalAnulacionTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioAnulacion">
                    <div class="form-group">
                        <label for="fk_causales" class="control-label">Causal</label>
                        <select id="fk_causales" name="fk_causales" class="form-control" required></select>
                    </div>
                    <div class="form-group">
                        <label for="observaciones" class="control-label">Observación</label>
                        <textarea id="observaciones" name="observaciones" class="form-control" cols="10" rows="3"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="submit" form="formularioAnulacion">
                    <i class="fa fa-save"></i> Cancelar
                </button>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php'; ?>
<script type="text/javascript">
    var id
    var boton
    //var idP

    function initLogin(user){
        $('.numero').number( true, 0,',','.')
        $('.calendario').datetimepicker({
            format: 'YYYY-MM-DD',
            locale: 'es'
        })
        $('#fecha_inicio').datetimepicker({
            format: 'YYYY-MM-DD',
            locale: 'es'
        })
        $('#fecha_fin').datetimepicker({
            format: 'YYYY-MM-DD',
            locale: 'es',
            useCurrent: false
        })
        $("#fecha_inicio").on("dp.change", function (e) {
            $('#fecha_fin').data("DateTimePicker").minDate(e.date);
        });
        $("#fecha_fin").on("dp.change", function (e) {
            $('#fecha_inicio').data("DateTimePicker").maxDate(e.date);
        })

        llenarSelect('causales', 'getCausales', {estado:'Activo'}, 'fk_causales', 'nombre', 1)
        
        cargarRegistros({campo:'estado', valor:'Cotización'}, 'crear')

        $('#formularioCotizaciones').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            data.id = id
            procesarRegistro('viajes', 'update', data, function(r){
                swal('Perfecto!', 'La cotización se actualizo correctamente', 'success')                
                cargarRegistros({campo:'id', valor:id}, 'actualizar')
                $('#modalCotizaciones').modal('hide')
            })
        })

        $('#formularioAnulacion').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))            
            data.id = id
            data.estado = 'Cancelada'
            procesarRegistro('viajes', 'update', data, function(r){                
                $('#'+id).hide('slow')
                $('#modalAnulacion').modal('hide')
            })            
        })
    }

    function cargarRegistros(data, accion, elemento){
        procesarRegistro('viajes', 'getViajes', data, function(r){
            let fila
            let i
            for(i = 0; i < r.data.length; i++){
                fila += '<tr id="'+r.data[i].id+'">'+
                            '<td class="text-center">C-'+r.data[i].id+'</td>'+
                            '<td>'+r.data[i].cliente+'</td>'+
                            '<td class="text-center">'+r.data[i].pasajeros+'</td>'+
                            '<td class="text-center">'+r.data[i].fecha_inicio+'</td>'+
                            '<td class="text-center">'+r.data[i].fecha_fin+'</td>'+
                            '<td class="text-center">'+
                                '<div class="progress">'+
                                    '<div id=indicador_'+r.data[i].id+' class="progress-bar bg-amarillo" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"><span class="text-muted">0/0</span></div>'+
                                '</div>'+
                            '</td>'+                            
                            '<td class="text-center">'+r.data[i].vendedor+'</td>'+
                            '<td>'+
                                '<button class="btn btn-default btn-xs" onClick="mostrarModalEditarCotizaciones('+r.data[i].id+')" title="Editar cotización"><i class="fa fa-pencil-square-o"></i></button> '+
                                '<a class="btn btn-default btn-xs" href="intranet/cotizacionesComercial.php?idV='+r.data[i].id+'" title="Gestión comercial"><i class="fa fa-shopping-cart"></i></a> '+
                                '<a class="btn btn-default btn-xs" href="intranet/cotizacionesOperacion.php?idV='+r.data[i].id+'" title="Gestión operativa"><i class="fa fa-gear"></i></a> '+
                                '<button id="pagos_'+r.data[i].id+'" class="btn btn-success btn-xs ocultar" onClick="crearCobro('+r.data[i].id+')" title="Crear cuenta de cobro"><i class="fa fa-money"></i></button> '+
                                '<button class="btn btn-danger btn-xs" onClick="cancelar('+r.data[i].id+')" title="Cancelar cotización"><i class="fa fa-close"></i></button>'+
                            '</td>'+
                        '</tr>' 
            }
            if(accion == 'crear'){
                $('#contenido').append(fila)                        
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }            
            //Se ocultan los botones y los que esten al 100% se muestran en la siguiente función
            if(i != 0){
                $('.ocultar').hide()
                cargarIndicador()
            }            
        })
    }

    function mostrarModalEditarCotizaciones(idCotizacion){
        id = idCotizacion
        llenarFormulario('formularioCotizaciones','viajes', 'select', {'id':idCotizacion}, function(r){
            $('#modalCotizacionesTitulo').text('C-'+idCotizacion)            
            $('#modalCotizaciones').modal('show')            
        })
    }

    function cargarIndicador(){
        procesarRegistro('viajesDetalle', 'getIndicador', {estado: 'Activo'}, function(r){
            if(r.data.length != 0){
                let centinela = r.data[0].fk_viajes
                let numerador = 0
                let denominador = 0
                let indicador_texto
                let indicador_grafico
                for(let i = 0; i < r.data.length; i++){
                    if(centinela == r.data[i].fk_viajes){
                        if(r.data[i].estado == 'Bloqueado'){
                            numerador += r.data[i].cantidad
                            denominador += r.data[i].cantidad
                        }else{
                            denominador += r.data[i].cantidad
                        }                    
                    }
                    if(centinela != r.data[i].fk_viajes){
                        porcentaje = (numerador / denominador)*100                    
                        $('#indicador_'+centinela).css('width', porcentaje+'%').attr('aria-valuenow', porcentaje).html('<span class="text-muted">'+(numerador+'/'+denominador)+'</span>')
                        if(porcentaje == 100){                            
                            $('#pagos_'+centinela).show()    
                        }                    
                        centinela = r.data[i].fk_viajes
                        numerador = 0
                        denominador = 0
                        i--
                    }
                }
                porcentaje = (numerador / denominador)*100
                $('#indicador_'+centinela).css('width', porcentaje+'%').attr('aria-valuenow', porcentaje).html('<span class="text-muted">'+(numerador+'/'+denominador)+'</span>')
                if(porcentaje == 100){
                    $('#pagos_'+centinela).show()
                }
            }
        })
    }

    function cancelar(idViaje){
        id = idViaje
        $('#modalAnulacionTitulo').text('Cancelar cotización C-'+idViaje)
        $('#formularioAnulacion')[0].reset()
        $('#modalAnulacion').modal('show')
    }

    function crearCobro(idViaje){
        procesarRegistro('viajesDetalle', 'getValorTotal', {id:idViaje}, function(r){
            swal('Seguro desea crear la cuenta de cobro por valor de $'+currency(r.data[0].valor_total,0), {
                buttons: ['No','Si']
            })
            .then((value)=>{                
                if(value){
                    procesarRegistro('cxc', 'crearCuenta', {viaje:idViaje, valor:r.data[0].valor_total}, function(r){
                        swal('Perfecto!', 'Se creo correctamente la cuenta de cobro', 'success')
                    })
                }
            })
        })
    }
</script>
</body>
</html>