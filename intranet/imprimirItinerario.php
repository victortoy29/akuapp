<?php
require_once '../class/viajes.php';
require_once '../class/viajesDetalle.php';
require_once '../plugins/fpdf/fpdf.php';

$objViaje = new viajes();
$viaje = $objViaje->getViajes(['campo'=>'id', 'valor'=> $_GET['idV']]);

$objDetalle = new viajesDetalle();
$detalle = $objDetalle->getItinerario(['viaje'=> $_GET['idV']]);

$itinerario = [];
for ($i=0; $i < count($detalle['data']); $i++){
	$cantidad = $detalle['data'][$i]['di'] * $detalle['data'][$i]['cantidad'];
	$fecha = $detalle['data'][$i]['fecha_inicio'];
	for($j=0; $j < $cantidad; $j++){
		$itinerario[$fecha][] = [
			'hora' => $detalle['data'][$i]['hora_inicio'],
			'destino' => $detalle['data'][$i]['destino'],
			'servicio' => $detalle['data'][$i]['servicio'],
			'opcion' => $detalle['data'][$i]['opcion'],
			'id'=>$detalle['data'][$i]['idServicio'],
			'descripcion'=>$detalle['data'][$i]['descripcion'],
			'incluye'=>$detalle['data'][$i]['incluye'],
			'no_incluye'=>$detalle['data'][$i]['no_incluye'],
			'recomendaciones'=>$detalle['data'][$i]['recomendaciones'],
			'pasajeros' => $detalle['data'][$i]['pasajeros']
		];
		$fecha = date('Y-m-d', strtotime($fecha. ' + 1 days'));
	}
}

class PDF extends FPDF{
	// Cabecera de página
	function Header(){
		global $viaje;
	    // Logos
	    $this->SetFont('Arial','B',12);
	    $this->Cell(50,30,$this->Image('../assets/img/logo.png',$this->GetX(),$this->GetY()+1,40),0,0,'C');
	    $this->Cell(90,10,utf8_decode('Diana María Guevara'),0,1,'C');
	    $this->SetFont('Arial','',8);
	    $this->SetX(60);
	    $this->Cell(90,5,'NIT 31576329-2',0,1,'C');
	    $this->SetX(60);
	    $this->Cell(90,5,'gerencia@akua.com.co',0,1,'C');
	    $this->SetFont('Arial','B',12);
	    $this->SetXY(150,10);
	    $this->Cell(50,30,'R-'.$viaje['data'][0]['cr'],0,1,'C');
	    $this->Ln(3);

	    /*$this->SetFont('Arial','',10);
	    $this->Cell(20,5,'Cliente',0,0);
	    $this->Cell(100,5,utf8_decode($viaje['data'][0]['cliente']),0);
	    $this->Cell(30,5,'Fecha inicio',0,0);
	    $this->Cell(40,5,utf8_decode($viaje['data'][0]['fecha_inicio']),0,1);
	    
	    $this->Cell(20,5,$viaje['data'][0]['tipo_doc'],0,0);
	    $this->Cell(100,5,utf8_decode($viaje['data'][0]['numero']),0);
	    $this->Cell(30,5,'Fecha fin',0,0);
	    $this->Cell(40,5,utf8_decode($viaje['data'][0]['fecha_fin']),0,1);

	    $this->Cell(20,5,'Pasajeros',0,0);
	    $this->Cell(100,5,utf8_decode($viaje['data'][0]['pasajeros']),0);
	    $this->Cell(30,5,utf8_decode('Fecha creación'),0,0);
	    $this->Cell(40,5,utf8_decode($viaje['data'][0]['fecha_creacion']),0,1);
	    $this->Ln(3);*/
	}

	// Pie de página
	function Footer(){
	    // Posición: a 1,5 cm del final
	    $this->SetY(-15);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Número de página
	    $this->Cell(0,10,'Pagina '.$this->PageNo(),0,0,'C');
	}
}

//print_r($itinerario);

$pdf = new PDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,20,'Itinerario',0,1,'C');
$pdf->Ln();

$pdf->SetFont('Arial','',10);
$pdf->Cell(190,5,'Cliente',0,1,'C');

$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,5,utf8_decode($viaje['data'][0]['cliente']),0,1,'C');
$pdf->Ln(30);

$pdf->SetFont('Arial','',10);
$pdf->Cell(70,20,'');
$pdf->Cell(25,20,'Fecha inicio');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(95,20,$viaje['data'][0]['fecha_inicio'],0,1);

$pdf->SetFont('Arial','',10);
$pdf->Cell(70,20,'');
$pdf->Cell(25,20,'Fecha fin');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(95,20,$viaje['data'][0]['fecha_fin'],0,1);

$pdf->SetFont('Arial','',10);
$pdf->Cell(70,20,'');
$pdf->Cell(25,20,'Pasajeros');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(95,20,$viaje['data'][0]['pasajeros'],0,1);

$pdf->AddPage();
$centinela = 1;
$dia = 1;
foreach ($itinerario as $key => $actividades) {	
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(20,7,utf8_decode('Día').$dia,0);
	$pdf->Cell(170,7,$key,0,1);	
	$pdf->SetFont('Arial','',10);
	foreach ($actividades as $value) {		
		$pdf->Cell(20,7,$value['hora'],1);
		$pdf->Cell(70,7,$value['servicio'],1);
		$pdf->Cell(100,7,$value['opcion'],1,1);
		if(file_exists('../assets/img/servicios/'.$value['id'].'.jpg')){
			$pdf->Cell(80,50,$pdf->Image('../assets/img/servicios/'.$value['id'].'.jpg',$pdf->GetX(),$pdf->GetY(),80,50),1,0,'C');
		}else{
			$pdf->Cell(80,50,'No existe imagen',1,0,'C');
		}
		$y = $pdf->GetY();
		$pdf->MultiCell(110,5,utf8_decode($value['descripcion']),1,1);
		$pdf->SetY($y+50);
		$pdf->Ln();
		if($centinela % 3 == 0){
			$pdf->AddPage();
		}
		$centinela++;
	}
	$dia++;	
}
$pdf->Output();
?>