<?php require 'header.php'; ?>

<div class="content-wrapper">
	<section class="content-header">
        <h1>
            <span class="label bg-amarillo">C-<?=$_GET['idV']?></span>
            <small>
                <span class="label bg-amarillo">
                	<i class="fa fa-users"></i> X <span class="tituloPasajeros"></span>
                </span>
            </small>            
        </h1>
        <ol class="breadcrumb">
            <li><a href="intranet/index.php"><i class="fa fa-dashboard"></i> Menu</a></li>
            <li><a href="intranet/cotizaciones.php"> Cotizaciones</a></li>
            <li><a href="intranet/cotizacionesComercial.php?idV=<?=$_GET['idV']?>"> Comercial</a></li>
            <li class="active"> Detalle</li>
        </ol>
    </section>
    <section class="content container-fluid">
    	<div class="row">    		
            <div class="col-md-12">
                <form id="formularioItinerario"></form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <button class="btn color-magical btn-block" type="submit" form="formularioItinerario">
                    <i class="fa fa-save"></i> Guardar
                </button>
            </div>
            <div class="col-md-6">
                <a class="btn btn-info btn-block" href="class/imprimir.php?idV=<?=$_GET['idV']?>" target=_blank>
                    <i class="fa fa-file-pdf-o"></i> Imprimir
                </a>
            </div>
		</div>
    </section>
</div>

<?php require 'footer.php'; ?>
<script type="text/javascript">
	var idViaje = <?=$_REQUEST['idV']?>;
	var opcionales = []

	function initLogin(user){
		procesarRegistro('viajes', 'select', {id: idViaje}, function(r){			
			$('.tituloPasajeros').text(r.data[0].pasajeros)
			//if(r.data[0].opcionales == ""){
				procesarRegistro('viajesDetalle', 'getDetalle', {viaje: idViaje, estado: 'Activo'}, function(r){
					for(let i = 0; i < r.data.length; i++){
						opcionales.push({
							id: r.data[i].id,
							opciones: []
						})
						$('#formularioItinerario').append('<div class="box box-primary">'+
								'<div class="box-header with-border">'+
			      					'<h3 class="box-title">'+r.data[i].servicio+'</h3>'+
			      					'<div class="pull-right">'+
		                    			'<span class="label label-info">'+r.data[i].fecha_inicio+'</span>'+
			    					'</div>'+
			    				'</div>'+
			    				'<div class="box-body">'+
			    					'<div class="form-group">'+
		                                '<div class="checkbox">'+
		                                    '<label>'+
		                                        '<input type="checkbox" id="descripcion-'+r.data[i].id+'" name="descripcion-'+r.data[i].id+'" value="descripcion">'+r.data[i].servicio_descripcion+
		                                    '</label>'+
		                                '</div>'+
		                                '<div class="checkbox">'+
		                                    '<label>'+
		                                        '<input type="checkbox" id="recomendaciones-'+r.data[i].id+'" name="recomendaciones-'+r.data[i].id+'" value="recomendaciones">'+r.data[i].servicio_recomendaciones+
		                                    '</label>'+
		                                '</div>'+
		                                '<div class="checkbox">'+
		                                    '<label>'+
		                                        '<input type="checkbox" id="incluye-'+r.data[i].id+'" name="incluye-'+r.data[i].id+'" value="inlcuye">'+r.data[i].servicio_incluye+
		                                    '</label>'+
		                                '</div>'+
		                                '<div class="checkbox">'+
		                                    '<label>'+
		                                        '<input type="checkbox" id="no_incluye-'+r.data[i].id+'" name="no_incluye-'+r.data[i].id+'" value="no_inlcuye">'+r.data[i].servicio_noincluye+
		                                    '</label>'+
		                                '</div>'+
		                            '</div>'+
		                        '</div>'+
							'</div>')
					}
				})		
			//}
		})

		$('#formularioItinerario').on('submit', function(e){
            e.preventDefault()            
            let data = $(this).serializeArray();
            console.log(data)
            //Limpiar arreglo a enviar           
            for(let i = 0; i < opcionales.length; i++){
            	opcionales[i].opciones = []
            }            
            //Se llena el arreglo con lo escogido            
            for(let j = 0; j < data.length; j++){
            	temp = data[j].name.split('-')
            	for(let x = 0; x < opcionales.length; x++){
            		if(opcionales[x].id == temp[1]){
            			opcionales[x].opciones.push(temp[0])
            		}            	
            	}
            }
            procesarRegistro('viajes', 'guardarOpcionales', {id:idViaje, opcionales:opcionales}, function(r){
                swal('Perfecto!', 'Se guardo correctamente', 'success')
            })
        })
	}
</script>
</body>
</html>