<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <span class="label label-success" id="tituloReserva"></span>
            <small>                
                <span class="label label-success"><i class="fa fa-users"></i> X <span class="tituloPasajeros"></span></span>
            </small>            
        </h1>
        <ol class="breadcrumb">
            <li><a href="intranet/index.php"><i class="fa fa-dashboard"></i> Comercial</a></li>
            <li><a href="intranet/reservas.php"> Reservas</a></li>
            <li class="active"> Pasajeros</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row" id="contenido"></div>
        <div class="row text-center">
            <button id="botonMostrarModalPasajeros" class="btn btn-primary">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </section>
</div>

<div class="modal fade" id="modalPasajeros">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalPasajerosTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioPasajeros">                    
                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre</label>
                        <input id="nombre" name="nombre" class="form-control" type="text" required>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="tipo_doc" class="control-label">Tipo identificación</label>
                                <select id="tipo_doc" name="tipo_doc" class="form-control" required>
                                    <option value="">Seleccione...</option>
                                    <option value="Cedula de ciudadanía">Cedula de ciudadanía</option>
                                    <option value="Tarjeta de identidad">Tarjeta de identidad</option>
                                    <option value="Pasaporte">Pasaporte</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="numero" class="control-label">Numero de identificación</label>
                                <input id="numero" name="numero" class="form-control" type="text" required>
                            </div>
                        </div>                    
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="edad" class="control-label">Edad</label>
                                <input id="edad" name="edad" class="form-control" type="text" required>
                            </div>
                        </div>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button id="botonGuardarPasajeros" class="btn btn-primary btn-submit" type="submit" form="formularioPasajeros">
                    <i class="fa fa-save"></i> Guardar
                </button>
                <button id="botonEditarPasajeros" class="btn btn-default btn-submit" type="submit" form="formularioPasajeros">
                    <i class="fa fa-refresh"></i> Actualizar
                </button>
            </div>
        </div>            
    </div>        
</div>

<?php require 'footer.php'; ?>

<script type="text/javascript">
	var idViaje = <?=$_REQUEST['idV']?>;
	var id
    var pasajeros
    var contador = 0

	function initLogin(user){
        procesarRegistro('viajes', 'select', {id: idViaje}, function(r){            
            pasajeros = r.data[0].pasajeros
            $('.tituloPasajeros').text(r.data[0].pasajeros)
            $('#tituloReserva').text('R-'+r.data[0].codigo_reserva)
        })

        cargarRegistros({fk_viajes:idViaje, estado:'Activo'}, 'crear')

        $('#botonMostrarModalPasajeros').on('click', function(){            
            if(contador < pasajeros){
                $('#formularioPasajeros')[0].reset()
                $('#modalPasajerosTitulo').text('Crear pasajero')
                $('#botonEditarPasajeros').hide()
                $('#botonGuardarPasajeros').show()
                $('#modalPasajeros').modal('show')
            }else{
                swal('Ups!', 'Solo puede crear '+pasajeros+' pasajeros', 'error')
            }
        })

        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })

        $('#formularioPasajeros').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            data.fk_viajes = idViaje
            if(boton == "botonGuardarPasajeros"){                
                procesarRegistro('pasajeros', 'insert', data, function(r){
                    swal('Perfecto!', 'El pasajero se creo correctamente', 'success')
                    cargarRegistros({'id':r.insertId}, 'crear')
                    $('#modalPasajeros').modal('hide')                    
                })                                
            }else{                
                data.id = id
                procesarRegistro('pasajeros', 'update', data, function(r){                    
                    swal('Perfecto!', 'El pasajero se actualizo correctamente', 'success')
                    if(data.estado == 'Cancelado'){
                        $('#'+data.id).remove()
                    }else{
                        cargarRegistros({'id':id}, 'actualizar')
                    }
                    $('#modalPasajeros').modal('hide')
                })
            }
        })
    }

    function cargarRegistros(data, accion){
        procesarRegistro('pasajeros', 'getPasajeros', data, function(r){
            let fila = ''
            //let opciones
            for(let i = 0; i < r.data.length; i++){
                contador++
                fila += '<div class="col-md-4" id="'+r.data[i].id+'">'+
                            '<div class="box box-solid">'+
                                '<div class="box-header with-border text-center">'+
                                    '<h3 class="box-title info-box-number">'+r.data[i].nombre+'</h3>'+
                                    '<div class="box-tools pull-right">'+
                                        '<label class="btn btn-default btn-xs" for="foto_'+r.data[i].id+'">'+
                                            '<input id="foto_'+r.data[i].id+'" onChange="cargarImagen('+r.data[i].id+')" type="file"  style="display:none;"/><i class="fa fa-camera" aria-hidden="true"></i>'+
                                        '</label>'+
                                        '<button class="btn btn-default btn-xs" onClick="mostrarModalEditarUsuarios('+r.data[i].id+')" title="Editar pasajero"><i class="fa fa-pencil-square-o"></i></button>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="box-body">'+
                                    '<p class="text-center">'+
                                        '<img src="assets/img/documentos/'+r.data[i].id+'.jpg" id="f_'+r.data[i].id+'" style="height:200px;">'+
                                    '</p>'+                                    
                                    '<table class="table table-bordered">'+                                        
                                        '<tr><td>Tipo de documento</td><td><strong>'+r.data[i].tipo_doc+'</strong></td>'+
                                        '<tr><td>Número</td><td><strong>'+r.data[i].numero+'</strong></td>'+                                        
                                        '<tr><td>Edad</td><td><strong>'+r.data[i].edad+'</strong></td>'+
                                    '</table>'+                                        
                                '</div>'+
                            '</div>'+
                        '</div>'
            }
            if(accion == 'crear'){
                $('#contenido').append(fila)
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
                contador--
            }
        })
    }

    function mostrarModalEditarUsuarios(idPasajero){
        id = idPasajero
        llenarFormulario('formularioPasajeros','pasajeros', 'select', {'id':idPasajero}, function(r){
            $('#modalPasajerosTitulo').text('Modificar pasajero')
            $('#botonGuardarPasajeros').hide()
            $('#botonEditarPasajeros').show()            
            $('#modalPasajeros').modal('show')            
        })
    }

    function cargarImagen(idPasajero){
        let archivo = $('#foto_'+idPasajero)[0].files[0]        
        let ext = archivo.name.split('.')        
        if(ext[1] == 'jpg' || ext[1] == 'png' || ext[1] == 'jpeg' || ext[1] == 'JPG'){
            //se carga la imagen
            var fd = new FormData()        
            fd.append('objeto','pasajeros')
            fd.append('metodo','subirImagen')
            fd.append('datos',idPasajero)
            fd.append('file',archivo)
            $.ajax({
                url: 'class/frontController.php',
                type: 'post',
                data: fd,
                contentType: false,                
                processData: false,
                dataType: 'json',
                success: function(r){
                    d = new Date();
                    $('#f_'+idPasajero).attr("src", "assets/img/documentos/"+idPasajero+".jpg?"+d.getTime())                    
                },
                error: function(xhr,status){
                    console.log('Disculpe, existio un problema procesando')
                }
            })
        }else{
            swal('Ups!', 'Tipo de archivo no permitido', 'error')
        }
    }
</script>
</body>
</html>