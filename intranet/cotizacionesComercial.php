<?php require 'header.php'; ?>

<div class="content-wrapper">
	<section class="content-header">
        <h1>
            <span class="label bg-amarillo">C-<?=$_GET['idV']?></span>
            <small>
                <span class="label bg-amarillo">
                	<i class="fa fa-users"></i> X <span class="tituloPasajeros"></span>
                </span>
            </small>            
        </h1>
        <ol class="breadcrumb">
            <li><a href="intranet/index.php"><i class="fa fa-dashboard"></i> Menu</a></li>
            <li><a href="intranet/cotizaciones.php"> Cotizaciones</a></li>
            <li class="active"> Comercial</li>
        </ol>
    </section>
    <section class="content container-fluid">
    	<div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Servicios</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <form id="formularioDetalle">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="fecha_inicio" class="control-label">Día inicio</label>
                                                <select id="fecha_inicio" name="fecha_inicio" class="listado_dias form-control" required="required"></select>
                                            </div>
                                        </div>                                        
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="tipos_servicio" class="control-label">Tipo servicio</label>
                                                <select id="tipos_servicio" class="form-control filtro"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="departamentos" class="control-label">Departamento</label>
                                                <select id="departamentos" class="form-control"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="zonas" class="control-label">Zona</label>
                                                <select id="zonas" class="form-control"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="destinos" class="control-label">Destino</label>
                                                <select id="destinos" class="form-control filtro"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Hora:</label>
                                                <div class="input-group">
                                                    <div class='input-group date hora_inicio'>
                                                        <input id="hora_inicio" name="hora_inicio" class="form-control" type='text'>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="servicios" class="control-label">Servicio</label>
                                                <select id="servicios" class="form-control"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="fk_opciones" class="control-label">Opciones</label>
                                                <select id="fk_opciones" name="fk_opciones" class="form-control" required></select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label id="indicativoTipo" class="control-label">.</label>
                                                <input id="indicativoPrecio" class="form-control numero" type="text" readonly="readonly">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="precio" class="control-label">Precio por persona</label>
                                                <input id="precio" name="precio" class="form-control numero" type="text" required="required">
                                            </div>
                                            <div class="form-group">                                
                                                <input id="costo" name="costo" class="form-control" type="hidden">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="pasajeros" class="control-label">Pasajeros</label>
                                                <input id="pasajeros" name="pasajeros" class="form-control" type="number" required="required">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="cantidad" id="unidad_medida" class="control-label">Cantidad</label>
                                                <input id="cantidad" name="cantidad" class="form-control numero" type="text" required="required" value=1>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="total" class="control-label">Total</label>
                                                <input id="total" class="form-control numero" type="text" readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="detalle" class="control-label">Detalle</label>
                                                <textarea id="detalle" name="detalle" class="form-control" rows="1"></textarea>
                                            </div>
                                        </div>
                                    </div>                    
                                </form>
                            </div>
                            <div class="box-footer text-center">
                                <button id="botonGuardarDetalle" class="btn btn-primary btn-submit" type="submit" form="formularioDetalle">
                                    <i class="fa fa-arrow-down"></i>
                                </button>
                                &nbsp;
                                <button id="botonEditarDetalle" class="btn btn-default btn-submit" type="submit" form="formularioDetalle">
                                    Actualizar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Servicios</h3>
                                <div class="pull-right">
                                    <button id="botonMostrarModalPlantilla" class="btn btn-primary btn-sm">Adicionar plantilla</button>
                                    <button id="botonMostrarModalClon" class="btn btn-primary btn-sm" title="Clonar para crear plantilla"><i class="fa fa-clone"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
        					                <tr>
        					                	<th class="text-center">Fecha</th>
        					                	<th class="text-center">Hora</th>
        					                    <th class="text-center">Destino</th>
        					                    <th class="text-center">Servicio</th>
        					                    <th class="text-center">Opción</th>
        					                    <th class="text-center">Detalle</th>
        					                    <th class="text-center">Precio</th>
        					                    <th class="text-center">Descuento</th>
        					                    <th class="text-center">Pasajeros</th>
        					                    <th class="text-center">Cantidad</th>
        					                    <th class="text-center">Total</th>
        					                    <th class="text-center">Opciones</th>
        					                </tr>
        					            </thead>
                    					<tbody id="contenido"></tbody>
                					</table>
                				</div>
                			</div>
                		</div>
                    </div>
                </div>        
                <div class="row">            
                    <div class="col-md-offset-9 col-md-3">
                        <div class="box">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">Pasajero</th>
                                        <th class="text-center">Precio</th>
                                    </tr>
                                </thead>
                                <tbody id="contenidoDetallePax"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-9 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-amarillo"><i class="fa fa-user"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Precio PAX</span>
                                <span class="info-box-number" id="precio_promedio"></span>
                            </div>            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-9 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-amarillo"><i class="fa fa-users"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Precio total</span>
                                <span class="info-box-number" id="precio_total"></span>
                            </div>            
                        </div>
                    </div>
                </div>
                <div class="row">                    
                    <div class="col-md-offset-9 col-md-3 text-right">
                        <p>
                        <a class="btn btn-default btn-xs" id="imprimir" href="class/imprimir.php?idV=<?=$_GET['idV']?>" target=_blank>
                            Cotización automática <i class="fa fa-file-pdf-o fa-2x"></i>
                        </a>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-9 col-md-3 text-right">
                        <a class="btn btn-default btn-xs" id="imprimir" href="intranet/manual.php?idV=<?=$_GET['idV']?>">
                            Cotización manual <i class="fa fa-file-pdf-o fa-2x"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <ul class="timeline" id="contenidoItinerario"></ul>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalPlantillas">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                    <span aria-hidden="true">&times;</span> 
                </button>                 
                <h3 class="modal-title">
                    Agregar plantilla
                </h3>
            </div> 
            <div class="modal-body"> 
                <form id="formularioPlantillas">
                    <div class="form-group">
                        <label for="plantilla" class="control-label">Plantillas:</label>
                        <select id="plantilla" name="plantilla" class="form-control" required="required"></select>
                    </div>
                    <div class="form-group">
                        <label for="inicio_plantilla" class="control-label">Día inicio</label>
                        <select id="inicio_plantilla" name="inicio_plantilla" class="listado_dias form-control" required="required"></select>
                    </div>
                </form>
                <div class="text-center">
                    <button class="btn btn-primary" type="submit" form="formularioPlantillas">
                        <i class="fa fa-save"></i> Guardar
                    </button>
                </div>
            </div>
        </div>             
    </div>         
</div>

<div class="modal fade" id="modalPasajeros">
    <div class="modal-dialog modal-lg"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                    <span aria-hidden="true">&times;</span> 
                </button>                 
                <h3 class="modal-title">
                    <span id="modalPasajerosTitulo"></span>
                    <small> Asignar pasajeros</small>
                </h3>
            </div> 
            <div class="modal-body"> 
                <form id="formularioPasajeros">
                    <div class="form-group">
                        <label for="pasajero" class="control-label">Pasajero:</label>
                        <select id="pasajero" name="pasajero" class="form-control" required></select>
                    </div>
                </form>
                <div class="text-center">
                    <button class="btn btn-primary" type="submit" form="formularioPasajeros">
                        <i class="fa fa-arrow-down"></i>
                    </button>
                    <button id="agregarTodos" class="btn btn-primary" type="button">
                        Todos
                    </button>
                </div>
            </div> 
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <table class="table table-bordered"> 
                        <thead> 
                            <tr>
                                <th class="text-center">Pasajero</th>
                                <th class="text-center">Opciones</th>
                            </tr> 
                        </thead> 
                        <tbody id="contenidoPasajeros"></tbody> 
                    </table>
                </div>
            </div>
        </div>             
    </div>         
</div>

<?php require 'footer.php'; ?>

<script type="text/javascript">
	var idViaje = <?=$_REQUEST['idV']?>;    
	var id
    var boton
	var pasajeros
	var valores = []
	var opcionEscojida

	function initLogin(user){
    	$('.numero').number( true, 0,',','.')
        $('.hora_inicio').datetimepicker({
            format: 'HH:mm'
        })
        $('#botonEditarDetalle').hide()        

    	//COMPONENTE DE LLENADO DE SELECT
    	//Llenado de dias
    	procesarRegistro('viajes', 'select', {id: idViaje}, function(r){
            pasajeros = r.data[0].pasajeros
            $('.tituloPasajeros').text(r.data[0].pasajeros)
            $('#pasajeros').val(pasajeros)

            //llenar dinamicamente pasajeros para detalle px
            for (let i = 1; i <= r.data[0].pasajeros; i++) {
                $('#pasajero').append('<option value="Pasajero '+i+'">Pasajero '+i+'</option>')                
            }

            //LLenar combo de días con los días entre los cuales esta el viaje
            let inicio = moment(r.data[0].fecha_inicio, 'YYYY-MM-DD')
            let fin = moment(r.data[0].fecha_fin, 'YYYY-MM-DD')
            let dias = fin.diff(inicio, 'days')
            //iteración para llenar combo de días
            for (let i = 0; i <= dias; i++) {            	
            	$('.listado_dias').append("<option value="+inicio.format('YYYY-MM-DD')+">"+inicio.format('DD-MMM-YYYY')+"</option>")
            	inicio.add(1, 'days')
            }

            //CLON
            $('#botonMostrarModalClon').on('click', function(){
                swal("Escriba el nombre de la plantilla",{
                content: "input"
                })
                .then((value) => {
                    if (value) {
                        procesarRegistro('plantillas', 'crear', {nombre: value, viaje: idViaje, fecha_inicio: r.data[0].fecha_inicio ,pasajeros: r.data[0].pasajeros, dias: dias+1}, function(r){
                            swal('Perfecto!', 'Se creo la plantilla correctamente', 'success')
                        })
                    }
                })
            })            
        })
        //llenado de destinos y tipos de servicio
        llenarSelect('tiposServicio', 'getTiposServicio', {estado:'Activo'}, 'tipos_servicio', 'nombre', 1)
        llenarSelect('departamentos', 'select', {estado:'Activo'}, 'departamentos', 'nombre', 1)
        $('#departamentos').on('change', function(){
            llenarSelect('zonas', 'select', {fk_departamentos:$(this).val(), estado:'Activo'}, 'zonas', 'nombre', 1)
        })
        llenarSelect('zonas', 'select', {estado:'Activo'}, 'zonas', 'nombre', 1)
        $('#zonas').on('change', function(){
            llenarSelect('destinos', 'select', {fk_zonas:$(this).val(), estado:'Activo'}, 'destinos', 'nombre', 1)
        })
        llenarSelect('destinos', 'select', {estado:'Activo'}, 'destinos', 'nombre', 1)        

        //Cuando se escoge hospedaje o paquete se bloquea la hora
        $('#tipos_servicio').on('change', function(){
            if($(this).val() == 3 || $(this).val() == 4 || $(this).val() == 7 || $(this).val() == 8){
                $('#hora_inicio').val('')
                $('#hora_inicio').prop('disabled', 'disabled')                
            }else{
                $('#hora_inicio').prop('disabled', false)
            }
        })
        
        //llenado de servicios
        $('.filtro').on('change', function(){
            let datos = {
                destino:$('#destinos').val(),
                tipo_servicio:$('#tipos_servicio').val()
            }
            llenarSelect('servicios', 'getServiciosPorFiltro', datos, 'servicios', 'nombre', 1)
        })
        //Llenado de opciones
        $('#servicios').on('change', function(){
        	procesarRegistro('servicios', 'getUnidad', {id:$(this).val()}, function(r){
        		$('#unidad_medida').text(r.data[0].um)
        	})
            procesarRegistro('opciones', 'select', {fk_servicios: $(this).val()}, function(r){
                valores = []                
                $('#fk_opciones').empty()
                $("#fk_opciones").append("<option value=''>Seleccione...</option>")
                for (i = 0; i < r.data.length; i++){
                    valores[r.data[i].id] = {
                        tipo: r.data[i].tipo_valor,
                        costo: r.data[i].costo,
                        precio: r.data[i].precio
                    }
                    $("#fk_opciones").append("<option value="+r.data[i].id+">"+r.data[i].opcion+"</option>")
                }
            })
        })
        //FIN DE LLENADO DE SELECT
        
        cargarRegistros({viaje: idViaje, estado: 'Activo'}, 'crear')

        //Calculo automatico de precios
        $('#fk_opciones').on('change', function(){
            $('#indicativoTipo').text(valores[$(this).val()].tipo)
            $('#indicativoPrecio').val(valores[$(this).val()].precio)
            opcionEscojida = $(this).val()
            calcularTotalDetalle()
        })
        $('#precio').on('change', function(){
            $('#total').val($('#precio').val()*$('#pasajeros').val()*$('#cantidad').val())
        })
        $('#pasajeros').on('change', function(){
            calcularTotalDetalle()
        })        
        $('#cantidad').on('change', function(){
            $('#total').val($('#precio').val()*$('#pasajeros').val()*$('#cantidad').val())
        })

        //Logica de creación de servicios
        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })

        $('#formularioDetalle').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            data.fk_viajes = idViaje
            if(boton == "botonGuardarDetalle"){
                procesarRegistro('viajesDetalle', 'insert', data, function(r){
                    $('#formularioDetalle')[0].reset()
                    $('#pasajeros').val(pasajeros)
                    cargarRegistros({id:r.insertId}, 'crear')
                    $('#modalDetalle').modal('hide')
                })    
            }else{                
                data.id = id
                procesarRegistro('viajesDetalle', 'update', data, function(r){
                    cargarRegistros({id:id}, 'actualizar')
                    $('#formularioDetalle')[0].reset()
                    $('#pasajeros').val(pasajeros)
                    $('#botonGuardarDetalle').show('slow')
                    $('#botonEditarDetalle').hide('slow')
                    $('#modalDetalle').modal('hide')
                })
            }
        })

        //PLANTILLAS
        //Bloque de plantillas
        llenarSelect('plantillas', 'select', {estado:'Activo'}, 'plantilla', 'nombre', 1)
        //Modal para agregar plantilla
        $('#botonMostrarModalPlantilla').on('click', function(){
            $('#formularioPlantillas')[0].reset()
            $('#modalPlantillas').modal('show')
        })
        $('#formularioPlantillas').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            data.viaje = idViaje
            procesarRegistro('viajesDetalle', 'cargarPlantilla', data, function(r){
                $('#contenido').empty()
                cargarRegistros({viaje: idViaje, estado: 'Activo'}, 'crear')
                $('#modalPlantillas').modal('hide')
            })
        })        

        //PASAJEROS
        //Esta es la parte de pasajeros
        $('#formularioPasajeros').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            data.fk_viajes_detalle = id
            procesarRegistro('viajesDetallePax', 'insert', data, function(r){                
                $('#formularioPasajeros')[0].reset()
                cargarRegistrosPasajeros({id: r.insertId})
                calcularTotales()
                alertaPasajerosFaltantes()
            })            
        })

        $('#agregarTodos').on('click', function(){
            procesarRegistro('viajesDetallePax', 'agregarTodos', {detalle: id, cantidad: pasajeros}, function(r){
                cargarRegistrosPasajeros({fk_viajes_detalle: id})
                calcularTotales()
                alertaPasajerosFaltantes()
            })
        })
	}

	function cargarRegistros(data, accion){
        procesarRegistro('viajesDetalle', 'getDetalle', data, function(r){
            let fila
            let total
            let fondo = {
                Gestionar: 'bg-gray',
                Bloqueado: 'bg-amarillo',
                Reservado: 'bg-green',
                Devuelto: 'bg-red'
            }            
            for(let i = 0; i < r.data.length; i++){
                total = (r.data[i].precio - (r.data[i].precio * (r.data[i].descuento/100))) * r.data[i].pasajeros * r.data[i].cantidad
                fila += '<tr id="'+r.data[i].id+'" class="'+fondo[r.data[i].estado]+'">'+
                			'<td class="text-center">'+r.data[i].fecha_inicio+'</td>'+
                            '<td class="text-center">'+r.data[i].hora_inicio+'</td>'+
                            '<td class="text-center">'+r.data[i].destino+'</td>'+
                            '<td>'+r.data[i].servicio+'</td>'+
                            '<td>'+r.data[i].opcion+'</td>'+
                            '<td>'+r.data[i].detalle+'</td>'+
                            '<td class="text-center">$'+currency(r.data[i].precio,0)+'</td>'+
                            '<td class="text-center">'+r.data[i].descuento+'%</td>'+
                            '<td class="text-center">'+r.data[i].pasajeros+'</td>'+
                            '<td class="text-center">'+r.data[i].cantidad+'</td>'+
                            '<td class="text-center">$'+currency(total,0)+'</td>'+
                            '<td class="text-center">'+
                                '<button class="btn btn-default btn-xs" title="Editar item" onClick="mostrarModalEditarDetalle('+r.data[i].id+','+total+')"><i class="fa fa-edit"></i></button> '+
                                '<button class="btn btn-default btn-xs" title="aplicar descuento" onClick="aplicarDescuento('+r.data[i].id+')"><i class="fa"><b>%</b></i></button> '+
                                '<button id="indicador_'+r.data[i].id+'" class="btn btn-warning btn-xs" title="Relacionar viajeros" onClick="cuadrarPasajeros('+r.data[i].id+',\''+r.data[i].servicio+'\')"><i class="fa fa-users"></i></button> '+
                                '<button class="btn btn-default btn-xs" title="Eliminar detalle" onClick="eliminarDetalle('+r.data[i].id+')"><i class="fa fa-close"></i></button>'+
                            '</td>'+
                        '</tr>'                
            }
            if(accion == 'crear'){
                $('#contenido').append(fila)
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }
            calcularTotales()
            alertaPasajerosFaltantes()
            $('#contenidoItinerario').empty()
            construirItinerario(idViaje)
        })
    }

    function mostrarModalEditarDetalle(idDetalle, t){
        id = idDetalle
        $('#formularioDetalle')[0].reset()
        $('#total').val(t)
        llenarFormulario('formularioDetalle','viajesDetalle', 'select', {'id':idDetalle}, function(r){
            procesarRegistro('opciones', 'select', {id:r.data[0].fk_opciones}, function(r){
                valores[r.data[0].id] = {
                    tipo: r.data[0].tipo_valor,
                    costo: r.data[0].costo,
                    precio: r.data[0].precio
                }
                opcionEscojida = r.data[0].id
                $('#fk_opciones').empty()
                $("#fk_opciones").append("<option value="+r.data[0].id+">"+r.data[0].opcion+"</option>")
                $('#indicativoTipo').text(r.data[0].tipo_valor)
                $('#indicativoPrecio').val(r.data[0].precio)
            })
            $('#botonGuardarDetalle').hide('slow')
            $('#botonEditarDetalle').show('slow')
        })
    }

    function eliminarDetalle(idDetalle){
        swal('¿Seguro que desea cancelar el servicio?')
        .then((value) => {
            if (value) {
                procesarRegistro('viajesDetalle', 'update', {id:idDetalle, estado:'Cancelado'}, function(r){                    
                    $('#'+idDetalle).remove()
                    calcularTotales()
                })
            }
        })
    }

    function calcularTotalDetalle(){
        //Aqui evaluo si es costo grupal o unitario para saber el valor individual
        let precio_local = valores[opcionEscojida].precio
        let costo_local = valores[opcionEscojida].costo
        if(valores[opcionEscojida].tipo == 'Grupal'){        
            precio_local = valores[opcionEscojida].precio / $('#pasajeros').val()
            costo_local = valores[opcionEscojida].costo / $('#pasajeros').val()
        }
        $('#precio').val(precio_local)
        $('#costo').val(costo_local)
        $('#total').val(precio_local * $('#pasajeros').val() * $('#cantidad').val())
    }

    function aplicarDescuento(idItem){
        swal("Escriba porcentaje de descuento",{
            content: "input"
        })
        .then((value) => {
            if (value) {
                procesarRegistro('viajesDetalle', 'update', {id: idItem, descuento: value}, function(r){
                    cargarRegistros({id:idItem}, 'actualizar')
                    calcularTotales()
                })
            }
        })
    }

    /*
    Este es el bloque para cuadrar pasajeros
    */
    function cuadrarPasajeros(idDetalle, servicio){
        id = idDetalle
        $('#modalPasajerosTitulo').text(servicio) 
        $('#contenidoPasajeros').html('')
        cargarRegistrosPasajeros({fk_viajes_detalle: idDetalle})
        $('#modalPasajeros').modal('show')
    }

    function cargarRegistrosPasajeros(data){
        procesarRegistro('viajesDetallePax', 'select', data, function(r){
            let fila = ''
            for(let i = 0; i < r.data.length; i++){
                fila += '<tr id="p_'+r.data[i].id+'">'+
                    '<td>'+r.data[i].pasajero+'</td>'+
                    '<td class="text-center">'+
                        '<button class="btn btn-default btn-xs" onClick="borrarPasajeros('+r.data[i].id+')"><i class="fa fa-times"></i></button>'+
                    '</td>'+
                '</tr>'
            }            
            $('#contenidoPasajeros').append(fila)
        })
    }

    function borrarPasajeros(idPasajeros){
        swal('¿Esta seguro de borrar el pasajero?')
        .then((value) => {
            if (value) {
                procesarRegistro('viajesDetallePax', 'delete', {'id':idPasajeros}, function(r){
                    $('#p_'+idPasajeros).remove()
                    calcularTotales()
                    alertaPasajerosFaltantes()
                })
            }
        })
    }    

    function calcularTotales(){
        //precio por pasajero
        let totalCOP = 0
        procesarRegistro('viajesDetallePax', 'getTotalesByPax', {id: idViaje}, function(r){
            if(r.data.length != 0){
                $('#contenidoDetallePax').empty()
                for(let i = 0; i < r.data.length; i++){                    
                    $('#contenidoDetallePax').append('<tr>'+
                        '<td class="text-center">'+r.data[i].pasajero+'</td>'+
                        '<td class="text-right">$'+currency(r.data[i].precio,0)+'</td>'+
                        '</tr>')
                    totalCOP += r.data[i].precio
                }
                $('#contenidoDetallePax').append('<tr>'+
                    '<td class="text-center">TOTAL</td>'+
                    '<td class="text-right">$'+currency(totalCOP,0)+'</td>'+
                    '</tr>')
            }
        })

        //Precio promedio
        procesarRegistro('viajesDetalle', 'getValorTotal', {id: idViaje}, function(r){
            if(r.data.length != 0){
                $('#precio_promedio').text('$ '+currency((r.data[0].valor_total / pasajeros),0))
                $('#precio_total').text('$ '+currency(r.data[0].valor_total,0))
            }
        })
    }

    function alertaPasajerosFaltantes(){
        let centinela = 0
        procesarRegistro('viajesDetallePax', 'getPaxFaltantes', {id: idViaje}, function(r){
            if(r.data.length != 0){
                for(let i = 0; i < r.data.length; i++){                    
                    if(r.data[i].pasajeros == r.data[i].cantidad){
                        $('#indicador_'+r.data[i].id).removeClass('btn-warning').addClass('btn-default')                        
                    }else{
                        centinela = 1
                        $('#indicador_'+r.data[i].id).removeClass('btn-default').addClass('btn-warning')
                    }
                }
            }
        })
    }
</script>