<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <span><?=$_GET['n']?></span>            
            <small>Gestión de servicios</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="intranet/index.php"><i class="fa fa-dashboard"></i> Configuración</a></li>
            <li><a href="intranet/proveedores.php"> Proveedores</a></li>
            <li class="active">Servicios</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row" id="contenido"></div>
        <div class="row text-center">
            <button id="botonMostrarModalServicios" class="btn btn-primary">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </section>
</div>    

<div class="modal fade" id="modalServicios">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalServiciosTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioServicios">
                    <div class="form-group">
                        <label for="categoria" class="control-label">Categoria</label>                        
                        <select id="categoria" name="categoria" class="form-control" required>
                            <option value="Básico">Básico</option>
                            <option value="Privado">Privado</option>
                            <option value="VIP">VIP</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="fk_destinos" class="control-label">Destino</label>
                        <select id="fk_destinos" name="fk_destinos" class="form-control"></select>
                    </div>
                    <div class="form-group">
                        <label for="fk_tipos_servicio" class="control-label">Tipo servicio</label>
                        <select id="fk_tipos_servicio" name="fk_tipos_servicio" class="form-control"></select>
                    </div>                    
                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre</label>
                        <input id="nombre" name="nombre" class="form-control" type="text" required>
                    </div>
                    <div class="form-group">
                        <label for="descripcion" class="control-label">Descripción</label>
                        <textarea id="descripcion" name="descripcion" class="form-control" cols="10" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="recomendaciones" class="control-label">Recomendaciones</label>
                        <textarea id="recomendaciones" name="recomendaciones" class="form-control" cols="10" rows="3"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="incluye" class="control-label">Incluye</label>
                                <textarea id="incluye" name="incluye" class="form-control" cols="10" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="no_incluye" class="control-label">No incluye</label>
                                <textarea id="no_incluye" name="no_incluye" class="form-control" cols="10" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="estado" class="control-label">Estado</label>                        
                        <select id="estado" name="estado" class="form-control" required>
                            <option value="Activo">Activo</option>
                            <option value="Cancelado">Cancelado</option>
                        </select>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button id="botonGuardarServicios" class="btn btn-primary btn-submit" type="submit" form="formularioServicios">
                    <i class="fa fa-save"></i> Guardar
                </button>
                <button id="botonEditarServicios" class="btn btn-default btn-submit" type="submit" form="formularioServicios">
                    <i class="fa fa-refresh"></i> Actualizar
                </button>
            </div>
        </div>            
    </div>        
</div>

<div class="modal fade" id="modalOpciones">
    <div class="modal-dialog modal-lg"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                    <span aria-hidden="true">&times;</span> 
                </button>                 
                <h3 class="modal-title">
                    <span id="modalOpcionesTitulo"></span>
                    <small> Opciones</small>
                </h3>
            </div> 
            <div class="modal-body"> 
                <form id="formularioOpciones">                    
                    <div class="form-group">
                        <label for="opcion">Opción</label>
                        <input id="opcion" name="opcion" class="form-control" type="text" required>
                    </div>                    
                    <div class="form-group" id="visualLlegada">
                        <label for="fk_llegada" class="control-label">Llegada</label>
                        <select id="fk_llegada" name="fk_llegada" class="form-control"></select>
                    </div>
            
                    <div class="form-group" id="visualDuracion">
                        <label for="duracion">Duración(horas)</label>
                        <input id="duracion" name="duracion" class="form-control" type="number">
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="tipo_valor" class="control-label">Tipo valor</label>
                                <select id="tipo_valor" name="tipo_valor" class="form-control" required>
                                    <option value="Unitario">Unitario</option>
                                    <option value="Grupal">Grupal</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="costo">Costo</label>
                                <input id="costo" name="costo" class="form-control numero calcular" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="margen">Margen(%)</label>
                                <input id="margen" name="margen" class="form-control calcular" type="number" required="required" value="30">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="precio">Precio</label>
                                <input id="precio" name="precio" class="form-control numero" type="text" required="required" readonly="readonly">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dias_en_itinerario">Días en itinerario</label>
                                <input id="dias_en_itinerario" name="dias_en_itinerario" class="form-control" type="number" required="required">
                            </div>
                        </div>
                    </div>
                </form>                     
            </div> 
            <div class="modal-footer">
                <button id="botonGuardarOpciones" class="btn btn-primary btn-submit" type="submit" form="formularioOpciones">
                    <i class="fa fa-save"></i> Crear
                </button>
                <button id="botonEditarOpciones" class="btn btn-default btn-submit" type="submit" form="formularioOpciones">
                    <i class="fa fa-refresh"></i> Actualizar
                </button>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <table class="table table-bordered"> 
                        <thead> 
                            <tr>
                                <th>Opción</th>
                                <th>Llegada</th>
                                <th>Duración</th>
                                <th>Tipo de valor</th>
                                <th>Costo</th>
                                <th>Margen</th>
                                <th>Precio</th>
                                <th>Días en itinerario</th>
                                <th>Opciones</th>
                            </tr> 
                        </thead> 
                        <tbody id="contenidoOpciones"></tbody> 
                    </table>
                </div>
            </div>
        </div>             
    </div>         
</div>

<?php require 'footer.php'; ?>   
<script type="text/javascript">
    var idProveedor = <?=$_REQUEST['p']?>;
    var id 
    var boton    
    var idO

    function initLogin(user){        
        $('.numero').number( true, 0,',','.')

        llenarSelect('proveedoresDestinos', 'getDestinos2', {p:idProveedor}, 'fk_destinos', 'nombre', 0)
        llenarSelect('proveedoresOferta', 'getOferta2', {p:idProveedor}, 'fk_tipos_servicio', 'nombre', 0)

        //Llenar select de llegada
        procesarRegistro('proveedoresDestinos','getDestinos2',{p:idProveedor},function(r){            
            $('#fk_llegada').empty()
            $('#fk_llegada').append("<option value='1'>Seleccione...</option>")
            for (let i = 0; i < r.data.length; i++){        
                $('#fk_llegada').append("<option value="+r.data[i].id+">"+r.data[i].nombre+"</option>")
            }        
        })

        cargarRegistros({fk_proveedores:idProveedor, estado:'Activo'}, 'crear')

        $('#botonMostrarModalServicios').on('click', function(){
            $('#formularioServicios')[0].reset()
            $('#modalServiciosTitulo').text('Crear servicio')            
            $('#botonEditarServicios').hide()
            $('#botonGuardarServicios').show()
            $('#modalServicios').modal('show')
        })
        
        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })

        $('#formularioServicios').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            data.fk_proveedores = idProveedor
            if(boton == "botonGuardarServicios"){                
                procesarRegistro('servicios', 'insert', data, function(r){
                    swal('Perfecto!', 'El servicio se creo correctamente', 'success')
                    cargarRegistros({fk_proveedores:idProveedor, id:r.insertId}, 'crear')
                    $('#modalServicios').modal('hide')
                })    
            }else{                
                data.id = id
                procesarRegistro('servicios', 'update', data, function(r){                    
                    swal('Perfecto!', 'El servicio se actualizo correctamente', 'success')
                    if(data.estado == 'Cancelado'){
                        $('#'+data.id).remove()
                    }else{
                        cargarRegistros({fk_proveedores:idProveedor, id:id}, 'actualizar')
                    }
                    $('#modalServicios').modal('hide')
                })
            }
        })        

        //Esta es la logica de opciones
        //Logica para calcular el precio de venta
        $('.calcular').on('change', function(){
            valor = $('#costo').val() / (1-($('#margen').val()/100))
            temporal = parseInt(valor / 1000)*1000+1000            
            $('#precio').val(temporal)
        })

        $('#formularioOpciones').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            if(boton == "botonGuardarOpciones"){
                data.fk_servicios = id                
                procesarRegistro('opciones', 'insert', data, function(r){
                    $('#formularioOpciones')[0].reset()
                    cargarRegistrosOpciones({'id':r.insertId}, 'crear')
                    $('#botonEditarOpciones').hide()
                })
            }else{                
                data.id = idO
                procesarRegistro('opciones', 'update', data, function(r){
                    cargarRegistrosOpciones({'id':idO}, 'actualizar')
                })
            }            
        })
    }    

    function cargarRegistros(data, accion){        
        procesarRegistro('servicios', 'getServicios', data, function(r){            
            let fila = ''
            for(let i = 0; i < r.data.length; i++){                
                fila += '<div class="col-md-4" id="'+r.data[i].id+'">'+
                            '<div class="box box-solid">'+
                                '<div class="box-header with-border">'+
                                    '<div class="row">'+
                                        '<div class="col-xs-1">'+
                                            '<span class="label label-default"><i class="fa '+r.data[i].icono+'"></i></span>'+
                                        '</div>'+
                                        '<div class="col-xs-8 text-center">'+
                                            '<h3 class="box-title">'+r.data[i].servicio+'</h3>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="box-tools pull-right">'+
                                        '<label class="btn btn-default btn-xs" for="foto_'+r.data[i].id+'">'+
                                            '<input id="foto_'+r.data[i].id+'" onChange="cargarImagen('+r.data[i].id+')" type="file"  style="display:none;"/><i class="fa fa-camera" aria-hidden="true"></i>'+
                                        '</label>'+
                                        '<button class="btn btn-default btn-xs" title="Editar servicio" onClick="mostrarModalEditarServicios('+r.data[i].id+')"><i class="fa fa-pencil-square-o"></i></button>'+
                                        '<button class="btn btn-default btn-xs" title="Editar opciones" onClick="mostrarModalOpciones('+r.data[i].id+',\''+r.data[i].servicio+'\','+r.data[i].idts+')"><i class="fa fa-sliders"></i></button>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="box-body">'+
                                    '<div style="position:relative;"">'+
                                        '<span class="label label-default" style="position:absolute;top:0em;"><i class="fa fa-map-marker"></i> '+r.data[i].departamento+'</span> '+
                                        '<span class="label label-default" style="position:absolute;top:2em;"><i class="fa fa-map-marker"></i> '+r.data[i].zona+'</span> '+
                                        '<span class="label label-default" style="position:absolute;top:4em;"><i class="fa fa-map-marker"></i> '+r.data[i].destino+'</span> '+
                                        '<img class="img-responsive centrar" src="assets/img/servicios/'+r.data[i].id+'.jpg" id="f_'+r.data[i].id+'"><br>'+
                                    '</div>'+
                                    '<div class="row">'+
                                        '<div class="col-xs-12">'+
                                            '<div class="panel panel-default">'+
                                                '<div class="panel-heading text-center">Descripción</div>'+
                                                '<div class="panel-body text-justify">'+r.data[i].descripcion+'</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-xs-12">'+
                                            '<div class="panel panel-default">'+
                                                '<div class="panel-heading text-center">Recomendaciones</div>'+
                                                '<div class="panel-body text-justify">'+r.data[i].recomendaciones+'</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-xs-6">'+
                                            '<div class="panel panel-default">'+
                                                '<div class="panel-heading text-center">Incluye</div>'+
                                                '<div class="panel-body">'+r.data[i].incluye+'</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-xs-6">'+
                                            '<div class="panel panel-default">'+
                                                '<div class="panel-heading text-center">No Incluye</div>'+
                                                '<div class="panel-body">'+r.data[i].no_incluye+'</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'
            }
            if(accion == 'crear'){
                $('#contenido').append(fila)
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }            
        })
    }

    function mostrarModalEditarServicios(idServicio){
        id = idServicio
        llenarFormulario('formularioServicios','servicios', 'select', {'id':idServicio}, function(r){
            $('#modalServiciosTitulo').text('Modificar servicio')
            $('#botonGuardarServicios').hide()
            $('#botonEditarServicios').show()            
            $('#modalServicios').modal('show')            
        })
    }

    /*Este bloque es el de opciones*/
    function mostrarModalOpciones(idServicio, nombre, idts){
        id = idServicio
        $('#modalOpcionesTitulo').text(nombre)
        $('#formularioOpciones')[0].reset()
        if(idts == 2){
            $('#visualLlegada').hide()
            $('#visualDuracion').show()
        }else if(idts == 5 || idts == 6){
            $('#visualLlegada').show()
            $('#visualDuracion').show()
        }else{
            $('#visualLlegada').hide()
            $('#visualDuracion').hide()
        }
        $('#botonGuardarOpciones').show()
        $('#botonEditarOpciones').hide()
        $('#contenidoOpciones').html('')        
        cargarRegistrosOpciones({'fk_servicios':idServicio}, 'crear')        
        $('#modalOpciones').modal('show')
    }

    function cargarRegistrosOpciones(data, accion){
        procesarRegistro('opciones', 'opcionesDestinos', data, function(r){            
            let fila = ''
            for(let i = 0; i < r.data.length; i++){
                fila += '<tr id="o_'+r.data[i].id+'">'+
                    '<td>'+r.data[i].opcion+'</td>'+
                    '<td>'+r.data[i].llegada+'</td>'+
                    '<td>'+r.data[i].duracion+' horas</td>'+
                    '<td class="text-center">'+r.data[i].tipo_valor+'</td>'+
                    '<td class="text-right">$'+currency(r.data[i].costo,0)+'</td>'+
                    '<td class="text-center">'+r.data[i].margen+'%</td>'+
                    '<td class="text-right">$'+currency(r.data[i].precio,0)+'</td>'+
                    '<td class="text-center">'+r.data[i].dias_en_itinerario+'</td>'+
                    '<td class="text-center">'+
                        '<button class="btn btn-default btn-xs" onClick="funcionMostrarEditarOpciones('+r.data[i].id+')"><i class="fa fa-pencil-square-o"></i></button>'+
                    '</td>'+
                '</tr>'                    
            }
            if(accion == 'crear'){ 
                $('#contenidoOpciones').append(fila)
            }else{ 
                $('#o_'+r.data[0].id).replaceWith(fila) 
            }            
        })
    }

    function funcionMostrarEditarOpciones(idOpcion){ 
        idO = idOpcion
        llenarFormulario('formularioOpciones', 'opciones', 'select', {'id':idOpcion}, function(r){
            $('#botonEditarOpciones').show()
            $('#botonGuardarOpciones').hide()
        }) 
    }

    function cargarImagen(idServicio){
        let archivo = $('#foto_'+idServicio)[0].files[0]        
        let ext = archivo.name.split('.')        
        if(ext[1] == 'jpg' || ext[1] == 'png' || ext[1] == 'jpeg' || ext[1] == 'JPG'){
            //se carga la imagen
            var fd = new FormData()        
            fd.append('objeto','servicios')
            fd.append('metodo','subirImagen')
            fd.append('datos',idServicio)
            fd.append('file',archivo)
            $.ajax({
                url: 'class/frontController.php',
                type: 'post',
                data: fd,
                contentType: false,                
                processData: false,
                dataType: 'json',
                success: function(r){
                    d = new Date();
                    $('#f_'+idServicio).attr("src", "assets/img/servicios/"+idServicio+".jpg?"+d.getTime())
                },
                error: function(xhr,status){
                    console.log('Disculpe, existio un problema procesando')
                }
            })
        }else{
            swal('Ups!', 'Tipo de archivo no permitido', 'error')
        }
    }
</script>
</body>
</html>