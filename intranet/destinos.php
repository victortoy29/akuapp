<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Destinos
            <small>Administración</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="intranet/index.php"><i class="fa fa-dashboard"></i> Configuración</a></li>
            <li class="active">Destinos</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1" data-toggle="tab" id="t1">Activos</a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab" id="t2">Cancelados</a>
                        </li>
                        <li class="pull-right">
                            <button id="botonMostrarModalDestinos" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></button>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="table-responsive">
                                <table id="tablaContenidoActivos" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Departamento</th>
                                            <th class="text-center">Zona</th>
                                            <th class="text-center">Destino</th>
                                            <th class="text-center">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenidoActivos"></tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Departamento</th>
                                            <th class="text-center">Zona</th>
                                            <th class="text-center">Destino</th>
                                            <th class="text-center">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenidoCancelados"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalDestinos">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalDestinosTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioDestinos">
                    <div class="form-group">
                        <label for="fk_departamentos" class="control-label">Departamento</label>
                        <select id="fk_departamentos" class="form-control" required></select>
                    </div>
                    <div class="form-group">
                        <label for="fk_zonas" class="control-label">Zonas</label>
                        <select id="fk_zonas" name="fk_zonas" class="form-control" required></select>
                    </div>
                    <div class="form-group">
                        <label for="nombre" class="control-label">Destino</label>
                        <input id="nombre" name="nombre" class="form-control" type="text" required>
                    </div>                    
                    <div class="form-group">
                        <label for="estado" class="control-label">Estado</label>                        
                        <select id="estado" name="estado" class="form-control" required>                            
                            <option value="Activo">Activo</option>
                            <option value="Cancelado">Cancelado</option>
                        </select>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button id="botonGuardarDestinos" class="btn btn-primary btn-submit" type="submit" form="formularioDestinos">
                    <i class="fa fa-save"></i> Guardar
                </button>
                <button id="botonEditarDestinos" class="btn btn-default btn-submit" type="submit" form="formularioDestinos">
                    <i class="fa fa-refresh"></i> Actualizar
                </button>
            </div>
        </div>            
    </div>        
</div>

<?php require 'footer.php'; ?>   
<script type="text/javascript">
    var id 
    var boton

    function initLogin(user){
        llenarSelect('departamentos', 'getDepartamentos', {estado:'Activo'}, 'fk_departamentos', 'nombre', 1)
        llenarSelect('zonas', 'getZonas', {estado:'Activo'}, 'fk_zonas', 'zona', 1)
        $('#fk_departamentos').on('change', function(){
            llenarSelect('zonas', 'getZonas', {fk_departamentos:$(this).val(), estado:'Activo'}, 'fk_zonas', 'zona', 1)
        })

        cargarRegistros({'estado':'Activo'}, 'crear', 'contenidoActivos', function(){
            $('#tablaContenidoActivos').DataTable({
                "lengthMenu": [ 50, 100, 200, 300 ],
                "language":{
                    "decimal":        "",
                    "emptyTable":     "Sin datos para mostrar",
                    "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 de 0 de 0 registros",
                    "infoFiltered":   "(Filtrado de un total de _MAX_ registros)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "Ningún registro encontrado",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Sig",
                        "previous":   "Ant"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "bDestroy": true
                }
            })
        })

        $('#botonMostrarModalDestinos').on('click', function(){
            $('#formularioDestinos')[0].reset()
            $('#modalDestinosTitulo').text('Crear destino')            
            $('#botonEditarDestinos').hide()
            $('#botonGuardarDestinos').show()
            $('#modalDestinos').modal('show')
        })
        
        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })

        $('#formularioDestinos').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            if(boton == "botonGuardarDestinos"){
                procesarRegistro('destinos', 'insert', data, function(r){
                    swal('Perfecto!', 'El destino se creo correctamente', 'success')
                    cargarRegistros({'id':r.insertId}, 'crear', 'contenidoActivos', function(){
                        $('#modalDestinos').modal('hide')    
                    })                    
                })    
            }else{                
                data.id = id
                procesarRegistro('destinos', 'update', data, function(r){                    
                    swal('Perfecto!', 'El destino se actualizo correctamente', 'success')
                    if(data.estado == 'Cancelado'){
                        $('#'+data.id).remove()
                    }else{
                        cargarRegistros({'id':id}, 'actualizar', 'contenidoActivos', function(){
                            console.log('ok')
                        })
                    }
                    $('#modalDestinos').modal('hide')
                })
            }
        })

        $('#t1').on('click', function(){
            $('#contenidoActivos').html('')
            $('#botonMostrarModalDestinos').show()
            cargarRegistros({'estado':'Activo'}, 'crear', 'contenidoActivos', function(){
                console.log('ok')
            })
        })
        $('#t2').on('click', function(){
            $('#contenidoCancelados').html('')
            $('#botonMostrarModalDestinos').hide()
            cargarRegistros({'estado':'Cancelado'}, 'crear', 'contenidoCancelados', function(){
                console.log('ok')
            })
        })
    }

    function cargarRegistros(data, accion, elemento, callback){
        procesarRegistro('destinos', 'getDestinos', data, function(r){            
            let fila
            let opciones
            for(let i = 0; i < r.data.length; i++){
                if(r.data[i].id != 2){
                    opciones = '<button class="btn btn-default btn-xs" onClick="mostrarModalEditarDestinos('+r.data[i].id+')" title="Editar Destino"><i class="fa fa-pencil-square-o"></i></button>'                
                    if(r.data[i].estado == 'Cancelado'){
                        opciones = '<button class="btn btn-default btn-xs" onClick="funcionReactivarDestino('+r.data[i].id+')" title="Reactivar Destino"><i class="fa fa-reply"></i></button>'
                    }
                    fila += '<tr id="'+r.data[i].id+'">'+                            
                                '<td>'+r.data[i].departamento+'</td>'+
                                '<td>'+r.data[i].zona+'</td>'+
                                '<td>'+r.data[i].destino+'</td>'+
                                '<td class="text-center">'+
                                    opciones+
                                '</td>'+
                            '</tr>'
                }
            }
            if(accion == 'crear'){
                $('#'+elemento).append(fila)                        
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }
            callback()
        })
    }

    function mostrarModalEditarDestinos(idDestino){
        id = idDestino
        llenarFormulario('formularioDestinos','destinos', 'select', {'id':idDestino}, function(r){
            $('#modalDestinosTitulo').text('Modificar destino')
            $('#botonGuardarDestinos').hide()
            $('#botonEditarDestinos').show()            
            $('#modalDestinos').modal('show')            
        })
    }    

    function funcionReactivarDestino(idDestino){
        swal('¿Esta seguro de reactivar el Destino?')
        .then((value) => {
            if (value) {
                procesarRegistro('destinos', 'update', {'id':idDestino, 'estado':'Activo'}, function(r){                    
                    $('#'+idDestino).remove()
                })
            }
        })
    }
</script>
</body>
</html>