<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Tipos de cliente
            <small>Administración</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="intranet/index.php"><i class="fa fa-dashboard"></i> Configuración</a></li>
            <li class="active">Tipos de cliente</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1" data-toggle="tab" id="t1">Activos</a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab" id="t2">Cancelados</a>
                        </li>
                        <li class="pull-right">
                            <button id="botonMostrarModalTipos" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></button>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="table-responsive">
                                <table id="tablaContenidoActivos" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Grupo</th>
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">Párrafo</th>
                                            <th class="text-center">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenidoActivos"></tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Grupo</th>
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">Párrafo</th>
                                            <th class="text-center">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenidoCancelados"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalTipos">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalTiposTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioTipos">
                    <div class="form-group">
                        <label for="grupo" class="control-label">Grupo</label>
                        <select id="grupo" name="grupo" class="form-control" required>                            
                            <option value="Persona natural">Persona natural</option>
                            <option value="Persona Jurídica">Persona Jurídica</option>
                            <option value="ONG">ONG</option>
                            <option value="Gobierno">Gobierno</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre</label>
                        <input id="nombre" name="nombre" class="form-control" type="text" required>
                    </div>
                    <div class="form-group">
                        <label for="parrafo" class="control-label">Parrafo</label>
                        <textarea id="parrafo" name="parrafo" class="form-control" cols="10" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="estado" class="control-label">Estado</label>                        
                        <select id="estado" name="estado" class="form-control" required>                            
                            <option value="Activo">Activo</option>
                            <option value="Cancelado">Cancelado</option>
                        </select>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button id="botonGuardarTipos" class="btn btn-primary btn-submit" type="submit" form="formularioTipos">
                    <i class="fa fa-save"></i> Guardar
                </button>
                <button id="botonEditarTipos" class="btn btn-default btn-submit" type="submit" form="formularioTipos">
                    <i class="fa fa-refresh"></i> Actualizar
                </button>
            </div>
        </div>            
    </div>        
</div>

<?php require 'footer.php'; ?>   
<script type="text/javascript">
    var id 
    var boton

    function initLogin(user){
        cargarRegistros({'estado':'Activo'}, 'crear', 'contenidoActivos', function(){
            $('#tablaContenidoActivos').DataTable({
                "lengthMenu": [ 50, 100, 200, 300 ],
                "language":{
                    "decimal":        "",
                    "emptyTable":     "Sin datos para mostrar",
                    "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 de 0 de 0 registros",
                    "infoFiltered":   "(Filtrado de un total de _MAX_ registros)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "Ningún registro encontrado",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Sig",
                        "previous":   "Ant"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "bDestroy": true
                }
            })
        })

        $('#botonMostrarModalTipos').on('click', function(){
            $('#formularioTipos')[0].reset()
            $('#modalTiposTitulo').text('Crear canal de venta')            
            $('#botonEditarTipos').hide()
            $('#botonGuardarTipos').show()
            $('#modalTipos').modal('show')
        })
        
        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })

        $('#formularioTipos').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            if(boton == "botonGuardarTipos"){
                procesarRegistro('tiposCliente', 'insert', data, function(r){
                    swal('Perfecto!', 'El tipo de cliente se creo correctamente', 'success')
                    cargarRegistros({'id':r.insertId}, 'crear', 'contenidoActivos', function(){
                        $('#modalTipos').modal('hide')    
                    })                    
                })    
            }else{                
                data.id = id
                procesarRegistro('tiposCliente', 'update', data, function(r){                    
                    swal('Perfecto!', 'El tipo de cliente se actualizo correctamente', 'success')
                    if(data.estado == 'Cancelado'){
                        $('#'+data.id).remove()
                    }else{
                        cargarRegistros({'id':id}, 'actualizar', 'contenidoActivos', function(){
                            console.log('ok')
                        })
                    }
                    $('#modalTipos').modal('hide')
                })
            }
        })

        $('#t1').on('click', function(){
            $('#contenidoActivos').html('')
            $('#botonMostrarModalTipos').show()
            cargarRegistros({'estado':'Activo'}, 'crear', 'contenidoActivos', function(){
                console.log('ok')
            })
        })
        $('#t2').on('click', function(){
            $('#contenidoCancelados').html('')
            $('#botonMostrarModalTipos').hide()
            cargarRegistros({'estado':'Cancelado'}, 'crear', 'contenidoCancelados', function(){
                console.log('ok')
            })
        })
    }

    function cargarRegistros(data, accion, elemento, callback){
        procesarRegistro('tiposCliente', 'select', data, function(r){            
            let fila
            let opciones
            for(let i = 0; i < r.data.length; i++){                
                opciones = '<button class="btn btn-default btn-xs" onClick="mostrarModalEditarTipos('+r.data[i].id+')" title="Editar tipo"><i class="fa fa-pencil-square-o"></i></button>'                
                if(r.data[i].estado == 'Cancelado'){
                    opciones = '<button class="btn btn-default btn-xs" onClick="funcionReactivarTipos('+r.data[i].id+')" title="Reactivar tipo"><i class="fa fa-reply"></i></button>'
                }
                fila += '<tr id="'+r.data[i].id+'">'+
                            '<td>'+r.data[i].grupo+'</td>'+
                            '<td>'+r.data[i].nombre+'</td>'+
                            '<td>'+r.data[i].parrafo+'</td>'+
                            '<td class="text-center">'+
                                opciones+
                            '</td>'+
                        '</tr>' 
            }
            if(accion == 'crear'){
                $('#'+elemento).append(fila)       
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }
            callback()
        })
    }

    function mostrarModalEditarTipos(idTipos){
        id = idTipos
        llenarFormulario('formularioTipos','tiposCliente', 'select', {'id':idTipos}, function(r){
            $('#modalTiposTitulo').text('Modificar tipo de cliente')
            $('#botonGuardarTipos').hide()
            $('#botonEditarTipos').show()            
            $('#modalTipos').modal('show')            
        })
    }    

    function funcionReactivarTipos(idTipos){
        swal('¿Esta seguro de reactivar el tipo de cliente?')
        .then((value) => {
            if (value) {
                procesarRegistro('tiposCliente', 'update', {'id':idTipos, 'estado':'Activo'}, function(r){                    
                    $('#'+idTipos).remove()
                })
            }
        })
    }
</script>
</body>
</html>