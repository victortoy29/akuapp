<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Zonas
            <small>Administración</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="intranet/index.php"><i class="fa fa-dashboard"></i> Configuración</a></li>
            <li class="active">Zonas</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1" data-toggle="tab" id="t1">Activos</a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab" id="t2">Cancelados</a>
                        </li>
                        <li class="pull-right">
                            <button id="botonMostrarModalZonas" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></button>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="table-responsive">
                                <table id="tablaContenidoActivos" class="table table-bordered">
                                    <thead>
                                        <tr>
                                        	<th class="text-center">Departamento</th>
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenidoActivos"></tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                        	<th class="text-center">Departamento</th>
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenidoCancelados"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalZonas">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalZonasTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioZonas">
                	<div class="form-group">
                        <label for="fk_departamentos" class="control-label">Departamento</label>
                        <select id="fk_departamentos" name="fk_departamentos" class="form-control" required></select>
                    </div>
                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre</label>
                        <input id="nombre" name="nombre" class="form-control" type="text" required>
                    </div>
                    <div class="form-group">
                        <label for="estado" class="control-label">Estado</label>                        
                        <select id="estado" name="estado" class="form-control" required>                            
                            <option value="Activo">Activo</option>
                            <option value="Cancelado">Cancelado</option>
                        </select>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button id="botonGuardarZonas" class="btn btn-primary btn-submit" type="submit" form="formularioZonas">
                    <i class="fa fa-save"></i> Guardar
                </button>
                <button id="botonEditarZonas" class="btn btn-default btn-submit" type="submit" form="formularioZonas">
                    <i class="fa fa-refresh"></i> Actualizar
                </button>
            </div>
        </div>            
    </div>        
</div>

<?php require 'footer.php'; ?>   
<script type="text/javascript">
    var id 
    var boton

    function initLogin(user){
    	llenarSelect('departamentos', 'getDepartamentos', {estado:'Activo'}, 'fk_departamentos', 'nombre', 1)

        cargarRegistros({'estado':'Activo'}, 'crear', 'contenidoActivos', function(){
            $('#tablaContenidoActivos').DataTable({
                "lengthMenu": [ 50, 100, 200, 300 ],
                "language":{
                    "decimal":        "",
                    "emptyTable":     "Sin datos para mostrar",
                    "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 de 0 de 0 registros",
                    "infoFiltered":   "(Filtrado de un total de _MAX_ registros)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "Ningún registro encontrado",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Sig",
                        "previous":   "Ant"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "bDestroy": true
                }
            })
        })

        $('#botonMostrarModalZonas').on('click', function(){
            $('#formularioZonas')[0].reset()
            $('#modalZonasTitulo').text('Crear zona')
            $('#botonEditarZonas').hide()
            $('#botonGuardarZonas').show()
            $('#modalZonas').modal('show')
        })
        
        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })

        $('#formularioZonas').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            if(boton == "botonGuardarZonas"){
                procesarRegistro('zonas', 'insert', data, function(r){
                    swal('Perfecto!', 'La zona se creo correctamente', 'success')
                    cargarRegistros({'id':r.insertId}, 'crear', 'contenidoActivos', function(){
                        $('#modalZonas').modal('hide')    
                    })                    
                })    
            }else{                
                data.id = id
                procesarRegistro('zonas', 'update', data, function(r){                    
                    swal('Perfecto!', 'La zona se actualizo correctamente', 'success')
                    if(data.estado == 'Cancelado'){
                        $('#'+data.id).remove()
                    }else{
                        cargarRegistros({'id':id}, 'actualizar', 'contenidoActivos', function(){
                            console.log('ok')
                        })
                    }
                    $('#modalZonas').modal('hide')
                })
            }
        })

        $('#t1').on('click', function(){
            $('#contenidoActivos').html('')
            $('#botonMostrarModalZonas').show()
            cargarRegistros({'estado':'Activo'}, 'crear', 'contenidoActivos', function(){
                console.log('ok')
            })
        })
        $('#t2').on('click', function(){
            $('#contenidoCancelados').html('')
            $('#botonMostrarModalZonas').hide()
            cargarRegistros({'estado':'Cancelado'}, 'crear', 'contenidoCancelados', function(){
                console.log('ok')
            })
        })
    }

    function cargarRegistros(data, accion, elemento, callback){
        procesarRegistro('zonas', 'getZonas', data, function(r){
            let fila
            let opciones
            for(let i = 0; i < r.data.length; i++){                
                opciones = '<button class="btn btn-default btn-xs" onClick="mostrarModalEditarZonas('+r.data[i].id+')" title="Editar zona"><i class="fa fa-pencil-square-o"></i></button>'                
                if(r.data[i].estado == 'Cancelado'){
                    opciones = '<button class="btn btn-default btn-xs" onClick="funcionReactivarZona('+r.data[i].id+')" title="Reactivar zona"><i class="fa fa-reply"></i></button>'
                }
                fila += '<tr id="'+r.data[i].id+'">'+
                			'<td>'+r.data[i].departamento+'</td>'+
                            '<td>'+r.data[i].zona+'</td>'+
                            '<td class="text-center">'+
                                opciones+
                            '</td>'+
                        '</tr>'                
            }
            if(accion == 'crear'){
                $('#'+elemento).append(fila)                        
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }
            callback()
        })
    }

    function mostrarModalEditarZonas(idZona){
        id = idZona
        llenarFormulario('formularioZonas','zonas', 'select', {'id':idZona}, function(r){
            $('#modalZonasTitulo').text('Modificar zona')
            $('#botonGuardarZonas').hide()
            $('#botonEditarZonas').show()            
            $('#modalZonas').modal('show')            
        })
    }    

    function funcionReactivarZona(idZona){
        swal('¿Esta seguro de reactivar el zona?')
        .then((value) => {
            if (value) {
                procesarRegistro('zonas', 'update', {'id':idZona, 'estado':'Activo'}, function(r){                    
                    $('#'+idZona).remove()
                })
            }
        })
    }
</script>
</body>
</html>