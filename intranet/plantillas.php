<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Plantillas
            <small>Gestión</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="intranet/index.php"><i class="fa fa-dashboard"></i> Configuración</a></li>
            <li class="active">Plantillas</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row" id="contenido"></div>
        <div class="row text-center">
            <button id="botonMostrarModalPlantillas" class="btn btn-primary">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </section>
</div>

<div class="modal fade" id="modalPlantillas">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalPlantillasTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioPlantillas">                    
                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre</label>
                        <input id="nombre" name="nombre" class="form-control" type="text" required>
                    </div>
                    <div class="form-group">
                        <label for="descripcion" class="control-label">Descripcion</label>
                        <textarea id="descripcion" name="descripcion" class="form-control" cols="10" rows="3"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="pasajeros" class="control-label">Pasajeros</label>
                                <input id="pasajeros" name="pasajeros" class="form-control" type="number" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="dias" class="control-label">Días</label>
                                <input id="dias" name="dias" class="form-control" type="number" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="incluye" class="control-label">Incluye</label>
                                <textarea id="incluye" name="incluye" class="form-control" cols="10" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="no_incluye" class="control-label">No incluye</label>
                                <textarea id="no_incluye" name="no_incluye" class="form-control" cols="10" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="promocion" class="control-label">Promocion</label>
                                <select id="promocion" name="promocion" class="form-control" required>
                                    <option value=0>No</option>
                                    <option value=1>Sí</option>                                    
                                </select>
                            </div>        
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="filtros" class="control-label">Filtros</label>
                                <textarea id="filtros" name="filtros" class="form-control" cols="10" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="estado" class="control-label">Estado</label>                        
                        <select id="estado" name="estado" class="form-control" required>                            
                            <option value="Activo">Activo</option>
                            <option value="Cancelado">Cancelado</option>
                        </select>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button id="botonGuardarPlantillas" class="btn btn-primary btn-submit" type="submit" form="formularioPlantillas">
                    <i class="fa fa-save"></i> Guardar
                </button>
                <button id="botonEditarPlantillas" class="btn btn-default btn-submit" type="submit" form="formularioPlantillas">
                    <i class="fa fa-refresh"></i> Actualizar
                </button>
            </div>
        </div>            
    </div>        
</div>

<?php require 'footer.php'; ?>   
<script type="text/javascript">
    var id 
    var boton

    function initLogin(user){
        cargarRegistros({'estado':'Activo'}, 'crear')

        $('#botonMostrarModalPlantillas').on('click', function(){
            $('#formularioPlantillas')[0].reset()
            $('#modalPlantillasTitulo').text('Crear plantilla')
            $('#botonEditarPlantillas').hide()
            $('#botonGuardarPlantillas').show()
            $('#modalPlantillas').modal('show')
        })
        
        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })

        $('#formularioPlantillas').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            if(boton == "botonGuardarPlantillas"){
                procesarRegistro('plantillas', 'insert', data, function(r){
                    swal('Perfecto!', 'La plantilla se creo correctamente', 'success')
                    cargarRegistros({id:r.insertId}, 'crear')
                    $('#modalPlantillas').modal('hide')
                })    
            }else{                
                data.id = id
                procesarRegistro('plantillas', 'update', data, function(r){                    
                    swal('Perfecto!', 'La plantilla se actualizo correctamente', 'success')
                    if(data.estado == 'Cancelado'){
                        $('#'+data.id).remove()
                    }else{
                        cargarRegistros({id:id}, 'actualizar')
                    }
                    $('#modalPlantillas').modal('hide')
                })
            }
        })
    }

    function cargarRegistros(data, accion){        
        procesarRegistro('plantillas', 'select', data, function(r){            
            let fila = ''
            let promo = ''
            for(let i = 0; i < r.data.length; i++){
                promo = ''
                if(r.data[i].promocion == 1){
                    promo = '<span class="label label-success">Promoción</span>'
                }
                fila += '<div class="col-md-4" id="'+r.data[i].id+'">'+
                            '<div class="box box-solid">'+
                                '<div class="box-header with-border">'+
                                    '<div class="row">'+
                                        '<div class="col-xs-3">'+promo+'</div>'+
                                        '<div class="col-xs-6 text-center">'+
                                            '<h3 class="box-title">'+r.data[i].nombre+'</h3>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="box-tools pull-right">'+
                                        '<label class="btn btn-default btn-xs" for="foto_'+r.data[i].id+'">'+
                                            '<input id="foto_'+r.data[i].id+'" onChange="cargarImagen('+r.data[i].id+')" type="file"  style="display:none;"/><i class="fa fa-camera"></i>'+
                                        '</label>'+
                                        '<button class="btn btn-default btn-xs" title="Editar plantilla" onClick="mostrarModalEditarPlantillas('+r.data[i].id+')"><i class="fa fa-pencil-square-o"></i></button>'+
                                        '<a class="btn btn-default btn-xs" title="Configurar detalle" href="intranet/plantillasDetalle.php?p='+r.data[i].id+'"><i class="fa fa-navicon"></i></a>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="box-body">'+
                                    '<p class="text-center">'+
                                        '<img class="img-responsive" src="assets/img/plantillas/'+r.data[i].id+'.jpg" id="f_'+r.data[i].id+'">'+
                                    '</p>'+
                                    '<div class="row">'+
                                        '<div class="col-xs-12">'+
                                            '<div class="panel panel-default">'+
                                                '<div class="panel-heading text-center">Descripción</div>'+
                                                '<div class="panel-body text-justify">'+r.data[i].descripcion+'</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="row">'+
                                        '<div class="col-xs-4">'+
                                            '<div class="panel panel-default text-center">'+                                                
                                                '<div class="panel-body"><i class="fa fa-users"></i> X '+r.data[i].pasajeros+'</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-xs-4">'+
                                            '<div class="panel panel-default text-center">'+
                                                '<div class="panel-body">Días: '+r.data[i].dias+'</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-xs-4">'+
                                            '<div class="panel panel-default text-center">'+
                                                '<div class="panel-body">Valor: $'+currency(r.data[i].total,0)+'</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="row">'+
                                        '<div class="col-xs-4">'+
                                            '<div class="panel panel-default">'+
                                                '<div class="panel-heading text-center">Incluye</div>'+
                                                '<div class="panel-body">'+r.data[i].incluye+'</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-xs-4">'+
                                            '<div class="panel panel-default">'+
                                                '<div class="panel-heading text-center">No Incluye</div>'+
                                                '<div class="panel-body">'+r.data[i].no_incluye+'</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-xs-4">'+
                                            '<div class="panel panel-default">'+
                                                '<div class="panel-heading text-center">Filtros</div>'+
                                                '<div class="panel-body">'+r.data[i].filtros+'</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'
            }
            if(accion == 'crear'){
                $('#contenido').append(fila)
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }            
        })
    }

    function mostrarModalEditarPlantillas(idPlantilla){
        id = idPlantilla
        llenarFormulario('formularioPlantillas','plantillas', 'select', {id:idPlantilla}, function(r){
            $('#modalPlantillasTitulo').text('Modificar plantilla')
            $('#botonGuardarPlantillas').hide()
            $('#botonEditarPlantillas').show()            
            $('#modalPlantillas').modal('show')            
        })
    }    

    function cargarImagen(idPlantilla){
        let archivo = $('#foto_'+idPlantilla)[0].files[0]        
        let ext = archivo.name.split('.')        
        if(ext[1] == 'jpg' || ext[1] == 'png' || ext[1] == 'jpeg' || ext[1] == 'JPG'){
            //se carga la imagen
            var fd = new FormData()        
            fd.append('objeto','plantillas')
            fd.append('metodo','subirImagen')
            fd.append('datos',idPlantilla)
            fd.append('file',archivo)
            $.ajax({
                url: 'class/frontController.php',
                type: 'post',
                data: fd,
                contentType: false,                
                processData: false,
                dataType: 'json',
                success: function(r){
                    d = new Date();
                    $('#f_'+idPlantilla).attr("src", "assets/img/plantillas/"+idPlantilla+".jpg?"+d.getTime())
                },
                error: function(xhr,status){
                    console.log('Disculpe, existio un problema procesando')
                }
            })
        }else{
            swal('Ups!', 'Tipo de archivo no permitido', 'error')
        }
    }
</script>
</body>
</html>