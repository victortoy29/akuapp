<?php
setlocale(LC_ALL,"es_ES");

require_once '../class/viajes.php';
require_once '../class/viajesDetalle.php';
require_once '../plugins/fpdf/fpdf.php';

$objViaje = new viajes();
$viaje = $objViaje->getViajes(['campo'=>'id', 'valor'=> $_GET['idV']]);

$itinerario = [];
$inicio = $viaje['data'][0]['fecha_inicio'];
$date1 = date_create($viaje['data'][0]['fecha_inicio']);
$date2 = date_create($viaje['data'][0]['fecha_fin']);
$diff = date_diff($date1,$date2);
$dias = $diff->format("%a");
for ($i=0; $i <= $dias; $i++) { 
	$itinerario[$inicio]=[];
	$inicio = date('Y-m-d', strtotime($inicio. ' + 1 days'));
}

$objDetalle = new viajesDetalle();
$detalle = $objDetalle->getItinerario(['viaje'=> $_GET['idV']]);
$total = $objDetalle->getValorTotal(['id'=> $_GET['idV']]);

for ($i=0; $i < count($detalle['data']); $i++){
	$cantidad = $detalle['data'][$i]['di'] * $detalle['data'][$i]['cantidad'];
	$fecha = $detalle['data'][$i]['fecha_inicio'];
	for($j=0; $j < $cantidad; $j++){
		$itinerario[$fecha][] = [
			'hora' => $detalle['data'][$i]['hora_inicio'],
			'idDestino' => $detalle['data'][$i]['idDestino'],
			'ts'=>$detalle['data'][$i]['ts'],
			'idServicio'=>$detalle['data'][$i]['idServicio'],
			'destino' => $detalle['data'][$i]['destino'],
			'desDestino' => $detalle['data'][$i]['desDestino'],
			'servicio' => $detalle['data'][$i]['servicio'],
			'opcion' => $detalle['data'][$i]['opcion'],
			'descripcion'=>$detalle['data'][$i]['descripcion'],
			'recomendaciones'=>$detalle['data'][$i]['recomendaciones'],
			'incluye'=>$detalle['data'][$i]['incluye'],
			'no_incluye'=>$detalle['data'][$i]['no_incluye'],
			'detalle' => $detalle['data'][$i]['detalle'],
			'pasajeros' => $detalle['data'][$i]['pasajeros']
		];
		$fecha = date('Y-m-d', strtotime($fecha. ' + 1 days'));
	}
}

class PDF extends FPDF{
	
}

$pdf = new PDF();
$pdf->AddPage();
$pdf->SetFont('Arial','',16);
$pdf->Image('../assets/img/logo.png',20,30,50);
$pdf->SetY(70);
$pdf->Cell(190,10,'Santiago de Cali, '.strftime("%d de %B del %Y"),0,1);
$pdf->Ln(30);
$pdf->SetFont('Arial','B',16);
$pdf->SetTextColor(0,156,199);
$pdf->Cell(190,10,utf8_decode('Cliente'),0,1);
$pdf->SetFont('Arial','',14);
$pdf->SetTextColor(0,0,0);
$pdf->SetX(30);
$pdf->Cell(190,10,utf8_decode($viaje['data'][0]['cliente']),0,1);
$pdf->Ln();

$pdf->SetFont('Arial','B',16);
$pdf->SetTextColor(0,156,199);
$pdf->Cell(190,10,utf8_decode('Información general'),0,1);
$pdf->SetFont('Arial','',16);
$pdf->SetTextColor(0,0,0);
$pdf->SetX(30);
$pdf->Cell(40,10,utf8_decode('Código:'));
$pdf->Cell(30,10,'C-'.$viaje['data'][0]['id'],0,1);
$pdf->SetX(30);
$pdf->Cell(40,10,'Fecha inicio:');
$pdf->Cell(30,10,$viaje['data'][0]['fecha_inicio'],0,1);
$pdf->SetX(30);
$pdf->Cell(40,10,'Fecha fin:');
$pdf->Cell(30,10,$viaje['data'][0]['fecha_fin'],0,1);
$pdf->SetX(30);
$pdf->Cell(40,10,'Pasajeros:');
$pdf->Cell(30,10,$viaje['data'][0]['pasajeros'],0,1);
$pdf->Image('../assets/img/linea.png',0,252,207);

$pdf->AddPage();
$dia = 1;
$destino = 0;
$servicio = 0;
foreach ($itinerario as $key => $actividades) {
	$pdf->SetFont('Arial','B',16);	
	$pdf->SetTextColor(0,156,199);
	$pdf->Cell(20,7,utf8_decode('Día ').$dia);
	$pdf->SetTextColor(0,0,0);
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(170,7,$key,0,1);
	$pdf->SetFont('Arial','',12);
	foreach ($actividades as $value) {
		if($value['ts'] == 5){//Vuelo
			$pdf->SetFont('Arial','B',16);
			$pdf->SetX(30);
			$pdf->Cell(20,7,$value['hora']);
			$pdf->Cell(140,7,'Vuelo',0,1);
			$pdf->SetFont('Arial','',12);
			$pdf->SetX(50);
			$pdf->Cell(140,7,utf8_decode('Trayecto: '.$value['detalle']),0,1);
			$pdf->Ln();
		}else{
			if($destino != $value['idDestino']){
				$pdf->SetFont('Arial','B',16);
				$pdf->SetX(30);
				$pdf->Cell(160,7,'Bienvenido a '.utf8_decode($value['destino']),0,1);
				$pdf->Ln(1);
				$pdf->SetFont('Arial','',12);
				$pdf->SetX(30);
				$pdf->MultiCell(160,7,utf8_decode($value['desDestino']));
				$pdf->Ln();
				$destino = $value['idDestino'];
			}
			//Esto es traslado, no mostrar foto
			if($value['ts'] == 4){//Traslado
				$pdf->SetFont('Arial','B',16);
				$pdf->SetX(30);
				$pdf->Cell(20,7,$value['hora']);
				$pdf->Cell(140,7,utf8_decode($value['servicio']),0,1);
				$pdf->SetFont('Arial','',12);
				$pdf->SetX(50);
				$pdf->Cell(130,7,utf8_decode('Opción: '.$value['opcion']),0,1);
				$pdf->Ln();
			}elseif($value['ts'] == 2){//Actividad
				$pdf->SetFont('Arial','B',16);
				$pdf->SetX(30);
				$pdf->Cell(20,7,$value['hora']);
				$pdf->Cell(140,7,'Actividad',0,1);				
				$pdf->SetX(50);
				$pdf->Cell(140,7,utf8_decode($value['servicio']),0,1);				
				$y = $pdf->GetY();
				if(file_exists('../assets/img/servicios/'.$value['idServicio'].'.jpg')){
					$pdf->Image('../assets/img/servicios/'.$value['idServicio'].'.jpg',50,$y,100);
				}else{
					$pdf->Cell(100,7,'No existe imagen',0,1);
				}
				$pdf->SetY($y+60);
				$pdf->SetX(50);
				$pdf->SetFont('Arial','',12);
				$pdf->MultiCell(140,7,utf8_decode($value['descripcion']));
				$pdf->SetX(50);
				$pdf->SetFont('Arial','B',10);
				$pdf->Cell(50,7,'Recomendaciones');
				$pdf->Cell(45,7,'Incluye');
				$pdf->Cell(45,7,'No incluye',0,1);
				$y = $pdf->GetY();
				$pdf->SetX(50);
				$pdf->SetFont('Arial','',10);
				$pdf->MultiCell(50,7,utf8_decode($value['recomendaciones']));
				$y1 = $pdf->GetY();
				$pdf->SetXY(100,$y);
				$pdf->MultiCell(45,7,utf8_decode($value['incluye']));
				$y2 = $pdf->GetY();
				$pdf->SetXY(145,$y);
				$pdf->MultiCell(45,7,utf8_decode($value['no_incluye']));
				$y3 = $pdf->GetY();
				//Evaluar el mayor
				$mayor = $y1;
				if($y2 > $y1){
					if($y3 > $y2){
						$mayor = $y3;	
					}else{
						$mayor = $y2;
					}
				}elseif($y3 > $y1){
					$mayor = $y3;
				}
				$pdf->SetXY(50,$mayor);
				$pdf->SetFont('Arial','B',10);
				$pdf->Cell(70,7,utf8_decode('Opción'));
				$pdf->Cell(30,7,'Pasajeros',0,1,'C');
				$pdf->SetX(50);
				$pdf->SetFont('Arial','',10);
				$pdf->Cell(70,7,utf8_decode($value['opcion']));
				$pdf->Cell(30,7,$value['pasajeros'],0,1,'C');
			}elseif($value['ts'] == 3){//Alojamiento
				if($servicio != $value['ts']){
					$pdf->SetFont('Arial','B',16);
					$pdf->SetX(30);
					$pdf->Cell(160,7,'Alojamiento',0,1);
					$pdf->SetX(50);
					$pdf->Cell(140,7,utf8_decode($value['servicio']),0,1);
					$y = $pdf->GetY();
					if(file_exists('../assets/img/servicios/'.$value['idServicio'].'.jpg')){
						$pdf->Image('../assets/img/servicios/'.$value['idServicio'].'.jpg',50,$y,80);
					}else{
						$pdf->Cell(100,7,'No existe imagen',0,1);
					}
					//$pdf->Image('../assets/img/servicios/'.$value['idServicio'].'.jpg',50,$y,80);
					$pdf->SetY($y+50);
					$pdf->SetFont('Arial','',12);
					$pdf->SetX(50);
					$pdf->MultiCell(140,7,utf8_decode($value['descripcion']));
					$pdf->SetX(50);

					$pdf->SetFont('Arial','B',10);					
					$pdf->Cell(45,7,'Incluye');
					$pdf->Cell(45,7,'No incluye',0,1);
					$y = $pdf->GetY();
					$pdf->SetX(50);
					$pdf->SetFont('Arial','',10);					
					$pdf->SetXY(50,$y);
					$pdf->MultiCell(45,7,utf8_decode($value['incluye']));
					$y1 = $pdf->GetY();
					$pdf->SetXY(95,$y);
					$pdf->MultiCell(45,7,utf8_decode($value['no_incluye']));
					$y2 = $pdf->GetY();
					//Evaluar el mayor
					$mayor = $y1;
					if($y2 > $y1){						
						$mayor = $y2;
					}	
					$pdf->SetXY(50,$mayor);

					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(70,7,utf8_decode('Opción'));
					$pdf->Cell(30,7,'Pasajeros',0,1,'C');					
					$servicio = $value['ts'];					
				}
				$pdf->SetX(50);
				$pdf->SetFont('Arial','',10);
				$pdf->Cell(70,7,utf8_decode($value['opcion']),0);
				$pdf->Cell(30,7,$value['pasajeros'],0,1,'C');
			}			
		}
		if($pdf->GetY() > 240){
			$pdf->AddPage();
		}
	}
	if($pdf->GetY() > 240){
		$pdf->AddPage();
	}
	$servicio = 0;
	$dia++;
}

$pdf->AddPage();
$pdf->SetFont('Arial','B',15);
$pdf->SetTextColor(0,156,199);
$pdf->Cell(190,15,utf8_decode('Información de precio'),0,1);
$pdf->SetFont('Arial','B',12);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(150,10,'PRECIO PAX',0,0,'R');
$pdf->Cell(40,10,'$'.number_format(($total['data'][0]['valor_total']/$viaje['data'][0]['pasajeros']),0,',','.'),0,1,'R');
$pdf->Cell(150,10,'PRECIO TOTAL',0,0,'R');
$pdf->Cell(40,10,'$'.number_format($total['data'][0]['valor_total'],0,',','.'),0,1,'R');
$pdf->Ln();

//Nota aclaratoria
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(255,0,0);
$pdf->Cell(190,5,'NOTA',0,1,'C');
$pdf->MultiCell(190,5,utf8_decode('TARIFAS SUJETAS A CAMBIO Y A DISPONIBILIDAD SIN PREVIO AVISO, PARA PAGOS CON TARJETA DE CRÉDITO SE APLICARÁ SUPLEMENTO'),0,'C');
$pdf->Ln();

//Sección de información de la cuenta
$pdf->SetFont('Arial','B',12);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(190,7,utf8_decode('INFORMACIÓN GENERAL'),0,1);
$pdf->SetFont('Arial','',10);
$pdf->Cell(190,7,utf8_decode('Realizar consignación en la siguiente cuenta:'),0,1);
$pdf->Cell(30,7,'Banco:');
$pdf->Cell(50,7,'Davivienda',0,1);
$pdf->Cell(30,7,'Tipo:');
$pdf->Cell(50,7,'Cuenta de ahorros',0,1);
$pdf->Cell(30,7,utf8_decode('Número:'));
$pdf->Cell(50,7,'0127 7002 1108',0,1);
$pdf->Cell(30,7,'Beneficiario:');
$pdf->Cell(50,7,utf8_decode('Diana María Guevara B'),0,1);
$pdf->Cell(30,7,'CC:');
$pdf->Cell(50,7,'31.576.329',0,1);
$pdf->Ln(2);
$pdf->MultiCell(190,5,utf8_decode('Una vez realizada la consignación, enviar comprobante de pago al correo electrónico asisgerencia@akua.com.co para realizar la reserva.'));
$pdf->Ln(2);
$pdf->MultiCell(190,5,utf8_decode('Nota: Tenga en cuenta que los horarios están sujetos a cambios por estados de mareas y clima.'));
$pdf->Ln(10);

$pdf->Image('../assets/img/usuarios/'.$viaje['data'][0]['idVendedor'].'.jpg',15,$pdf->GetY(),60);
$pdf->SetFont('Arial','B',14);
$pdf->SetTextColor(0,156,199);
$pdf->SetX(80);
$pdf->Cell(120,10,utf8_decode($viaje['data'][0]['vendedor']),0,1);
$pdf->SetFont('Arial','',12);
$pdf->SetTextColor(0,0,0);
$pdf->SetX(80);
$pdf->Cell(130,10,'Akua agencia de viajes',0,1);
$pdf->SetX(80);
$pdf->Cell(130,10,'Cra 30 No. 5A - 79, San fernando, Cali',0,1);
$pdf->SetX(80);
$pdf->Cell(40,10,utf8_decode('Teléfono:'));
$pdf->Cell(90,10,$viaje['data'][0]['telefono'],0,1);
$pdf->SetX(80);
$pdf->Cell(40,10,utf8_decode('Correo electrónico:'));
$pdf->Cell(90,10,$viaje['data'][0]['correo'],0,1);
$pdf->SetX(80);
$pdf->Cell(40,10,'Sitio web:');
$pdf->Cell(90,10,'http://akua.com.co',0,1);

$pdf->Output();
?>