<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <span class="label label-success" id="tituloReserva"></span>
            <small>                
                <span class="label label-success">
                    <i class="fa fa-users"></i> X <span class="tituloPasajeros"></span>
                </span>
            </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="intranet/index.php"><i class="fa fa-dashboard"></i> Menu</a></li>
            <li><a href="intranet/reservas.php"> Reservas</a></li>
            <li class="active"> Operacion</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Servicios</h3>
                        <div class="pull-right">
                            <button id="botonMostrarModalDetalle" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">Fecha</th>
                                        <th class="text-center">Hora</th>
                                        <th class="text-center">Destino</th>
                                        <th class="text-center">Proveedor</th>
                                        <th class="text-center">Servicio</th>
                                        <th class="text-center">Opción</th>
                                        <th class="text-center">Detalle</th>
                                        <th class="text-center">Costo</th>                                        
                                        <th class="text-center">Pasajeros</th>
                                        <th class="text-center">Cantidad</th>
                                        <th class="text-center">Total</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody id="contenido"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <ul class="timeline" id="contenidoItinerario"></ul>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalProveedores">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <span>Información proveedor</span>
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tbody id="contenidoProveedores"></tbody>
                </table>
                <div class="text-center">
                    <button id="mostrarContactos" class="btn btn-primary" type="button" title="Más contactos">
                        <i class="fa fa-users"></i>
                    </button>
                </div>
                <br>
                <div id="contenidoContactos"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalReservar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalReservarTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioReservar">
                    <div class="form-group">
                        <label for="costo" class="control-label">Precio total negociado</label>
                        <input id="costo" class="form-control numero" type="text">
                    </div>
                    <div class="form-group">
                        <label for="observaciones" class="control-label">Observación</label>
                        <textarea id="observaciones" name="observaciones" class="form-control" cols="10" rows="3"></textarea>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" type="submit" form="formularioReservar">
                    <i class="fa fa-check"></i> Reservar
                </button>                
            </div>
        </div>            
    </div>        
</div>

<div class="modal fade" id="modalCancelar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalCancelarTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioCancelar">
                    <div class="form-group">
                        <label for="observaciones" class="control-label">Observación</label>
                        <textarea id="observaciones" name="observaciones" class="form-control" cols="10" rows="3"></textarea>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="submit" form="formularioCancelar">
                    <i class="fa fa-save"></i> Cancelar
                </button>                
            </div>
        </div>            
    </div>        
</div>

<?php require 'footer.php'; ?>
<script type="text/javascript">
    var idViaje = <?=$_REQUEST['idV']?>;
    var id
    var idPro
    var pasajeros
    var cantidad

    function initLogin(user){
        $('.numero').number( true, 0,',','.')
        //consultar el viaje para obtener nombre y fechas para armar los días
        procesarRegistro('viajes', 'select', {id: idViaje}, function(r){
            //Coloco el nombre de la reserva en el titulo
            $('.tituloPasajeros').text(r.data[0].pasajeros)
            $('#tituloReserva').text('R-'+r.data[0].codigo_reserva)
        })

        cargarRegistros({viaje: idViaje, estado: 'Activo'}, 'crear')

        $('#formularioReservar').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            data.id = id
            data.estado = 'Reservado'
            data.costo = $('#costo').val() / cantidad / pasajeros            
            procesarRegistro('viajesDetalle', 'update', data, function(r){ 
                cargarRegistros({id: id}, 'actualizar')
                $('#modalReservar').modal('hide')                
                procesarRegistro('cxp', 'crearCuenta', {viaje: idViaje, proveedor: idPro, valor:$('#costo').val(), saldo:$('#costo').val()}, function(r){
                    console.log('ok cuenta')
                })                
            })
        })

        $('#formularioCancelar').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))            
            data.id = id
            data.estado = 'Devuelto'
            procesarRegistro('viajesDetalle', 'update', data, function(r){
                cargarRegistros({id: id}, 'actualizar')
                $('#modalCancelar').modal('hide')                
            })            
        })

        //Mostrar contactos de proveedor
        $('#mostrarContactos').on('click', function(){
            let fila = ''
            procesarRegistro('proveedoresContactos', 'select', {fk_proveedores: idPro}, function(r){
                fila = '<table class="table table-bordered"><tr><th>Nombre</th><th>Telefono</th><th>Correo</th><th>Observación</th></tr>'
                for(let i = 0; i < r.data.length; i++){
                    fila += '<tr>'+
                            '<td>'+r.data[i].nombre+'</td>'+
                            '<td>'+r.data[i].telefono+'</td>'+
                            '<td>'+r.data[i].correo+'</td>'+
                            '<td>'+r.data[i].observaciones+'</td>'+
                            '</tr>'
                }
                fila += '</table>'
                $('#contenidoContactos').append(fila)
            })
        })
    }    

    function cargarRegistros(data, accion){
        procesarRegistro('viajesDetalle', 'getDetalle', data, function(r){
            let fila
            let total
            let fondo = {
                Gestionar: 'bg-gray',
                Bloqueado: 'bg-amarillo',
                Reservado: 'bg-green',
                Devuelto: 'bg-red'
            }
            let opciones = ''
            for(let i = 0; i < r.data.length; i++){
                total = r.data[i].costo * r.data[i].pasajeros * r.data[i].cantidad
                opciones = ''
                if(r.data[i].estado == 'Bloqueado'){
                    opciones += '<button class="btn btn-success btn-xs" onClick="reservar('+r.data[i].id+',\''+r.data[i].servicio+'\','+r.data[i].idp+','+r.data[i].pasajeros+','+r.data[i].cantidad+','+total+')"><i class="fa fa-check"></i></button> '
                    opciones += '<button class="btn btn-danger btn-xs" onClick="cancelar('+r.data[i].id+',\''+r.data[i].servicio+'\')"><i class="fa fa-close"></i></button>'
                }
                fila += '<tr id="'+r.data[i].id+'" class="'+fondo[r.data[i].estado]+'">'+
                            '<td class="text-center">'+r.data[i].fecha_inicio+'</td>'+
                            '<td class="text-center">'+r.data[i].hora_inicio+'</td>'+
                            '<td class="text-center">'+r.data[i].destino+'</td>'+
                            '<td class="text-center"><button type="button" class="btn btn-link" onClick="infoProveedores('+r.data[i].idp+')">'+r.data[i].proveedor+'</button></td>'+
                            '<td>'+r.data[i].servicio+'</td>'+
                            '<td>'+r.data[i].opcion+'</td>'+
                            '<td>'+r.data[i].detalle+'</td>'+                            
                            '<td class="text-center">$'+currency(r.data[i].costo,0)+'</td>'+
                            '<td class="text-center">'+r.data[i].pasajeros+'</td>'+
                            '<td class="text-center">'+r.data[i].cantidad+'</td>'+
                            '<td class="text-center">$'+currency(total,0)+'</td>'+
                            '<td class="text-center">'+
                                opciones+
                            '</td>'+
                        '</tr>'
            }
            if(accion == 'crear'){
                $('#contenido').append(fila)
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }
            $('#contenidoItinerario').empty()
            construirItinerario(idViaje)
        })
    }

    function reservar(idDetalle, servicio, idProveedor, ppasajeros, pcantidad, pcosto){
        id = idDetalle
        idPro = idProveedor
        pasajeros = ppasajeros
        cantidad = pcantidad
        $('#formularioReservar')[0].reset()
        $('#costo').val(pcosto)
        $('#modalReservarTitulo').text(servicio)
        $('#modalReservar').modal('show')
    }

    function cancelar(idDetalle, servicio){
        id = idDetalle        
        $('#formularioCancelar')[0].reset()
        $('#modalCancelarTitulo').text(servicio)
        $('#modalCancelar').modal('show')
    }

    function infoProveedores(idProveedor){
        idPro = idProveedor
        $('#contenidoProveedores').empty()
        $('#contenidoContactos').empty()
        procesarRegistro('proveedores', 'select', {id:idProveedor}, function(r){
            $('#contenidoProveedores').append(
                '<tr><td>Nombre:</td><td>'+r.data[0].nombre+'</td></tr>'+
                '<tr><td>Tipo documento:</td><td>'+r.data[0].tipo_doc+'</td></tr>'+
                '<tr><td>Número:</td><td>'+r.data[0].numero+'</td></tr>'+
                '<tr><td>Telefono:</td><td>'+r.data[0].telefono+'</td></tr>'+
                '<tr><td>Correo:</td><td>'+r.data[0].correo+'</td></tr>'
            )
        })
        $('#modalProveedores').modal('show')
    }
</script>