<?php
require 'header.php';
?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Dashboard proveedor
            <small>Administración</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Menu</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <section class="content container-fluid">        
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-info">                    
                    <div class="box-body">
                        <div class="chart">
                            <div id="estadoServicios" style="height: 300px"></div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>        
    </section>
</div>
<?php
require 'footer.php';
?>
<script type="text/javascript">
    function initLogin(user){
        procesarRegistro('reportes', 'estadoServiciosProveedor', {id:user.usuario.proveedor}, function(r){
            let categorias = []
            let serie = {
                name: 'Cantidad',
                data: []                
            }
            for (let i = 0; i < r.data.length; i++){
                categorias.push(r.data[i].estado)
                serie.data.push(r.data[i].cantidad)
            }
            
            Highcharts.chart('estadoServicios', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Estado de servicios'
                },
                subtitle: {
                    text: 'Cantidad de servicios por estado'
                },
                xAxis: {
                    categories: categorias
                },
                colors:['#2597DC'],
                yAxis: {
                    title: {
                        text: 'Cantidad'
                    }
                },
                plotOptions: {
                    column: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [serie]
            })
        })
    }
</script>