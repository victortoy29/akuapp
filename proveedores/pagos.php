<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            CxC
            <small>Gestión</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="proveedores/index.php"><i class="fa fa-dashboard"></i> Proveedores</a></li>            
            <li class="active"> CxC</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listado</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tablaContenido" class="table table-bordered">
                                <thead>
                                    <tr>                                        
                                        <th class="text-center">Reserva</th>
                                        <th class="text-center">Valor</th>
                                        <th class="text-center">Saldo</th>
                                        <th class="text-center">Estado</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody id="contenido"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalHistoricoPagos">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <span id="modalHistoricoPagosTitulo"></span>
                    <small> Historico de pagos</small>
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Valor</th>                                
                            <th class="text-center">Fecha</th>
                            <th class="text-center">Comprobante</th>
                        </tr>
                    </thead>
                    <tbody id="contenidoHistoricoPagos"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php'; ?>
<script type="text/javascript">
    var id

    function initLogin(user){
        cargarRegistros({fk_proveedores:user.usuario.proveedor})
    }

    function cargarRegistros(data){
        procesarRegistro('cxp', 'getCuentas', data, function(r){
            let fila            
            let color = {
                Pendiente: 'text-red',
                Finalizado: 'text-green'
            }
            for(let i = 0; i < r.data.length; i++){                
                fila += '<tr id="'+r.data[i].id+'">'+
                            '<td class="text-center">R-'+r.data[i].codigo_reserva+'</td>'+                            
                            '<td class="text-right">$'+currency(r.data[i].valor,0)+'</td>'+
                            '<td class="text-right">$'+currency(r.data[i].saldo,0)+'</td>'+
                            '<td class="text-center '+color[r.data[i].estado]+'">'+r.data[i].estado+'</td>'+
                            '<td class="text-center">'+
                                '<button class="btn btn-default btn-xs" title="Historico pagos" onClick="verHistorico('+r.data[i].id+','+r.data[i].codigo_reserva+')"><i class="fa fa-history"></i></button>'+
                            '</td>'+
                        '</tr>'
            }            
            $('#contenido').append(fila)            
        })
    }

    function verHistorico(idcxp, cr){
        $('#modalHistoricoPagosTitulo').text('R-'+cr)
        $('#contenidoHistoricoPagos').empty()
        cargarRegistrosPagos({'fk_cxp':idcxp})
        $('#modalHistoricoPagos').modal('show')
    }

    function cargarRegistrosPagos(data){
        procesarRegistro('cxpPagos', 'select', data, function(r){
            let fila = ''            
            for(let i = 0; i < r.data.length; i++){                
                fila += '<tr>'+                            
                            '<td class="text-right">$'+currency(r.data[i].valor,0)+'</td>'+                            
                            '<td class="text-center">'+r.data[i].fecha_creacion+'</td>'+
                            '<td class="text-center">'+                                
                                '<a class="btn btn-default btn-xs" href="assets/img/comprobantes_cxp/'+r.data[i].id+'.jpg" target="_blank"><i class="fa fa-image"></i></a>'+
                            '</td>'+
                        '</tr>'
            }            
            $('#contenidoHistoricoPagos').append(fila)
        })
    }
</script>
</body>
</html>