<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Servicios
            <small>Listado</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="proveedores/index.php"><i class="fa fa-dashboard"></i> Proveedores</a></li>            
            <li class="active">Servicios</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box">                    
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tablaContenido" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">Código</th>
                                        <th class="text-center">Fecha</th>
                                        <th class="text-center">Hora</th>
                                        <th class="text-center">Servicio</th>
                                        <th class="text-center">Opción</th>
                                        <th class="text-center">Detalle</th>
                                        <th class="text-center">Costo por persona</th>
                                        <th class="text-center">Pasajeros</th>
                                        <th class="text-center">Total</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody id="contenido"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php require 'footer.php'; ?>
<script type="text/javascript">
    var id

    function initLogin(user){
        cargarRegistros({campo: 'servicios.fk_proveedores', valor: user.usuario.proveedor}, 'crear')
    }

    function cargarRegistros(data, accion){
        procesarRegistro('proveedores', 'getSeviciosByProveedor', data, function(r){
            let fila
            let total
            let codigo
            let opciones = ''
            let colores = {
                Gestionar: 'bg-gray',
                Bloqueado: 'bg-amarillo',
                Reservado: 'bg-green'
            }
            for(let i = 0; i < r.data.length; i++){
                total = r.data[i].pasajeros * r.data[i].costo
                opciones = ''
                codigo = 'C-'+r.data[i].idv
                if(r.data[i].vestado == 'Reserva'){
                    codigo = 'R-'+r.data[i].cr
                }
                if(r.data[i].estado == 'Gestionar'){
                    opciones += '<button class="btn btn-success btn-xs" onClick="procesar('+r.data[i].id+',\'Bloqueado\',\'Bloquear\')"><i class="fa fa-check"></i></button> '
                    opciones += '<button class="btn btn-danger btn-xs" onClick="procesar('+r.data[i].id+',\'Devuelto\',\'Rechazar\')"><i class="fa fa-close"></i></button>'
                }
                fila += '<tr id="'+r.data[i].id+'" class="'+colores[r.data[i].estado]+'">'+
                            '<td class="text-center">'+codigo+'</td>'+
                            '<td class="text-center">'+r.data[i].fecha_inicio+'</td>'+
                            '<td class="text-center">'+r.data[i].hora_inicio+'</td>'+
                            '<td class="text-center">'+r.data[i].nombre+'</td>'+
                            '<td class="text-center">'+r.data[i].opcion+'</td>'+
                            '<td class="text-center">'+r.data[i].detalle+'</td>'+
                            '<td class="text-center">$'+currency(r.data[i].costo,0)+'</td>'+
                            '<td class="text-center">'+r.data[i].pasajeros+'</td>'+
                            '<td class="text-center">$'+currency(total,0)+'</td>'+
                            '<td class="text-center">'+
                                opciones+
                            '</td>'+
                        '</tr>'
            }
            if(accion == 'crear'){
                $('#contenido').append(fila)
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }
        })
    }    

    function procesar(idS, estado, accion){        
        swal('Seguro de '+accion+' el servicio?', {
            buttons: ['No','Si']
        })
        .then((value) => {
            if (value) {
                procesarRegistro('viajesDetalle', 'update', {id:idS, estado: estado}, function(){
                    if(estado == 'Devuelto'){
                        $('#'+idS).hide('slow')
                    }else{
                        cargarRegistros({campo: 'viajes_detalle.id', valor:idS})
                    }                    
                })
            }
        })
    }
</script>
</body>
</html>